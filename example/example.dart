// ignore_for_file: unused_local_variable

import 'dart:async';
import 'package:etcd/etcd.dart';
import 'package:faker/faker.dart';
import 'package:grpc/grpc.dart';
import 'package:stack_trace/stack_trace.dart';



const String HOST = '192.168.145.139';
const int    PORT = 30326;
const String KEY  = 'foo';

Future<void> main(List<String> args) async {
  return Chain.capture(() async {
    Timer refreshTimer;

    final client = new EtcdClient(HOST, port: PORT, options: new ChannelOptions(credentials: ChannelCredentials.insecure()));

    try {
      print('creating lease...');
      var lease = await client.lease.grant(Duration(seconds: 5));
      print('lease ${lease.id} created with TTL: ${lease.ttl}');
      print('');

      print('creating a watcher for `$KEY`...');
      var watcher = await client.watch.prefix(KEY, prevKv: true); // ignore: avoid_single_cascade_in_expression_statements
      watcher.listen((EtcdWatchEvent event) {
        switch (event.type) {
          case EtcdWatchEventType.PUT:
            print('Key `${event.kv.key}` modified: replaced value `${event.prevKv.value}` with `${event.kv.value}`');
            break;

          case EtcdWatchEventType.DELETE:
            print('Key `${event.kv.key}` was deleted: deleted value was `${event.prevKv.value}`');
            break;

          default:
            print('some other event ${event.type.name}');
        }
      });
      print('watcher ${watcher.id} created for `$KEY`');
      print('');

      refreshTimer = Timer.periodic(Duration(seconds: 1), (_) async {
        await lease.keepAlive().then((response) => print('\t*renewed: ${response.ttl}'));
      });



      print('setting original value and applying lease ...');
      await client.kv.put(KEY, faker.lorem.word(), lease: lease.id);

      var currentValue = await client.kv.fetch(KEY);
      print('received current value: ${currentValue}');

      var remainingTTL = await lease.timeToLive(listKeys: true);
      print('expiring in ${remainingTTL.ttl}: keys: ${remainingTTL.keys}');

      await Future.delayed(Duration(seconds: 3), refreshTimer.cancel); // -- wait for a bit so we can see our lease renewals

      await lease.revoke();
      print('forcefully revoked lease ${lease.id} (and cleared all keys bound to it)');

      currentValue = await client.kv.fetch(KEY);
      print('received current value: ${currentValue}');
      print('');



      print('re-setting value ...');
      var oldValue = await client.kv.put(KEY, faker.lorem.word());

      print('updating value ...');
      await client.kv.put(KEY, faker.lorem.word());

      print('compacting history ...');
      await client.kv.compact(3); // -- compact up to (but not including) version 3



      print('deleting value (transaction)...');
      var transaction = client.kv.startTransaction()
        ..compare.value(KEY, EtcdComparisonOperator.IS_NOT_EQUAL, '') // -- if KEY is not empty
        ..success.delete(KEY)                                         // -- delete it
        ..failure.put('ERROR', 'key already deleted');                // -- otherwise, record the error

      var responses = await transaction.send();

    } finally {
      print('\nshutting down...');
      await client.shutdown(); // -- close our connection
    }
  });
}

part of etcd;

abstract class _EtcdResponse {
  final GeneratedMessage _delegate;
  final Encoding         _codec;

  _EtcdResponse(GeneratedMessage response, Encoding encoding): _delegate = response, _codec = encoding;

  ResponseHeader get header => (_delegate as dynamic).header;
}

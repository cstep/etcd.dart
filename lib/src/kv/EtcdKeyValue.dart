part of etcd;

class EtcdKeyValue /*implements grpc.KeyValue*/ implements MapEntry {
  final KeyValue _delegate;
  final Encoding _codec;

  EtcdKeyValue(KeyValue keyValue, Encoding encoding): _delegate = keyValue, _codec = encoding;

  @override dynamic get key   => _codec.decode(_delegate.key);
  @override dynamic get value => _codec.decode(_delegate.value);
  int     get createRevision  => _delegate.createRevision.toInt();
  int     get modRevision     => _delegate.modRevision.toInt();
  int     get version         => _delegate.version.toInt();
  int     get lease           => _delegate.lease.toInt();

  @override
  String toString() => "EtcdKeyValue(${key.toString()}: ${value.toString()})";
}

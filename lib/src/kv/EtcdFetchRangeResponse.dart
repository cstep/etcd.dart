part of etcd;

class EtcdFetchRangeResponse extends EtcdKVResponse /*implements grpc.RangeResponse*/ {
  @override RangeResponse get _delegate => super._delegate;

  EtcdFetchRangeResponse(RangeResponse response, Encoding encoding): super(response, encoding);

  /// the list of key-value pairs matched by the range request. When [countOnly] is set in the request, this field is empty.
  List<EtcdKeyValue> get kvs => _delegate.kvs.map((kv) => new EtcdKeyValue(kv, _codec)).toList();

  /// indicates if there are more keys to return in the requested range if [limit] was set on the range request.
  bool get more => _delegate.more;

  /// the total number of keys satisfying the range request.
  int get count => _delegate.count.toInt();
}

// ignore_for_file: avoid_returning_this

part of etcd;

class EtcdTransactionOpQueue with DelegatingList<RequestOp> implements _EtcdKVClientBase {
  final List<RequestOp> _queue;
  final EtcdKVClient    _client;

  @override List<RequestOp> get _listDelegate => _queue;

  EtcdTransactionOpQueue(this._queue, this._client);

  @override
  EtcdTransactionOpQueue put(String key, String value, {int lease, bool prevKv = true, bool ignoreValue, bool ignoreLease, CallOptions options}) {
    _queue.add(new RequestOp()..requestPut = _client._requestBuilder._putRequest(key, value, lease: lease, prevKv: prevKv, ignoreValue: ignoreValue, ignoreLease: ignoreLease));

    return this;
  }

  @override
  EtcdTransactionOpQueue delete(String key, {bool prevKv = true}) {
    _queue.add(new RequestOp()..requestDeleteRange = _client._requestBuilder._deleteRequest(key, prevKv : prevKv,));

    return this;
  }

  @override
  EtcdTransactionOpQueue deleteRange(String key, String rangeEnd, {bool prevKv = true}) {
    _queue.add(new RequestOp()..requestDeleteRange = _client._requestBuilder._deleteRangeRequest(key, rangeEnd, prevKv: prevKv));

    return this;
  }

  @override
  EtcdTransactionOpQueue deletePrefix(String key, {bool prevKv = true}) {
    _queue.add(new RequestOp()..requestDeleteRange = _client._requestBuilder._deletePrefixRequest(key, prevKv: prevKv));

    return this;
  }

  @override
  EtcdTransactionOpQueue fetch<T>(String key, {int revision, bool serializable}) {
    _queue.add(new RequestOp()..requestRange = _client._requestBuilder._fetchRequest(key, revision: revision, serializable: serializable));

    return this;
  }

  @override
  EtcdTransactionOpQueue fetchRange(String key, String rangeEnd, {
    int                     limit,
    int                     revision,
    bool                    serializable,
    bool                    keysOnly,
    bool                    countOnly,
    int                     minModRevision,
    int                     maxModRevision,
    int                     minCreateRevision,
    int                     maxCreateRevision,
    RangeRequest_SortOrder  sortOrder,
    RangeRequest_SortTarget sortTarget,
  }) {
    _queue.add(new RequestOp()..requestRange = _client._requestBuilder._fetchRangeRequest(key, rangeEnd,
      limit            : limit,
      revision         : revision,
      serializable     : serializable,
      keysOnly         : keysOnly,
      countOnly        : countOnly,
      minModRevision   : minModRevision,
      maxModRevision   : maxModRevision,
      minCreateRevision: minCreateRevision,
      maxCreateRevision: maxCreateRevision,
      sortOrder        : sortOrder,
      sortTarget       : sortTarget,
    ));

    return this;
  }

  @override
  EtcdTransactionOpQueue fetchPrefix(String key, {
    int                     limit,
    int                     revision,
    bool                    serializable,
    bool                    keysOnly,
    bool                    countOnly,
    int                     minModRevision,
    int                     maxModRevision,
    int                     minCreateRevision,
    int                     maxCreateRevision,
    RangeRequest_SortOrder  sortOrder,
    RangeRequest_SortTarget sortTarget,
  }) {
    _queue.add(new RequestOp()..requestRange = _client._requestBuilder._fetchPrefixRequest(key,
      limit            : limit,
      revision         : revision,
      serializable     : serializable,
      keysOnly         : keysOnly,
      countOnly        : countOnly,
      minModRevision   : minModRevision,
      maxModRevision   : maxModRevision,
      minCreateRevision: minCreateRevision,
      maxCreateRevision: maxCreateRevision,
      sortOrder        : sortOrder,
      sortTarget       : sortTarget,
    ));

    return this;
  }

  /// NOTE: this is currently listed in etcd's protobuf, but the matching server version (server: 3.4.7, api: 3.4.0) doesn't seem to support it
  EtcdTransactionOpQueue transaction(void Function(EtcdTransaction) provisioner) {
    var transaction = _client.startTransaction();
    provisioner(transaction);

    _queue.add(new RequestOp()..requestTxn = transaction._request);

    return this;
  }
}

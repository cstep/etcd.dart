part of etcd;

class EtcdTransaction {
  final EtcdKVClient _client;
  final TxnRequest   _request = new TxnRequest();

  EtcdTransactionOpQueue _success;
  EtcdTransactionOpQueue get success => _success ??= new EtcdTransactionOpQueue(_request.success, this._client);

  EtcdTransactionOpQueue _failure;
  EtcdTransactionOpQueue get failure => _failure ??= new EtcdTransactionOpQueue(_request.failure, this._client);

  EtcdTransactionCompareQueue _compare;
  EtcdTransactionCompareQueue get compare => _compare ??= new EtcdTransactionCompareQueue(_request.compare, this._client);

  EtcdTransaction._(this._client);

  Future<EtcdTransactionResponse> send() => _client._sendTransaction(this);
}

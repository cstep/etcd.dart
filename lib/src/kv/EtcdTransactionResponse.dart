part of etcd;

class EtcdTransactionResponse extends EtcdKVResponse with DelegatingList<EtcdKVResponse> /*, grpc.TxnResponse*/ {
  TxnResponse                    get _transactionDelegate => super._delegate;
  @override List<EtcdKVResponse> get _listDelegate        => responses;

  EtcdTransactionResponse(TxnResponse response, Encoding encoding): super(response, encoding);

  /// the key-value pair overwritten by the put operation, if [prevKv] was set in the put request
  bool get succeeded => _transactionDelegate.succeeded;

  List<EtcdKVResponse> _responses;
  List<EtcdKVResponse> get responses {
    return _responses ??= new UnmodifiableListView<EtcdKVResponse>(
      _transactionDelegate.responses
        .map((ResponseOp r) {
          if (r.hasResponseRange())       return new  EtcdFetchRangeResponse(r.responseRange,       super._codec);
          if (r.hasResponsePut())         return new         EtcdPutResponse(r.responsePut,         super._codec);
          if (r.hasResponseDeleteRange()) return new EtcdDeleteRangeResponse(r.responseDeleteRange, super._codec);
          if (r.hasResponseTxn())         return new EtcdTransactionResponse(r.responseTxn,         super._codec); // TODO: add support for this possibility in the txn request
          /*else*/ return null;
        })
        .where((r) => r != null)
        .toList()
    );
  }
}

part of etcd;

class _KVRequestBuilder {
  final Encoding _codec;

  _KVRequestBuilder(this._codec);

  PutRequest _putRequest(String key, String value, {
      int  lease,
      bool prevKv = true,
      bool ignoreValue,
      bool ignoreLease,
  }) {
    var request = new PutRequest()
      ..key   = _encode(key)
      ..value = _encode(value);

    if (lease       != null) request.lease       = Int64(lease);
    if (prevKv      != null) request.prevKv      = prevKv;
    if (ignoreValue != null) request.ignoreValue = ignoreValue;
    if (ignoreLease != null) request.ignoreLease = ignoreLease;

    return request;
  }

  RangeRequest _fetchRequest(String key, {int revision, bool serializable}) {
    return _fetchRangeRequest(key, null, limit: 1, revision: revision, serializable: serializable);
  }

  RangeRequest _fetchPrefixRequest(String key, {
    int                     limit,
    int                     revision,
    bool                    serializable,
    bool                    keysOnly,
    bool                    countOnly,
    int                     minModRevision,
    int                     maxModRevision,
    int                     minCreateRevision,
    int                     maxCreateRevision,
    RangeRequest_SortOrder  sortOrder,
    RangeRequest_SortTarget sortTarget
  }) {
    return _fetchRangeRequest(key, null,
      limit            : limit,
      revision         : revision,
      serializable     : serializable,
      keysOnly         : keysOnly,
      countOnly        : countOnly,
      minModRevision   : minModRevision,
      maxModRevision   : maxModRevision,
      minCreateRevision: minCreateRevision,
      maxCreateRevision: maxCreateRevision,
      sortOrder        : sortOrder,
      sortTarget       : sortTarget,
      asPrefix         : true,
    );
  }

  RangeRequest _fetchRangeRequest(String key, String rangeEnd, {
      int                     limit,
      int                     revision,
      bool                    serializable,
      bool                    keysOnly,
      bool                    countOnly,
      int                     minModRevision,
      int                     maxModRevision,
      int                     minCreateRevision,
      int                     maxCreateRevision,
      RangeRequest_SortOrder  sortOrder,
      RangeRequest_SortTarget sortTarget,
      bool                    asPrefix = false,
  }) {
    var request = new RangeRequest();

    if (key      != null) request.key = _encode(key);
    if (rangeEnd != null) {
      request.rangeEnd = _encode(rangeEnd)..last += 1; // -- make the range INCLUSIVE of `rangeEnd`
    } else if (asPrefix == true) {
      if (key == null) { // -- both start and end are null
        // -- fetch everything
        request.key      = [0];
        request.rangeEnd = [0];
      } else {
        request.rangeEnd = List.from(request.key)..last += 1; // -- treat `key` like a prefix
      }
    }

    if (limit             != null) request.limit             = Int64(limit);
    if (revision          != null) request.revision          = Int64(revision);
    if (serializable      != null) request.serializable      = serializable;
    if (keysOnly          != null) request.keysOnly          = keysOnly;
    if (countOnly         != null) request.countOnly         = countOnly;
    if (minModRevision    != null) request.minModRevision    = Int64(minModRevision);
    if (maxModRevision    != null) request.maxModRevision    = Int64(maxModRevision);
    if (minCreateRevision != null) request.minCreateRevision = Int64(minCreateRevision);
    if (maxCreateRevision != null) request.maxCreateRevision = Int64(maxCreateRevision);
    if (sortOrder         != null) request.sortOrder         = sortOrder;
    if (sortTarget        != null) request.sortTarget        = sortTarget;

    return request;
  }

  DeleteRangeRequest _deleteRequest(String key, {bool prevKv}) {
    return _deleteRangeRequest(key, null, prevKv: prevKv);
  }

  DeleteRangeRequest _deletePrefixRequest(String key, {bool prevKv = true}) {
    return _deleteRangeRequest(key, null, prevKv: prevKv, asPrefix: true);
  }

  DeleteRangeRequest _deleteRangeRequest(String key, String rangeEnd, {bool prevKv = true, bool asPrefix = false}) {
    var request = new DeleteRangeRequest();

    if (key      != null) request.key    = _encode(key);
    if (prevKv   != null) request.prevKv = prevKv;
    if (rangeEnd != null) {
      request.rangeEnd = _encode(rangeEnd)..last += 1; // -- make the range INCLUSIVE of `rangeEnd`
    } else if (asPrefix == true) {
      if (key == null) { // -- both start and end are null
        // -- delete everything
        request.key      = [0];
        request.rangeEnd = [0];
      } else {
        request.rangeEnd = List.from(request.key)..last += 1; // -- treat `key` like a prefix
      }
    }

    return request;
  }

  // --

  List<int> _encode(String    value) => _codec.encode(value);
  String    _decode(List<int> bytes) => _codec.decode(bytes);
}

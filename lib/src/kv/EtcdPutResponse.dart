part of etcd;

class EtcdPutResponse extends EtcdKVResponse /*implements grpc.PutResponse*/ {
  @override PutResponse get _delegate => super._delegate;

  EtcdPutResponse(PutResponse response, Encoding encoding): super(response, encoding);

  /// the key-value pair overwritten by the put operation, if [prevKv] was set in the put request
  EtcdKeyValue get prevKv => new EtcdKeyValue(_delegate.prevKv, _codec);

  bool get hasPrevKv => _delegate.hasPrevKv();
}

// ignore_for_file: avoid_returning_this

part of etcd;

class EtcdTransactionCompareQueue with DelegatingList<Compare> /*implements grpc.Compare*/ {
  final List<Compare> _queue;
  final EtcdKVClient  _client;

  @override List<Compare> get _listDelegate => _queue;

  EtcdTransactionCompareQueue(this._queue, this._client);

  EtcdTransactionCompareQueue value(String key, EtcdComparisonOperator operator, dynamic value) {
    _queue.add(new Compare()
      ..key    = _client._requestBuilder._encode(key)
      ..result = operator
      ..target = Compare_CompareTarget.VALUE
      ..value  = _client._requestBuilder._encode(value.toString())
    );

    return this;
  }

  EtcdTransactionCompareQueue createRevision(String key, EtcdComparisonOperator operator, int revision) {
    _queue.add(new Compare()
      ..key    = _client._requestBuilder._encode(key)
      ..result = operator
      ..target = Compare_CompareTarget.CREATE
      ..value  = _client._requestBuilder._encode(revision.toString())
    );

    return this;
  }

  EtcdTransactionCompareQueue modRevision(String key, EtcdComparisonOperator operator, int revision) {
    _queue.add(new Compare()
      ..key    = _client._requestBuilder._encode(key)
      ..result = operator
      ..target = Compare_CompareTarget.MOD
      ..value  = _client._requestBuilder._encode(revision.toString())
    );

    return this;
  }

  EtcdTransactionCompareQueue version(String key, EtcdComparisonOperator operator, int version) {
    _queue.add(new Compare()
      ..key    = _client._requestBuilder._encode(key)
      ..result = operator
      ..target = Compare_CompareTarget.VERSION
      ..value  = _client._requestBuilder._encode(version.toString())
    );

    return this;
  }
}

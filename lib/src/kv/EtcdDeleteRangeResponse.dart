part of etcd;

class EtcdDeleteRangeResponse extends EtcdKVResponse /*implements grpc.DeleteRangeResponse*/ {
  @override DeleteRangeResponse get _delegate => super._delegate;

  EtcdDeleteRangeResponse(DeleteRangeResponse response, Encoding encoding): super(response, encoding);

  /// number of keys deleted
  int get deleted => _delegate.deleted.toInt();

  /// a list of all key-value pairs deleted by the DeleteRange operation
  List<EtcdKeyValue> get prevKvs => _delegate.prevKvs.map((keyValue) => new EtcdKeyValue(keyValue, _codec)).toList();

  bool get hasPrevKvs => _delegate.prevKvs.isNotEmpty;
}

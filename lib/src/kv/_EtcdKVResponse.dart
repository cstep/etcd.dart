part of etcd;

abstract class EtcdKVResponse extends _EtcdResponse {
  EtcdKVResponse(GeneratedMessage response, Encoding encoding): super(response, encoding);
}

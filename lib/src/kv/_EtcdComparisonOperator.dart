part of etcd;

/// this class is a wrapper and is PURELY to create a better/shorter class name than Compare_CompareResult
class EtcdComparisonOperator implements Compare_CompareResult {
  final Compare_CompareResult _delegate;

  static const EtcdComparisonOperator IS_EQUAL        = EtcdComparisonOperator._(Compare_CompareResult.EQUAL    );
  static const EtcdComparisonOperator IS_GREATER_THAN = EtcdComparisonOperator._(Compare_CompareResult.GREATER  );
  static const EtcdComparisonOperator IS_LESS_THAN    = EtcdComparisonOperator._(Compare_CompareResult.LESS     );
  static const EtcdComparisonOperator IS_NOT_EQUAL    = EtcdComparisonOperator._(Compare_CompareResult.NOT_EQUAL);

  @override String get name  => _delegate.name;
  @override int    get value => _delegate.value;

  static const List<EtcdComparisonOperator> values = <EtcdComparisonOperator> [
    IS_EQUAL,
    IS_GREATER_THAN,
    IS_LESS_THAN,
    IS_NOT_EQUAL,
  ];

  const EtcdComparisonOperator._(Compare_CompareResult delegate): _delegate = delegate;

  @override String toString() => name;
}

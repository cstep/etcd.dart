part of etcd;

class EtcdKVClient extends _EtcdSubClient implements _EtcdKVClientBase /*, grpc.KVClient*/ {
  @override KVClient get _delegate => super._delegate;

  _KVRequestBuilder _requestBuilder;

  EtcdKVClient(ClientChannel channel, Encoding encoding): super(new KVClient(channel), encoding) {
    _requestBuilder = new _KVRequestBuilder(this._codec);
  }

  /// Update the value for a given key
  ///
  /// @param [key]         the name of the key to put into the key-value store.
  /// @param [value]       the value, in bytes, to associate with the key in the key-value store.
  /// @param [lease]       the lease ID to associate with the key in the key-value store. A lease value of 0 indicates no lease.
  /// @param [prevKv]      when set, responds with the key-value pair data before the update from this Put request.
  /// @param [ignoreValue] when set, update the key without changing its current value. Returns an error if the key does not exist.
  /// @param [ignoreLease] when set, update the key without changing its current lease. Returns an error if the key does not exist.
  @override
  Future<String> put(String key, String value, {
      int         lease,
      bool        prevKv = true,
      bool        ignoreValue,
      bool        ignoreLease,
      CallOptions options
  }) {
    var request = _requestBuilder._putRequest(key, value, lease: lease, prevKv: prevKv, ignoreValue: ignoreValue, ignoreLease: ignoreLease);

    return _delegate.put(request, options: options)
      .then((response) => new EtcdPutResponse(response, _codec).prevKv?.value);
  }

  /// Fetch a value stored for a single key
  ///
  /// @param [key], [rangeEnd]     the key range to fetch.
  /// @param [revision]            the point-in-time of the key-value store to use for the range. If revision is less or equal to zero, the range is over the latest key-value store. If the revision is compacted, ErrCompacted is returned as a response.
  /// @param [serializable]        sets the range request to use serializable member-local reads. By default, Range is linearizable; it reflects the current consensus of the cluster. For better performance and availability, in exchange for possible stale reads, a serializable range request is served locally without needing to reach consensus with other nodes in the cluster.
  @override
  Future<T> fetch<T>(String key, {int revision, bool serializable, CallOptions options}) {
    var request = _requestBuilder._fetchRequest(key, revision: revision, serializable: serializable);

    return _delegate.range(request, options: options)
      .then((rawResponse  ) => new EtcdFetchRangeResponse(rawResponse, _codec))
      .then((rangeResponse) => (rangeResponse.kvs.isNotEmpty ? rangeResponse.kvs.first.value : null));
  }

  /// Fetch the values stored for multiple keys
  ///
  /// @param [key], [rangeEnd]     the key range to fetch (inclusive)
  /// @param [limit]               the maximum number of keys returned for the request; When set to 0, it is treated as no limit.
  /// @param [revision]            the point-in-time of the key-value store to use for the range; If <= zero, the range is over the latest key-value store. If the revision is compacted, ErrCompacted is returned as a response.
  /// @param [sort_Order]          the ordering for sorted requests
  /// @param [sort_Target]         the key-value field to sort
  /// @param [serializable]        sets the range request to use serializable member-local reads; By default, Range is linearizable; it reflects the current consensus of the cluster. For better performance and availability, in exchange for possible stale reads, a serializable range request is served locally without needing to reach consensus with other nodes in the cluster.
  /// @param [keys_Only]           return only the keys and not the values
  /// @param [count_Only]          return only the count of the keys in the range
  /// @param [min_Mod_Revision]    the lower bound for key mod revisions; filters out lesser mod revisions
  /// @param [max_Mod_Revision]    the upper bound for key mod revisions; filters out greater mod revisions
  /// @param [min_Create_Revision] the lower bound for key create revisions; filters out lesser create revisions
  /// @param [max_Create_Revision] the upper bound for key create revisions; filters out greater create revisions
  @override
  Future<EtcdFetchRangeResponse> fetchRange(String key, String rangeEnd, {
      int                     limit,
      int                     revision,
      bool                    serializable,
      bool                    keysOnly,
      bool                    countOnly,
      int                     minModRevision,
      int                     maxModRevision,
      int                     minCreateRevision,
      int                     maxCreateRevision,
      RangeRequest_SortOrder  sortOrder,
      RangeRequest_SortTarget sortTarget,
      CallOptions             options
  }) {

    var request = _requestBuilder._fetchRangeRequest(key, rangeEnd,
      limit            : limit,
      revision         : revision,
      serializable     : serializable,
      keysOnly         : keysOnly,
      countOnly        : countOnly,
      minModRevision   : minModRevision,
      maxModRevision   : maxModRevision,
      minCreateRevision: minCreateRevision,
      maxCreateRevision: maxCreateRevision,
      sortOrder        : sortOrder,
      sortTarget       : sortTarget,
    );

    return _delegate.range(request, options: options)
      .then((response) => new EtcdFetchRangeResponse(response, _codec));
  }

  /// Fetch the values stored for all keys starting with [prefix]
  ///
  /// @param [prefix]              the prefix for which to fetch all keys. Passing a value of [null] will fetch all keys
  /// @param [limit]               the maximum number of keys returned for the request; When set to 0, it is treated as no limit.
  /// @param [revision]            the point-in-time of the key-value store to use for the range; If <= zero, the range is over the latest key-value store. If the revision is compacted, ErrCompacted is returned as a response.
  /// @param [sort_Order]          the ordering for sorted requests
  /// @param [sort_Target]         the key-value field to sort
  /// @param [serializable]        sets the range request to use serializable member-local reads; By default, Range is linearizable; it reflects the current consensus of the cluster. For better performance and availability, in exchange for possible stale reads, a serializable range request is served locally without needing to reach consensus with other nodes in the cluster.
  /// @param [keys_Only]           return only the keys and not the values
  /// @param [count_Only]          return only the count of the keys in the range
  /// @param [min_Mod_Revision]    the lower bound for key mod revisions; filters out lesser mod revisions
  /// @param [max_Mod_Revision]    the upper bound for key mod revisions; filters out greater mod revisions
  /// @param [min_Create_Revision] the lower bound for key create revisions; filters out lesser create revisions
  /// @param [max_Create_Revision] the upper bound for key create revisions; filters out greater create revisions
  @override
  Future<EtcdFetchRangeResponse> fetchPrefix(String prefix, {
      int                     limit,
      int                     revision,
      bool                    serializable,
      bool                    keysOnly,
      bool                    countOnly,
      int                     minModRevision,
      int                     maxModRevision,
      int                     minCreateRevision,
      int                     maxCreateRevision,
      RangeRequest_SortOrder  sortOrder,
      RangeRequest_SortTarget sortTarget,
      CallOptions             options
  }) {

    var request = _requestBuilder._fetchRangeRequest(prefix, null,
      limit            : limit,
      revision         : revision,
      serializable     : serializable,
      keysOnly         : keysOnly,
      countOnly        : countOnly,
      minModRevision   : minModRevision,
      maxModRevision   : maxModRevision,
      minCreateRevision: minCreateRevision,
      maxCreateRevision: maxCreateRevision,
      sortOrder        : sortOrder,
      sortTarget       : sortTarget,
      asPrefix         : true,
    );

    return _delegate.range(request, options: options)
      .then((response) => new EtcdFetchRangeResponse(response, _codec));
  }

  /// Delete the value stored for a single key
  ///
  /// @param [key]    the key to remove
  /// @param [prevKv] when set, return the contents of the deleted key-value pairs
  @override
  Future<EtcdKeyValue> delete(String key, {bool prevKv = true, CallOptions options}) {
    return deleteRange(key, null, prevKv: prevKv, options: options)
      .then((rangeResponse) => (rangeResponse.prevKvs.isNotEmpty ? rangeResponse.prevKvs.first : null));
  }

  /// Delete the values stored for multiple keys
  ///
  /// @param [key], [rangeEnd] the key range to remove (inclusive)
  /// @param [prevKv]          when set, return the contents of the deleted key-value pairs
  @override
  Future<EtcdDeleteRangeResponse> deleteRange(String key, String rangeEnd, {bool prevKv = true, CallOptions options}) {
    var request = _requestBuilder._deleteRangeRequest(key, rangeEnd, prevKv: prevKv);

    return _delegate.deleteRange(request, options: options).then((response) => new EtcdDeleteRangeResponse(response, _codec));
  }

  /// Delete the values stored for all keys starting with [prefix]
  ///
  /// @param [prefix] the prefix for which to delete all keys. Passing a value of [null] will delete all keys
  /// @param [prevKv] when set, return the contents of the deleted key-value pairs
  @override
  Future<EtcdDeleteRangeResponse> deletePrefix(String prefix, {bool prevKv = true, CallOptions options}) {
    var request = _requestBuilder._deleteRangeRequest(prefix, null, prevKv: prevKv, asPrefix: true);

    return _delegate.deleteRange(request, options: options).then((response) => new EtcdDeleteRangeResponse(response, _codec));
  }

  /// Compacts the event history in the etcd key-value store up to a given revision.
  /// The key-value store should be periodically compacted or the event history will continue to grow indefinitely.
  /// All superseded keys with a revision less than the compaction revision will be removed.
  ///
  /// @param [revision] the revision up to which all previous history for a key will be compacted.
  Future<void> compact(int revision, {CallOptions options}) async {
    var request = new CompactionRequest()..revision = Int64(revision);

    try {
      await _delegate.compact(request, options: options);
    } catch (e) {
      if (e is GrpcError && e.code == StatusCode.outOfRange && e.message.contains('revision has been compacted')) {
        // --- already compacted that revision ---
        // that's ok, ignore it and carry on
      } else {
        rethrow;
      }
    }
  }

  // ignore: use_to_and_as_if_applicable
  EtcdTransaction startTransaction() {
    return new EtcdTransaction._(this);
  }

  Future<EtcdTransactionResponse> _sendTransaction(EtcdTransaction transaction, {CallOptions options}) {
    return _delegate.txn(transaction._request).then((r) => new EtcdTransactionResponse(r, super._codec));
  }
}

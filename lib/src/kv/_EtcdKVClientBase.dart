part of etcd;

abstract class _EtcdKVClientBase {
  dynamic put(String key, String value, {int lease, bool prevKv = true, bool ignoreValue, bool ignoreLease});

  dynamic fetch<T>(   String key,                  {           int revision, bool serializable});
  dynamic fetchRange( String key, String rangeEnd, {int limit, int revision, bool serializable, bool keysOnly, bool countOnly, int minModRevision, int maxModRevision, int minCreateRevision, int maxCreateRevision, RangeRequest_SortOrder sortOrder, RangeRequest_SortTarget sortTarget});
  dynamic fetchPrefix(String key,                  {int limit, int revision, bool serializable, bool keysOnly, bool countOnly, int minModRevision, int maxModRevision, int minCreateRevision, int maxCreateRevision, RangeRequest_SortOrder sortOrder, RangeRequest_SortTarget sortTarget});

  dynamic delete(      String key,                  {bool prevKv = true});
  dynamic deleteRange( String key, String rangeEnd, {bool prevKv = true});
  dynamic deletePrefix(String key,                  {bool prevKv = true});
}


part of etcd;

class _EtcdStreamCache {
  final StreamController<GeneratedMessage> requestStream;
  final ResponseStream<GeneratedMessage>   responseStream;
  final Stream<_EtcdResponse>              transformedResponseStream;

  _EtcdStreamCache(this.requestStream, this.responseStream, this.transformedResponseStream);
}

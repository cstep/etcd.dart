part of etcd;

class EtcdLeaseKeepAliveResponse extends _EtcdLeaseResponse {
  EtcdLeaseKeepAliveResponse(LeaseKeepAliveResponse response): super(response);
}

part of etcd;

class EtcdLeaseGrantResponse extends _EtcdLeaseResponse {
  @override LeaseGrantResponse get _delegate => super._delegate;

  final EtcdLeaseClient _leaseClient;

  EtcdLeaseGrantResponse(LeaseGrantResponse response, EtcdLeaseClient client): _leaseClient = client, super(response);

  String get error => _delegate.error;

  Future<EtcdLeaseKeepAliveResponse> keepAlive() => _leaseClient.keepAlive(this.id).first;

  Future<EtcdLeaseTimeToLiveResponse> timeToLive({bool listKeys}) => _leaseClient.timeToLive(this.id, listKeys: listKeys);

  Future<EtcdLeaseRevokeResponse> revoke() => _leaseClient.revoke(this.id);
}

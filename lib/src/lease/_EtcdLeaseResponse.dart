part of etcd;

abstract class _EtcdLeaseResponse extends _EtcdResponse {
  _EtcdLeaseResponse(GeneratedMessage response): super(response, null);

  /// the lease ID for the granted lease.
  int get id => ((_delegate as dynamic).iD as Int64).toInt();

  /// the server selected time-to-live, in seconds
  Duration get ttl => new Duration(seconds: ((_delegate as dynamic).tTL as Int64).toInt());
}

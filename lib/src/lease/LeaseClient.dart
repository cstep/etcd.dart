part of etcd;

class EtcdLeaseClient extends _EtcdSubClient /*implements grpc.LeaseClient*/ {
  @override LeaseClient get _delegate => super._delegate;

  final Map<int, _KeepAliveStreamCache> _keepAliveStreams = {};

  EtcdLeaseClient(ClientChannel channel, Encoding encoding): super(new LeaseClient(channel), encoding);

  /// Create a new lease
  ///
  /// @param [ttlSeconds]  the advisory time-to-live, in seconds
  /// @param [requestedId] the requested ID for the lease; if set to 0, etcd will choose an ID
  Future<EtcdLeaseGrantResponse> grant(Duration ttlSeconds, {int requestedId, CallOptions options}) {
    assert(ttlSeconds.inSeconds > 0 && ((ttlSeconds.inMilliseconds / Duration.millisecondsPerSecond) == ttlSeconds.inSeconds));

    var request = new LeaseGrantRequest();

    request.tTL = Int64(ttlSeconds.inSeconds);
    if (requestedId != null) request.iD = Int64(requestedId);

    return _delegate.leaseGrant(request, options: options)
      .then((response) => new EtcdLeaseGrantResponse(response, this));
  }

  /// Refresh a lease to prevent it from expiring
  ///
  /// @param [id] the lease ID for the lease to keep alive
  Stream<EtcdLeaseKeepAliveResponse> keepAlive(int id, {CallOptions options}) {
    var cache = _keepAliveStreams.putIfAbsent(id, () { // ignore: close_sinks
      var requestStream             = new StreamController<LeaseKeepAliveRequest>();                    // -- these are closed when `revoke()` is called // ignore: close_sinks
      var responseStream            = _delegate.leaseKeepAlive(requestStream.stream, options: options); // -- these are closed when `revoke()` is called // ignore: close_sinks
      var transformedResponseStream = responseStream.map((response) => new EtcdLeaseKeepAliveResponse(response)).asBroadcastStream();

      return new _KeepAliveStreamCache(requestStream, responseStream, transformedResponseStream);
    });

    cache.requestStream.add(LeaseKeepAliveRequest()..iD = Int64(id));

    return cache.transformedResponseStream;
  }

  /// Retrieve the remaining timeToLive until a lease expires
  ///
  /// @param [id]       the lease ID for the lease to retrieve
  /// @param [listKeys] whether or not to return the keys bound to this lease
  Future<EtcdLeaseTimeToLiveResponse> timeToLive(int id, {bool listKeys, CallOptions options}) {
    var request = new LeaseTimeToLiveRequest();

    request.iD = Int64(id);
    if (listKeys != null) request.keys = listKeys;

    return _delegate.leaseTimeToLive(request, options: options)
      .then((response) => new EtcdLeaseTimeToLiveResponse(response, _codec));
  }

  /// Revoke/expire a lease. When the lease is revoked, all attached keys are deleted.
  ///
  /// @param [id] the lease ID for the lease to revoke
  Future<EtcdLeaseRevokeResponse> revoke(int id, {CallOptions options}) {
    var request = new LeaseRevokeRequest()..iD = Int64(id);

    return _delegate.leaseRevoke(request, options: options)
      .then((response) {
        _closeKeepAlive(id);

        return new EtcdLeaseRevokeResponse(response);
      });
  }

  /// List all existing lease IDs
  ///
  /// NOTE: this is currently listed in etcd's protobuf, but the matching server version (server: 3.4.7, api: 3.4.0) doesn't seem to support it
  Future<EtcdLeaseListResponse> list({CallOptions options}) {
    var request = new LeaseLeasesRequest();

    return _delegate.leaseLeases(request, options: options)
      .then((response) => new EtcdLeaseListResponse(response));
  }

  /// close the keepAlive streams when we're done with them
  ///
  /// @param [leaseId] the leaseId identifying the keepAlive stream to shutdown
  Future<void> _closeKeepAlive(int leaseId) async {
    var cache = _keepAliveStreams[leaseId];

    if (cache != null) {
      return cache.responseStream.cancel().then((_) => _keepAliveStreams.remove(leaseId));
    }
  }

  /// close all keepAlive streams
  Future<void> shutdown() {
    return Future.wait(_keepAliveStreams.keys.map(_closeKeepAlive));
  }
}

class _KeepAliveStreamCache implements _EtcdStreamCache {
  @override StreamController<LeaseKeepAliveRequest> requestStream;
  @override ResponseStream<LeaseKeepAliveResponse>  responseStream;
  @override Stream<EtcdLeaseKeepAliveResponse>      transformedResponseStream;

  _KeepAliveStreamCache(this.requestStream, this.responseStream, this.transformedResponseStream);
}

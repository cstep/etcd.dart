part of etcd;

class EtcdLeaseTimeToLiveResponse extends _EtcdLeaseResponse {
  @override LeaseTimeToLiveResponse get _delegate => super._delegate;

  @override final Encoding _codec;

  EtcdLeaseTimeToLiveResponse(LeaseTimeToLiveResponse response, Encoding encoding): _codec = encoding, super(response);

  /// the server selected time-to-live, in seconds
  int get grantedTTL => _delegate.grantedTTL.toInt();

  /// the keys attached to (expire with) this lease
  List<String> get keys => _delegate.keys.map(_codec.decode).toList();
}

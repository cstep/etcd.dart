part of etcd;

class EtcdLeaseListResponse extends _EtcdLeaseResponse {
  @override LeaseLeasesResponse get _delegate => super._delegate;

  EtcdLeaseListResponse(LeaseLeasesResponse response): super(response);

  /// a list of lease ids that exist on the server
  List<int> get leases => _delegate.leases.map((leaseStatus) => leaseStatus.iD.toInt()).toList();
}

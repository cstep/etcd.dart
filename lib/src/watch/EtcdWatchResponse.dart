part of etcd;

class EtcdWatchResponse extends _EtcdWatchResponseBase {
  @override WatchResponse get _delegate => super._delegate;

  final EtcdWatchClient _watchClient;

  EtcdWatchResponse(WatchResponse response, Encoding encoding, EtcdWatchClient client): _watchClient = client, super(response, encoding);

  int    get id              => _delegate.watchId.toInt();
  bool   get created         => _delegate.created;
  bool   get canceled        => _delegate.canceled;
  int    get compactRevision => _delegate.compactRevision.toInt();
  String get cancelReason    => _delegate.cancelReason;
  bool   get fragment        => _delegate.fragment;

  List<EtcdWatchEvent> _events;
  List<EtcdWatchEvent> get events => _events??=_delegate.events.map((event) => new EtcdWatchEvent(event, _codec)).toList();

  Future<EtcdWatchResponse> cancel() => _watchClient.cancel(this.id);
}

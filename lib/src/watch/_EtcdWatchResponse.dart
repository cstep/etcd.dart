part of etcd;

abstract class _EtcdWatchResponseBase extends _EtcdResponse {
  _EtcdWatchResponseBase(GeneratedMessage response, Encoding encoding): super(response, encoding);
}

part of etcd;

class EtcdWatcher {
  final Stream<EtcdWatchEvent> stream;
  final int                    id;

  static final StreamTransformer<EtcdWatchResponse, EtcdWatchEvent> _eventTransformer = new StreamTransformer.fromHandlers(
    handleData: (watchResponse, sink) => watchResponse.events.forEach(sink.add),

    handleError: (error, stack, sink) {
      if (error is! GrpcError || (error as GrpcError).code != StatusCode.cancelled) { // -- not a grpc cancelled error
        sink.addError(error, stack);
      }
    }
  );

  EtcdWatcher(this.id, Stream<EtcdWatchResponse> stream): stream = stream.transform(_eventTransformer);

  StreamSubscription<T> listen<T extends EtcdWatchEvent>(void Function(T event) onData, {Function onError, void Function() onDone, bool cancelOnError}) {
    return stream.listen(onData, onError: onError, onDone: onDone);
  }
}

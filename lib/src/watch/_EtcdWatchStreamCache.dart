part of etcd;

class _EtcdWatchStreamCache implements _EtcdStreamCache {
  @override StreamController<WatchRequest> requestStream;
  @override ResponseStream<WatchResponse>  responseStream;
  @override Stream<EtcdWatchResponse>      transformedResponseStream;

  _EtcdWatchStreamCache(this.requestStream, this.responseStream, this.transformedResponseStream);
}



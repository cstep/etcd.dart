part of etcd;

class EtcdWatchClient extends _EtcdSubClient /*implements grpc.WatchClient*/ {
  @override WatchClient get _delegate => super._delegate;

  final Map<int, _EtcdWatchStreamCache> _watchStreams = {};

  EtcdWatchClient(ClientChannel channel, Encoding encoding): super(new WatchClient(channel), encoding);

  /// create a watcher on a single key
  Future<EtcdWatcher> single(String key, {int startRevision, bool progressNotify, List<EtcdWatchFilter> filters, bool prevKv, int watchId, bool fragment, CallOptions options}) {
    return range(key, null, startRevision: startRevision, progressNotify: progressNotify, filters: filters, prevKv: prevKv, watchId: watchId, fragment: fragment, options: options);
  }

  /// create a watcher on all keys that start with [prefix]
  ///
  /// @param [prefix]          the prefix for which to watch all keys
  /// @param [startRevision]   revision for where to inclusively begin watching; if not given, it will stream events following the current revision. The entire available event history can be watched starting from the last compaction revision.
  /// @param [progressNotify]  when set, the watch will periodically receive a WatchResponse with no events, if there are no recent events. It is useful when clients wish to recover a disconnected watcher starting from a recent known revision. The etcd server decides how often to send notifications based on current server load.
  /// @param [filters]         a list of event types to filter away at server side.
  /// @param [prevKv]          when set, the watch receives the key-value data from before the event happens. This is useful for knowing what data has been overwritten.
  /// @param [fragment]        enables splitting large revisions into multiple watch responses
  /// @param [watchId]         if provided and non-zero, it will be assigned to this watcher.
  ///                          Since creating a watcher in etcd is not a synchronous operation, this can be used ensure that ordering is correct when creating multiple watchers on the same stream.
  ///                          Creating a watcher with an ID already in use on the stream will cause an error to be returned.
  Future<EtcdWatcher> prefix(String prefix, {
      int                   startRevision,
      bool                  progressNotify,
      List<EtcdWatchFilter> filters,
      bool                  prevKv,
      int                   watchId,
      bool                  fragment,
      CallOptions           options
  }) async {
    // -------- prepare request
    var request = _watchPrefixRequest(prefix,
      startRevision : startRevision,
      progressNotify: progressNotify,
      filters       : filters,
      prevKv        : prevKv,
      watchId       : watchId,
      fragment      : fragment,
    );

    return _createWatch(request, options: options);
  }

  /// create a watcher on a range of keys
  ///
  /// @param [key], [rangeEnd] the key range to watch
  /// @param [startRevision]   revision for where to inclusively begin watching; if not given, it will stream events following the current revision. The entire available event history can be watched starting from the last compaction revision.
  /// @param [progressNotify]  when set, the watch will periodically receive a WatchResponse with no events, if there are no recent events. It is useful when clients wish to recover a disconnected watcher starting from a recent known revision. The etcd server decides how often to send notifications based on current server load.
  /// @param [filters]         a list of event types to filter away at server side.
  /// @param [prevKv]          when set, the watch receives the key-value data from before the event happens. This is useful for knowing what data has been overwritten.
  /// @param [fragment]        enables splitting large revisions into multiple watch responses
  /// @param [watchId]         if provided and non-zero, it will be assigned to this watcher.
  ///                          Since creating a watcher in etcd is not a synchronous operation, this can be used ensure that ordering is correct when creating multiple watchers on the same stream.
  ///                          Creating a watcher with an ID already in use on the stream will cause an error to be returned.
  Future<EtcdWatcher> range(String key, String rangeEnd, {
      int                   startRevision,
      bool                  progressNotify,
      List<EtcdWatchFilter> filters,
      bool                  prevKv,
      int                   watchId,
      bool                  fragment,
      CallOptions           options,
  }) async {

    // -------- prepare request
    var request = _watchRangeRequest(key, rangeEnd,
      startRevision : startRevision,
      progressNotify: progressNotify,
      filters       : filters,
      prevKv        : prevKv,
      watchId       : watchId,
      fragment      : fragment,
    );

    return _createWatch(request, options: options);
  }

  /// cancel a watch and close its streams
  ///
  /// @param [watchId] the watch ID for the watch to cancel
  Future<void> cancel(int watchId, {CallOptions options}) async {
    var cache = _cacheFor(watchId, options: options);

    // -------- make the request
    var request = new WatchCancelRequest()..watchId = Int64(watchId);
    cache.requestStream.add(new WatchRequest()..cancelRequest = request);

    await cache.requestStream.close();
    await _closeWatchStream(watchId);
  }

  /// requests that a watch stream progress status be sent in the watch response stream as soon as possible.
  ///
  /// @param [watchId] the watch ID for the watch to report on
  Future<EtcdWatchResponse> progress(int watchId, {CallOptions options}) {
    var cache = _cacheFor(watchId, options: options);

    // -------- make the request
    cache.requestStream.add(new WatchRequest()..progressRequest = new WatchProgressRequest());

    return cache.transformedResponseStream.first;
  }

  /// get the response stream for a given watchId
  Stream<EtcdWatchResponse> streamFor(int watchId, {bool createIfNotExists = false, CallOptions options}) {
    return _cacheFor(watchId, createIfNotExists: createIfNotExists, options: options)?.transformedResponseStream;
  }

  /// close all watch streams
  Future<void> shutdown() {
    return Future.wait(_watchStreams.keys.map(cancel).toList()/*..addAll(_subscriptions.map((s) => s.cancel()))*/);
  }

  // ---

  Future<EtcdWatcher> _createWatch(WatchCreateRequest request, {CallOptions options}) async {
    var watchId = request.watchId.toInt();

    // -------- set up our streams
    _EtcdWatchStreamCache cache;
    if (watchId != null) {
      cache = _cacheFor(watchId, options: options);
    } else {
      cache = _createStreamCache(options: options);
    }

    // -------- make the request
    cache.requestStream.add(new WatchRequest()..createRequest = request);

    if (watchId == null) { // -- still need to register our streams
      await cache.transformedResponseStream.first.then((response) { // -- wait for our watchId to come back from server
        watchId = response.id;
        _watchStreams.putIfAbsent(watchId, () => cache);
      });
    }

    return new EtcdWatcher(watchId, cache.transformedResponseStream);
  }

  WatchCreateRequest _watchPrefixRequest(String prefix, {
      int                   startRevision,
      bool                  progressNotify,
      List<EtcdWatchFilter> filters,
      bool                  prevKv,
      int                   watchId,
      bool                  fragment
  }) {
    return _watchRangeRequest(prefix, null,
      startRevision : startRevision,
      progressNotify: progressNotify,
      filters       : filters,
      prevKv        : prevKv,
      watchId       : watchId,
      fragment      : fragment,
      asPrefix      : true,
    );
  }

  WatchCreateRequest _watchRangeRequest(String key, String rangeEnd, {
      int                   startRevision,
      bool                  progressNotify,
      List<EtcdWatchFilter> filters,
      bool                  prevKv,
      int                   watchId,
      bool                  fragment,
      bool                  asPrefix = false,
  }) {
    var request = new WatchCreateRequest();

    if (key       != null) request.key = _codec.encode(key);
    if (rangeEnd  != null) {
      request.rangeEnd = _codec.encode(rangeEnd)..last += 1; // -- make the range INCLUSIVE of `rangeEnd`
    } else if (asPrefix == true) {
      if (key == null) { // -- both start and end are null
        // -- fetch everything
        request.key      = [0];
        request.rangeEnd = [0];
      } else {
        request.rangeEnd = List.from(request.key)..last += 1; // -- treat `key` like a prefix
      }
    }

    if (startRevision  != null) request.startRevision  = Int64(startRevision);
    if (progressNotify != null) request.progressNotify = progressNotify;
    if (prevKv         != null) request.prevKv         = prevKv;
    if (watchId        != null) request.watchId        = Int64(watchId);
    if (fragment       != null) request.fragment       = fragment;
    if (filters        != null) request.filters.addAll(filters);

    return request;
  }

  /// get the stream cache for a given watchId
  _EtcdWatchStreamCache _cacheFor(int watchId, {bool createIfNotExists = true, CallOptions options}) {
    if (createIfNotExists) {
      return _watchStreams.putIfAbsent(watchId, () => _createStreamCache(options: options));
    } else {
      return _watchStreams[watchId];
    }
  }

  _EtcdWatchStreamCache _createStreamCache({CallOptions options}) {
    var requestStream     = new StreamController<WatchRequest>();                    // -- these are closed when `cancel()` is called // ignore: close_sinks
    var responseStream    = _delegate.watch(requestStream.stream, options: options); // -- these are closed when `cancel()` is called // ignore: close_sinks
    var transformedStream = responseStream.map<EtcdWatchResponse>((response) => new EtcdWatchResponse(response, _codec, this)).asBroadcastStream();

    return new _EtcdWatchStreamCache(requestStream, responseStream, transformedStream);
  }

  Future<void> _closeWatchStream(int watchId) async {
    var cache = _watchStreams[watchId];

    if (cache != null) {
      return Future(() => cache.responseStream.cancel().then((_) => _watchStreams.remove(watchId)));
    }
  }
}

part of etcd;

class EtcdWatchEvent extends _EtcdWatchResponseBase /*implements grpc.Event*/ {
  @override Event get _delegate => super._delegate;

  EtcdWatchEvent(Event event, Encoding encoding) : super(event, encoding);

  /// the kind of event. If type is a PUT, it indicates new data has been stored to the key. If type is a DELETE, it indicates the key was
  EtcdWatchEventType     _type;
  EtcdWatchEventType get  type => _type??=new EtcdWatchEventType._from(_delegate.type);

  /// holds the KeyValue for the event.
  /// A PUT event contains current kv pair. A PUT event with kv.Version=1 indicates the creation of a key.
  /// A DELETE/EXPIRE event contains the deleted key with its modification revision set to the revision of deletion.
  EtcdKeyValue     _kv;
  EtcdKeyValue get  kv => _kv??=new EtcdKeyValue(_delegate.kv, _codec);

  /// holds the key-value pair before the event happens
  EtcdKeyValue     _prevKv;
  EtcdKeyValue get  prevKv => _prevKv??=new EtcdKeyValue(_delegate.prevKv, _codec);
}

/// this class is a wrapper and is PURELY to create a better/shorter class name than Event_EventType
class EtcdWatchEventType implements Event_EventType {
  final Event_EventType _delegate;

  static const EtcdWatchEventType PUT    = EtcdWatchEventType._(Event_EventType.PUT);
  static const EtcdWatchEventType DELETE = EtcdWatchEventType._(Event_EventType.DELETE);

  @override String get name  => _delegate.name;
  @override int    get value => _delegate.value;

  factory EtcdWatchEventType._from(Event_EventType rawEvent) {
    switch(rawEvent) {
      case Event_EventType.PUT   : return PUT;
      case Event_EventType.DELETE: return DELETE;
      default                    : throw new ArgumentError.value(rawEvent, 'input event', 'unrecognized EventType');
    }
  }
  const EtcdWatchEventType._(Event_EventType delegate): _delegate = delegate;
}

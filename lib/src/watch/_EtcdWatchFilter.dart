part of etcd;

/// this class is a wrapper and is PURELY to create a better/shorter class name than WatchCreateRequest_FilterType
class EtcdWatchFilter implements WatchCreateRequest_FilterType {
  final WatchCreateRequest_FilterType _delegate;

  static const EtcdWatchFilter NOPUT    = EtcdWatchFilter._(WatchCreateRequest_FilterType.NOPUT);
  static const EtcdWatchFilter NODELETE = EtcdWatchFilter._(WatchCreateRequest_FilterType.NODELETE);

  @override String get name  => _delegate.name;
  @override int    get value => _delegate.value;

  static const List<EtcdWatchFilter> values = <EtcdWatchFilter> [
    NOPUT,
    NODELETE,
  ];

  const EtcdWatchFilter._(WatchCreateRequest_FilterType delegate): _delegate = delegate;
}

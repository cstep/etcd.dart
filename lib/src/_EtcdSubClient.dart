part of etcd;

abstract class _EtcdSubClient implements Client {
  Client   _delegate; // ignore: prefer_final_fields
  Encoding _codec;    // ignore: prefer_final_fields

  _EtcdSubClient(Client delegate, Encoding encoding): _delegate = delegate, _codec = encoding;

  @override
  @Deprecated(r'''This method does not invoke interceptors and is superseded
by $createStreamingCall and $createUnaryCall which invoke interceptors.

If you are getting this warning in autogenerated protobuf client stubs,
regenerate these stubs using  protobuf compiler plugin version 19.2.0 or newer.
''')
  ClientCall<Q, R> $createCall<Q, R>(ClientMethod<Q, R> method, Stream<Q> requests, {CallOptions/*?*/ options}) {
    return _delegate.$createCall(method, requests, options: options);
  }

  @override
  ResponseFuture<R> $createUnaryCall<Q, R>(ClientMethod<Q, R> method, Q request, {CallOptions/*?*/ options}) {
    return _delegate.$createUnaryCall<Q, R>(method, request, options: options);
  }

  @override
  ResponseStream<R> $createStreamingCall<Q, R>(ClientMethod<Q, R> method, Stream<Q> requests, {CallOptions/*?*/ options}) {
    return _delegate.$createStreamingCall<Q, R>(method, requests, options: options);
  }
}

part of etcd;

class EtcdClient {
  final ClientChannel _channel;
  final Encoding      _encoding;

  EtcdKVClient    _kvClient;
  EtcdKVClient    get kv => _kvClient??=new EtcdKVClient(_channel, _encoding);

  EtcdWatchClient _watchClient;
  EtcdWatchClient get watch => _watchClient??=new EtcdWatchClient(_channel, _encoding);

  EtcdLeaseClient _leaseClient;
  EtcdLeaseClient get lease => _leaseClient??=new EtcdLeaseClient(_channel, _encoding);

  Completer _shutdownCompleter;
  bool get isShutdown     => _shutdownCompleter != null && _shutdownCompleter.isCompleted;
  bool get isShuttingDown => _shutdownCompleter != null && !_shutdownCompleter.isCompleted;

  EtcdClient(String host, {int port, Encoding encoding, ChannelOptions options}):
    _encoding = encoding??ascii,
    _channel  = new ClientChannel(host, port: port??443, options: options??ChannelOptions());

  /// close our connection and all our streams
  Future<void> shutdown() async {
    _shutdownCompleter = new Completer();

    Future.wait([ // ignore: unawaited_futures
      _channel.shutdown(),
      lease.shutdown(),
      watch.shutdown(),
    ])
    .then(_shutdownCompleter.complete)
    .catchError(_shutdownCompleter.completeError);

    return _shutdownCompleter.future;
  }

  Future getConnection() => _channel.getConnection();
}

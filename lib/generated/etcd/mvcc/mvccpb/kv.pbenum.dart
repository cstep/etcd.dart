// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: etcd/mvcc/mvccpb/kv.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Event_EventType extends $pb.ProtobufEnum {
  static const Event_EventType PUT = Event_EventType._(0, 'PUT');
  static const Event_EventType DELETE = Event_EventType._(1, 'DELETE');

  static const $core.List<Event_EventType> values = <Event_EventType>[
    PUT,
    DELETE,
  ];

  static final $core.Map<$core.int, Event_EventType> _byValue =
      $pb.ProtobufEnum.initByValue(values);
  static Event_EventType valueOf($core.int value) => _byValue[value];

  const Event_EventType._($core.int v, $core.String n) : super(v, n);
}

// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: etcd/mvcc/mvccpb/kv.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const KeyValue$json = const {
  '1': 'KeyValue',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 12, '10': 'key'},
    const {
      '1': 'create_revision',
      '3': 2,
      '4': 1,
      '5': 3,
      '10': 'createRevision'
    },
    const {'1': 'mod_revision', '3': 3, '4': 1, '5': 3, '10': 'modRevision'},
    const {'1': 'version', '3': 4, '4': 1, '5': 3, '10': 'version'},
    const {'1': 'value', '3': 5, '4': 1, '5': 12, '10': 'value'},
    const {'1': 'lease', '3': 6, '4': 1, '5': 3, '10': 'lease'},
  ],
};

const Event$json = const {
  '1': 'Event',
  '2': const [
    const {
      '1': 'type',
      '3': 1,
      '4': 1,
      '5': 14,
      '6': '.mvccpb.Event.EventType',
      '10': 'type'
    },
    const {
      '1': 'kv',
      '3': 2,
      '4': 1,
      '5': 11,
      '6': '.mvccpb.KeyValue',
      '10': 'kv'
    },
    const {
      '1': 'prev_kv',
      '3': 3,
      '4': 1,
      '5': 11,
      '6': '.mvccpb.KeyValue',
      '10': 'prevKv'
    },
  ],
  '4': const [Event_EventType$json],
};

const Event_EventType$json = const {
  '1': 'EventType',
  '2': const [
    const {'1': 'PUT', '2': 0},
    const {'1': 'DELETE', '2': 1},
  ],
};

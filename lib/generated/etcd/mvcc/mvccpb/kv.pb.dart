// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: etcd/mvcc/mvccpb/kv.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'kv.pbenum.dart';

export 'kv.pbenum.dart';

class KeyValue extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('KeyValue',
      package: const $pb.PackageName('mvccpb'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, 'key', $pb.PbFieldType.OY)
    ..aInt64(2, 'createRevision')
    ..aInt64(3, 'modRevision')
    ..aInt64(4, 'version')
    ..a<$core.List<$core.int>>(5, 'value', $pb.PbFieldType.OY)
    ..aInt64(6, 'lease')
    ..hasRequiredFields = false;

  KeyValue._() : super();
  factory KeyValue() => create();
  factory KeyValue.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory KeyValue.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  KeyValue clone() => KeyValue()..mergeFromMessage(this);
  KeyValue copyWith(void Function(KeyValue) updates) =>
      super.copyWith((message) => updates(message as KeyValue));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static KeyValue create() => KeyValue._();
  KeyValue createEmptyInstance() => create();
  static $pb.PbList<KeyValue> createRepeated() => $pb.PbList<KeyValue>();
  @$core.pragma('dart2js:noInline')
  static KeyValue getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<KeyValue>(create);
  static KeyValue _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get key => $_getN(0);
  @$pb.TagNumber(1)
  set key($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get createRevision => $_getI64(1);
  @$pb.TagNumber(2)
  set createRevision($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasCreateRevision() => $_has(1);
  @$pb.TagNumber(2)
  void clearCreateRevision() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get modRevision => $_getI64(2);
  @$pb.TagNumber(3)
  set modRevision($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasModRevision() => $_has(2);
  @$pb.TagNumber(3)
  void clearModRevision() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get version => $_getI64(3);
  @$pb.TagNumber(4)
  set version($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasVersion() => $_has(3);
  @$pb.TagNumber(4)
  void clearVersion() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.int> get value => $_getN(4);
  @$pb.TagNumber(5)
  set value($core.List<$core.int> v) {
    $_setBytes(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasValue() => $_has(4);
  @$pb.TagNumber(5)
  void clearValue() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get lease => $_getI64(5);
  @$pb.TagNumber(6)
  set lease($fixnum.Int64 v) {
    $_setInt64(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasLease() => $_has(5);
  @$pb.TagNumber(6)
  void clearLease() => clearField(6);
}

class Event extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Event',
      package: const $pb.PackageName('mvccpb'), createEmptyInstance: create)
    ..e<Event_EventType>(1, 'type', $pb.PbFieldType.OE,
        defaultOrMaker: Event_EventType.PUT,
        valueOf: Event_EventType.valueOf,
        enumValues: Event_EventType.values)
    ..aOM<KeyValue>(2, 'kv', subBuilder: KeyValue.create)
    ..aOM<KeyValue>(3, 'prevKv', subBuilder: KeyValue.create)
    ..hasRequiredFields = false;

  Event._() : super();
  factory Event() => create();
  factory Event.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory Event.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  Event clone() => Event()..mergeFromMessage(this);
  Event copyWith(void Function(Event) updates) =>
      super.copyWith((message) => updates(message as Event));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Event create() => Event._();
  Event createEmptyInstance() => create();
  static $pb.PbList<Event> createRepeated() => $pb.PbList<Event>();
  @$core.pragma('dart2js:noInline')
  static Event getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Event>(create);
  static Event _defaultInstance;

  @$pb.TagNumber(1)
  Event_EventType get type => $_getN(0);
  @$pb.TagNumber(1)
  set type(Event_EventType v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  KeyValue get kv => $_getN(1);
  @$pb.TagNumber(2)
  set kv(KeyValue v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasKv() => $_has(1);
  @$pb.TagNumber(2)
  void clearKv() => clearField(2);
  @$pb.TagNumber(2)
  KeyValue ensureKv() => $_ensure(1);

  @$pb.TagNumber(3)
  KeyValue get prevKv => $_getN(2);
  @$pb.TagNumber(3)
  set prevKv(KeyValue v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasPrevKv() => $_has(2);
  @$pb.TagNumber(3)
  void clearPrevKv() => clearField(3);
  @$pb.TagNumber(3)
  KeyValue ensurePrevKv() => $_ensure(2);
}

// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: etcd/etcdserver/etcdserverpb/rpc.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import '../../mvcc/mvccpb/kv.pb.dart' as $2;
import '../../auth/authpb/auth.pb.dart' as $3;

import 'rpc.pbenum.dart';

export 'rpc.pbenum.dart';

class ResponseHeader extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ResponseHeader',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'clusterId', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(2, 'memberId', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..aInt64(3, 'revision')
    ..a<$fixnum.Int64>(4, 'raftTerm', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false;

  ResponseHeader._() : super();
  factory ResponseHeader() => create();
  factory ResponseHeader.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ResponseHeader.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  ResponseHeader clone() => ResponseHeader()..mergeFromMessage(this);
  ResponseHeader copyWith(void Function(ResponseHeader) updates) =>
      super.copyWith((message) => updates(message as ResponseHeader));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResponseHeader create() => ResponseHeader._();
  ResponseHeader createEmptyInstance() => create();
  static $pb.PbList<ResponseHeader> createRepeated() =>
      $pb.PbList<ResponseHeader>();
  @$core.pragma('dart2js:noInline')
  static ResponseHeader getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ResponseHeader>(create);
  static ResponseHeader _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get clusterId => $_getI64(0);
  @$pb.TagNumber(1)
  set clusterId($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasClusterId() => $_has(0);
  @$pb.TagNumber(1)
  void clearClusterId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get memberId => $_getI64(1);
  @$pb.TagNumber(2)
  set memberId($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasMemberId() => $_has(1);
  @$pb.TagNumber(2)
  void clearMemberId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get revision => $_getI64(2);
  @$pb.TagNumber(3)
  set revision($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasRevision() => $_has(2);
  @$pb.TagNumber(3)
  void clearRevision() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get raftTerm => $_getI64(3);
  @$pb.TagNumber(4)
  set raftTerm($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasRaftTerm() => $_has(3);
  @$pb.TagNumber(4)
  void clearRaftTerm() => clearField(4);
}

class RangeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RangeRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, 'key', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, 'rangeEnd', $pb.PbFieldType.OY)
    ..aInt64(3, 'limit')
    ..aInt64(4, 'revision')
    ..e<RangeRequest_SortOrder>(5, 'sortOrder', $pb.PbFieldType.OE,
        defaultOrMaker: RangeRequest_SortOrder.NONE,
        valueOf: RangeRequest_SortOrder.valueOf,
        enumValues: RangeRequest_SortOrder.values)
    ..e<RangeRequest_SortTarget>(6, 'sortTarget', $pb.PbFieldType.OE,
        defaultOrMaker: RangeRequest_SortTarget.KEY,
        valueOf: RangeRequest_SortTarget.valueOf,
        enumValues: RangeRequest_SortTarget.values)
    ..aOB(7, 'serializable')
    ..aOB(8, 'keysOnly')
    ..aOB(9, 'countOnly')
    ..aInt64(10, 'minModRevision')
    ..aInt64(11, 'maxModRevision')
    ..aInt64(12, 'minCreateRevision')
    ..aInt64(13, 'maxCreateRevision')
    ..hasRequiredFields = false;

  RangeRequest._() : super();
  factory RangeRequest() => create();
  factory RangeRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory RangeRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  RangeRequest clone() => RangeRequest()..mergeFromMessage(this);
  RangeRequest copyWith(void Function(RangeRequest) updates) =>
      super.copyWith((message) => updates(message as RangeRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RangeRequest create() => RangeRequest._();
  RangeRequest createEmptyInstance() => create();
  static $pb.PbList<RangeRequest> createRepeated() =>
      $pb.PbList<RangeRequest>();
  @$core.pragma('dart2js:noInline')
  static RangeRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<RangeRequest>(create);
  static RangeRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get key => $_getN(0);
  @$pb.TagNumber(1)
  set key($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get rangeEnd => $_getN(1);
  @$pb.TagNumber(2)
  set rangeEnd($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRangeEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearRangeEnd() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get limit => $_getI64(2);
  @$pb.TagNumber(3)
  set limit($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasLimit() => $_has(2);
  @$pb.TagNumber(3)
  void clearLimit() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get revision => $_getI64(3);
  @$pb.TagNumber(4)
  set revision($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasRevision() => $_has(3);
  @$pb.TagNumber(4)
  void clearRevision() => clearField(4);

  @$pb.TagNumber(5)
  RangeRequest_SortOrder get sortOrder => $_getN(4);
  @$pb.TagNumber(5)
  set sortOrder(RangeRequest_SortOrder v) {
    setField(5, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasSortOrder() => $_has(4);
  @$pb.TagNumber(5)
  void clearSortOrder() => clearField(5);

  @$pb.TagNumber(6)
  RangeRequest_SortTarget get sortTarget => $_getN(5);
  @$pb.TagNumber(6)
  set sortTarget(RangeRequest_SortTarget v) {
    setField(6, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasSortTarget() => $_has(5);
  @$pb.TagNumber(6)
  void clearSortTarget() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get serializable => $_getBF(6);
  @$pb.TagNumber(7)
  set serializable($core.bool v) {
    $_setBool(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasSerializable() => $_has(6);
  @$pb.TagNumber(7)
  void clearSerializable() => clearField(7);

  @$pb.TagNumber(8)
  $core.bool get keysOnly => $_getBF(7);
  @$pb.TagNumber(8)
  set keysOnly($core.bool v) {
    $_setBool(7, v);
  }

  @$pb.TagNumber(8)
  $core.bool hasKeysOnly() => $_has(7);
  @$pb.TagNumber(8)
  void clearKeysOnly() => clearField(8);

  @$pb.TagNumber(9)
  $core.bool get countOnly => $_getBF(8);
  @$pb.TagNumber(9)
  set countOnly($core.bool v) {
    $_setBool(8, v);
  }

  @$pb.TagNumber(9)
  $core.bool hasCountOnly() => $_has(8);
  @$pb.TagNumber(9)
  void clearCountOnly() => clearField(9);

  @$pb.TagNumber(10)
  $fixnum.Int64 get minModRevision => $_getI64(9);
  @$pb.TagNumber(10)
  set minModRevision($fixnum.Int64 v) {
    $_setInt64(9, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasMinModRevision() => $_has(9);
  @$pb.TagNumber(10)
  void clearMinModRevision() => clearField(10);

  @$pb.TagNumber(11)
  $fixnum.Int64 get maxModRevision => $_getI64(10);
  @$pb.TagNumber(11)
  set maxModRevision($fixnum.Int64 v) {
    $_setInt64(10, v);
  }

  @$pb.TagNumber(11)
  $core.bool hasMaxModRevision() => $_has(10);
  @$pb.TagNumber(11)
  void clearMaxModRevision() => clearField(11);

  @$pb.TagNumber(12)
  $fixnum.Int64 get minCreateRevision => $_getI64(11);
  @$pb.TagNumber(12)
  set minCreateRevision($fixnum.Int64 v) {
    $_setInt64(11, v);
  }

  @$pb.TagNumber(12)
  $core.bool hasMinCreateRevision() => $_has(11);
  @$pb.TagNumber(12)
  void clearMinCreateRevision() => clearField(12);

  @$pb.TagNumber(13)
  $fixnum.Int64 get maxCreateRevision => $_getI64(12);
  @$pb.TagNumber(13)
  set maxCreateRevision($fixnum.Int64 v) {
    $_setInt64(12, v);
  }

  @$pb.TagNumber(13)
  $core.bool hasMaxCreateRevision() => $_has(12);
  @$pb.TagNumber(13)
  void clearMaxCreateRevision() => clearField(13);
}

class RangeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RangeResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<$2.KeyValue>(2, 'kvs', $pb.PbFieldType.PM,
        subBuilder: $2.KeyValue.create)
    ..aOB(3, 'more')
    ..aInt64(4, 'count')
    ..hasRequiredFields = false;

  RangeResponse._() : super();
  factory RangeResponse() => create();
  factory RangeResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory RangeResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  RangeResponse clone() => RangeResponse()..mergeFromMessage(this);
  RangeResponse copyWith(void Function(RangeResponse) updates) =>
      super.copyWith((message) => updates(message as RangeResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RangeResponse create() => RangeResponse._();
  RangeResponse createEmptyInstance() => create();
  static $pb.PbList<RangeResponse> createRepeated() =>
      $pb.PbList<RangeResponse>();
  @$core.pragma('dart2js:noInline')
  static RangeResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<RangeResponse>(create);
  static RangeResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$2.KeyValue> get kvs => $_getList(1);

  @$pb.TagNumber(3)
  $core.bool get more => $_getBF(2);
  @$pb.TagNumber(3)
  set more($core.bool v) {
    $_setBool(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasMore() => $_has(2);
  @$pb.TagNumber(3)
  void clearMore() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get count => $_getI64(3);
  @$pb.TagNumber(4)
  set count($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasCount() => $_has(3);
  @$pb.TagNumber(4)
  void clearCount() => clearField(4);
}

class PutRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PutRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, 'key', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, 'value', $pb.PbFieldType.OY)
    ..aInt64(3, 'lease')
    ..aOB(4, 'prevKv')
    ..aOB(5, 'ignoreValue')
    ..aOB(6, 'ignoreLease')
    ..hasRequiredFields = false;

  PutRequest._() : super();
  factory PutRequest() => create();
  factory PutRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory PutRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  PutRequest clone() => PutRequest()..mergeFromMessage(this);
  PutRequest copyWith(void Function(PutRequest) updates) =>
      super.copyWith((message) => updates(message as PutRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PutRequest create() => PutRequest._();
  PutRequest createEmptyInstance() => create();
  static $pb.PbList<PutRequest> createRepeated() => $pb.PbList<PutRequest>();
  @$core.pragma('dart2js:noInline')
  static PutRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<PutRequest>(create);
  static PutRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get key => $_getN(0);
  @$pb.TagNumber(1)
  set key($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get value => $_getN(1);
  @$pb.TagNumber(2)
  set value($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get lease => $_getI64(2);
  @$pb.TagNumber(3)
  set lease($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasLease() => $_has(2);
  @$pb.TagNumber(3)
  void clearLease() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get prevKv => $_getBF(3);
  @$pb.TagNumber(4)
  set prevKv($core.bool v) {
    $_setBool(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasPrevKv() => $_has(3);
  @$pb.TagNumber(4)
  void clearPrevKv() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get ignoreValue => $_getBF(4);
  @$pb.TagNumber(5)
  set ignoreValue($core.bool v) {
    $_setBool(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasIgnoreValue() => $_has(4);
  @$pb.TagNumber(5)
  void clearIgnoreValue() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get ignoreLease => $_getBF(5);
  @$pb.TagNumber(6)
  set ignoreLease($core.bool v) {
    $_setBool(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasIgnoreLease() => $_has(5);
  @$pb.TagNumber(6)
  void clearIgnoreLease() => clearField(6);
}

class PutResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PutResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aOM<$2.KeyValue>(2, 'prevKv', subBuilder: $2.KeyValue.create)
    ..hasRequiredFields = false;

  PutResponse._() : super();
  factory PutResponse() => create();
  factory PutResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory PutResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  PutResponse clone() => PutResponse()..mergeFromMessage(this);
  PutResponse copyWith(void Function(PutResponse) updates) =>
      super.copyWith((message) => updates(message as PutResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PutResponse create() => PutResponse._();
  PutResponse createEmptyInstance() => create();
  static $pb.PbList<PutResponse> createRepeated() => $pb.PbList<PutResponse>();
  @$core.pragma('dart2js:noInline')
  static PutResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<PutResponse>(create);
  static PutResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $2.KeyValue get prevKv => $_getN(1);
  @$pb.TagNumber(2)
  set prevKv($2.KeyValue v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasPrevKv() => $_has(1);
  @$pb.TagNumber(2)
  void clearPrevKv() => clearField(2);
  @$pb.TagNumber(2)
  $2.KeyValue ensurePrevKv() => $_ensure(1);
}

class DeleteRangeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DeleteRangeRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, 'key', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, 'rangeEnd', $pb.PbFieldType.OY)
    ..aOB(3, 'prevKv')
    ..hasRequiredFields = false;

  DeleteRangeRequest._() : super();
  factory DeleteRangeRequest() => create();
  factory DeleteRangeRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DeleteRangeRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  DeleteRangeRequest clone() => DeleteRangeRequest()..mergeFromMessage(this);
  DeleteRangeRequest copyWith(void Function(DeleteRangeRequest) updates) =>
      super.copyWith((message) => updates(message as DeleteRangeRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteRangeRequest create() => DeleteRangeRequest._();
  DeleteRangeRequest createEmptyInstance() => create();
  static $pb.PbList<DeleteRangeRequest> createRepeated() =>
      $pb.PbList<DeleteRangeRequest>();
  @$core.pragma('dart2js:noInline')
  static DeleteRangeRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DeleteRangeRequest>(create);
  static DeleteRangeRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get key => $_getN(0);
  @$pb.TagNumber(1)
  set key($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get rangeEnd => $_getN(1);
  @$pb.TagNumber(2)
  set rangeEnd($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRangeEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearRangeEnd() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get prevKv => $_getBF(2);
  @$pb.TagNumber(3)
  set prevKv($core.bool v) {
    $_setBool(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasPrevKv() => $_has(2);
  @$pb.TagNumber(3)
  void clearPrevKv() => clearField(3);
}

class DeleteRangeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DeleteRangeResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aInt64(2, 'deleted')
    ..pc<$2.KeyValue>(3, 'prevKvs', $pb.PbFieldType.PM,
        subBuilder: $2.KeyValue.create)
    ..hasRequiredFields = false;

  DeleteRangeResponse._() : super();
  factory DeleteRangeResponse() => create();
  factory DeleteRangeResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DeleteRangeResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  DeleteRangeResponse clone() => DeleteRangeResponse()..mergeFromMessage(this);
  DeleteRangeResponse copyWith(void Function(DeleteRangeResponse) updates) =>
      super.copyWith((message) => updates(message as DeleteRangeResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteRangeResponse create() => DeleteRangeResponse._();
  DeleteRangeResponse createEmptyInstance() => create();
  static $pb.PbList<DeleteRangeResponse> createRepeated() =>
      $pb.PbList<DeleteRangeResponse>();
  @$core.pragma('dart2js:noInline')
  static DeleteRangeResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DeleteRangeResponse>(create);
  static DeleteRangeResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get deleted => $_getI64(1);
  @$pb.TagNumber(2)
  set deleted($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasDeleted() => $_has(1);
  @$pb.TagNumber(2)
  void clearDeleted() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$2.KeyValue> get prevKvs => $_getList(2);
}

enum RequestOp_Request {
  requestRange,
  requestPut,
  requestDeleteRange,
  requestTxn,
  notSet
}

class RequestOp extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, RequestOp_Request> _RequestOp_RequestByTag =
      {
    1: RequestOp_Request.requestRange,
    2: RequestOp_Request.requestPut,
    3: RequestOp_Request.requestDeleteRange,
    4: RequestOp_Request.requestTxn,
    0: RequestOp_Request.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RequestOp',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<RangeRequest>(1, 'requestRange', subBuilder: RangeRequest.create)
    ..aOM<PutRequest>(2, 'requestPut', subBuilder: PutRequest.create)
    ..aOM<DeleteRangeRequest>(3, 'requestDeleteRange',
        subBuilder: DeleteRangeRequest.create)
    ..aOM<TxnRequest>(4, 'requestTxn', subBuilder: TxnRequest.create)
    ..hasRequiredFields = false;

  RequestOp._() : super();
  factory RequestOp() => create();
  factory RequestOp.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory RequestOp.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  RequestOp clone() => RequestOp()..mergeFromMessage(this);
  RequestOp copyWith(void Function(RequestOp) updates) =>
      super.copyWith((message) => updates(message as RequestOp));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RequestOp create() => RequestOp._();
  RequestOp createEmptyInstance() => create();
  static $pb.PbList<RequestOp> createRepeated() => $pb.PbList<RequestOp>();
  @$core.pragma('dart2js:noInline')
  static RequestOp getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RequestOp>(create);
  static RequestOp _defaultInstance;

  RequestOp_Request whichRequest() => _RequestOp_RequestByTag[$_whichOneof(0)];
  void clearRequest() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  RangeRequest get requestRange => $_getN(0);
  @$pb.TagNumber(1)
  set requestRange(RangeRequest v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasRequestRange() => $_has(0);
  @$pb.TagNumber(1)
  void clearRequestRange() => clearField(1);
  @$pb.TagNumber(1)
  RangeRequest ensureRequestRange() => $_ensure(0);

  @$pb.TagNumber(2)
  PutRequest get requestPut => $_getN(1);
  @$pb.TagNumber(2)
  set requestPut(PutRequest v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRequestPut() => $_has(1);
  @$pb.TagNumber(2)
  void clearRequestPut() => clearField(2);
  @$pb.TagNumber(2)
  PutRequest ensureRequestPut() => $_ensure(1);

  @$pb.TagNumber(3)
  DeleteRangeRequest get requestDeleteRange => $_getN(2);
  @$pb.TagNumber(3)
  set requestDeleteRange(DeleteRangeRequest v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasRequestDeleteRange() => $_has(2);
  @$pb.TagNumber(3)
  void clearRequestDeleteRange() => clearField(3);
  @$pb.TagNumber(3)
  DeleteRangeRequest ensureRequestDeleteRange() => $_ensure(2);

  @$pb.TagNumber(4)
  TxnRequest get requestTxn => $_getN(3);
  @$pb.TagNumber(4)
  set requestTxn(TxnRequest v) {
    setField(4, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasRequestTxn() => $_has(3);
  @$pb.TagNumber(4)
  void clearRequestTxn() => clearField(4);
  @$pb.TagNumber(4)
  TxnRequest ensureRequestTxn() => $_ensure(3);
}

enum ResponseOp_Response {
  responseRange,
  responsePut,
  responseDeleteRange,
  responseTxn,
  notSet
}

class ResponseOp extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, ResponseOp_Response>
      _ResponseOp_ResponseByTag = {
    1: ResponseOp_Response.responseRange,
    2: ResponseOp_Response.responsePut,
    3: ResponseOp_Response.responseDeleteRange,
    4: ResponseOp_Response.responseTxn,
    0: ResponseOp_Response.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ResponseOp',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<RangeResponse>(1, 'responseRange', subBuilder: RangeResponse.create)
    ..aOM<PutResponse>(2, 'responsePut', subBuilder: PutResponse.create)
    ..aOM<DeleteRangeResponse>(3, 'responseDeleteRange',
        subBuilder: DeleteRangeResponse.create)
    ..aOM<TxnResponse>(4, 'responseTxn', subBuilder: TxnResponse.create)
    ..hasRequiredFields = false;

  ResponseOp._() : super();
  factory ResponseOp() => create();
  factory ResponseOp.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ResponseOp.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  ResponseOp clone() => ResponseOp()..mergeFromMessage(this);
  ResponseOp copyWith(void Function(ResponseOp) updates) =>
      super.copyWith((message) => updates(message as ResponseOp));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResponseOp create() => ResponseOp._();
  ResponseOp createEmptyInstance() => create();
  static $pb.PbList<ResponseOp> createRepeated() => $pb.PbList<ResponseOp>();
  @$core.pragma('dart2js:noInline')
  static ResponseOp getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ResponseOp>(create);
  static ResponseOp _defaultInstance;

  ResponseOp_Response whichResponse() =>
      _ResponseOp_ResponseByTag[$_whichOneof(0)];
  void clearResponse() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  RangeResponse get responseRange => $_getN(0);
  @$pb.TagNumber(1)
  set responseRange(RangeResponse v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasResponseRange() => $_has(0);
  @$pb.TagNumber(1)
  void clearResponseRange() => clearField(1);
  @$pb.TagNumber(1)
  RangeResponse ensureResponseRange() => $_ensure(0);

  @$pb.TagNumber(2)
  PutResponse get responsePut => $_getN(1);
  @$pb.TagNumber(2)
  set responsePut(PutResponse v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasResponsePut() => $_has(1);
  @$pb.TagNumber(2)
  void clearResponsePut() => clearField(2);
  @$pb.TagNumber(2)
  PutResponse ensureResponsePut() => $_ensure(1);

  @$pb.TagNumber(3)
  DeleteRangeResponse get responseDeleteRange => $_getN(2);
  @$pb.TagNumber(3)
  set responseDeleteRange(DeleteRangeResponse v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasResponseDeleteRange() => $_has(2);
  @$pb.TagNumber(3)
  void clearResponseDeleteRange() => clearField(3);
  @$pb.TagNumber(3)
  DeleteRangeResponse ensureResponseDeleteRange() => $_ensure(2);

  @$pb.TagNumber(4)
  TxnResponse get responseTxn => $_getN(3);
  @$pb.TagNumber(4)
  set responseTxn(TxnResponse v) {
    setField(4, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasResponseTxn() => $_has(3);
  @$pb.TagNumber(4)
  void clearResponseTxn() => clearField(4);
  @$pb.TagNumber(4)
  TxnResponse ensureResponseTxn() => $_ensure(3);
}

enum Compare_TargetUnion {
  version,
  createRevision,
  modRevision,
  value,
  lease,
  notSet
}

class Compare extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Compare_TargetUnion>
      _Compare_TargetUnionByTag = {
    4: Compare_TargetUnion.version,
    5: Compare_TargetUnion.createRevision,
    6: Compare_TargetUnion.modRevision,
    7: Compare_TargetUnion.value,
    8: Compare_TargetUnion.lease,
    0: Compare_TargetUnion.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Compare',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..oo(0, [4, 5, 6, 7, 8])
    ..e<Compare_CompareResult>(1, 'result', $pb.PbFieldType.OE,
        defaultOrMaker: Compare_CompareResult.EQUAL,
        valueOf: Compare_CompareResult.valueOf,
        enumValues: Compare_CompareResult.values)
    ..e<Compare_CompareTarget>(2, 'target', $pb.PbFieldType.OE,
        defaultOrMaker: Compare_CompareTarget.VERSION,
        valueOf: Compare_CompareTarget.valueOf,
        enumValues: Compare_CompareTarget.values)
    ..a<$core.List<$core.int>>(3, 'key', $pb.PbFieldType.OY)
    ..aInt64(4, 'version')
    ..aInt64(5, 'createRevision')
    ..aInt64(6, 'modRevision')
    ..a<$core.List<$core.int>>(7, 'value', $pb.PbFieldType.OY)
    ..aInt64(8, 'lease')
    ..a<$core.List<$core.int>>(64, 'rangeEnd', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  Compare._() : super();
  factory Compare() => create();
  factory Compare.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory Compare.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  Compare clone() => Compare()..mergeFromMessage(this);
  Compare copyWith(void Function(Compare) updates) =>
      super.copyWith((message) => updates(message as Compare));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Compare create() => Compare._();
  Compare createEmptyInstance() => create();
  static $pb.PbList<Compare> createRepeated() => $pb.PbList<Compare>();
  @$core.pragma('dart2js:noInline')
  static Compare getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Compare>(create);
  static Compare _defaultInstance;

  Compare_TargetUnion whichTargetUnion() =>
      _Compare_TargetUnionByTag[$_whichOneof(0)];
  void clearTargetUnion() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  Compare_CompareResult get result => $_getN(0);
  @$pb.TagNumber(1)
  set result(Compare_CompareResult v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasResult() => $_has(0);
  @$pb.TagNumber(1)
  void clearResult() => clearField(1);

  @$pb.TagNumber(2)
  Compare_CompareTarget get target => $_getN(1);
  @$pb.TagNumber(2)
  set target(Compare_CompareTarget v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasTarget() => $_has(1);
  @$pb.TagNumber(2)
  void clearTarget() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get key => $_getN(2);
  @$pb.TagNumber(3)
  set key($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasKey() => $_has(2);
  @$pb.TagNumber(3)
  void clearKey() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get version => $_getI64(3);
  @$pb.TagNumber(4)
  set version($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasVersion() => $_has(3);
  @$pb.TagNumber(4)
  void clearVersion() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get createRevision => $_getI64(4);
  @$pb.TagNumber(5)
  set createRevision($fixnum.Int64 v) {
    $_setInt64(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasCreateRevision() => $_has(4);
  @$pb.TagNumber(5)
  void clearCreateRevision() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get modRevision => $_getI64(5);
  @$pb.TagNumber(6)
  set modRevision($fixnum.Int64 v) {
    $_setInt64(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasModRevision() => $_has(5);
  @$pb.TagNumber(6)
  void clearModRevision() => clearField(6);

  @$pb.TagNumber(7)
  $core.List<$core.int> get value => $_getN(6);
  @$pb.TagNumber(7)
  set value($core.List<$core.int> v) {
    $_setBytes(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasValue() => $_has(6);
  @$pb.TagNumber(7)
  void clearValue() => clearField(7);

  @$pb.TagNumber(8)
  $fixnum.Int64 get lease => $_getI64(7);
  @$pb.TagNumber(8)
  set lease($fixnum.Int64 v) {
    $_setInt64(7, v);
  }

  @$pb.TagNumber(8)
  $core.bool hasLease() => $_has(7);
  @$pb.TagNumber(8)
  void clearLease() => clearField(8);

  @$pb.TagNumber(64)
  $core.List<$core.int> get rangeEnd => $_getN(8);
  @$pb.TagNumber(64)
  set rangeEnd($core.List<$core.int> v) {
    $_setBytes(8, v);
  }

  @$pb.TagNumber(64)
  $core.bool hasRangeEnd() => $_has(8);
  @$pb.TagNumber(64)
  void clearRangeEnd() => clearField(64);
}

class TxnRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TxnRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..pc<Compare>(1, 'compare', $pb.PbFieldType.PM, subBuilder: Compare.create)
    ..pc<RequestOp>(2, 'success', $pb.PbFieldType.PM,
        subBuilder: RequestOp.create)
    ..pc<RequestOp>(3, 'failure', $pb.PbFieldType.PM,
        subBuilder: RequestOp.create)
    ..hasRequiredFields = false;

  TxnRequest._() : super();
  factory TxnRequest() => create();
  factory TxnRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory TxnRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  TxnRequest clone() => TxnRequest()..mergeFromMessage(this);
  TxnRequest copyWith(void Function(TxnRequest) updates) =>
      super.copyWith((message) => updates(message as TxnRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TxnRequest create() => TxnRequest._();
  TxnRequest createEmptyInstance() => create();
  static $pb.PbList<TxnRequest> createRepeated() => $pb.PbList<TxnRequest>();
  @$core.pragma('dart2js:noInline')
  static TxnRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<TxnRequest>(create);
  static TxnRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Compare> get compare => $_getList(0);

  @$pb.TagNumber(2)
  $core.List<RequestOp> get success => $_getList(1);

  @$pb.TagNumber(3)
  $core.List<RequestOp> get failure => $_getList(2);
}

class TxnResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TxnResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aOB(2, 'succeeded')
    ..pc<ResponseOp>(3, 'responses', $pb.PbFieldType.PM,
        subBuilder: ResponseOp.create)
    ..hasRequiredFields = false;

  TxnResponse._() : super();
  factory TxnResponse() => create();
  factory TxnResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory TxnResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  TxnResponse clone() => TxnResponse()..mergeFromMessage(this);
  TxnResponse copyWith(void Function(TxnResponse) updates) =>
      super.copyWith((message) => updates(message as TxnResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TxnResponse create() => TxnResponse._();
  TxnResponse createEmptyInstance() => create();
  static $pb.PbList<TxnResponse> createRepeated() => $pb.PbList<TxnResponse>();
  @$core.pragma('dart2js:noInline')
  static TxnResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<TxnResponse>(create);
  static TxnResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.bool get succeeded => $_getBF(1);
  @$pb.TagNumber(2)
  set succeeded($core.bool v) {
    $_setBool(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSucceeded() => $_has(1);
  @$pb.TagNumber(2)
  void clearSucceeded() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<ResponseOp> get responses => $_getList(2);
}

class CompactionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('CompactionRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'revision')
    ..aOB(2, 'physical')
    ..hasRequiredFields = false;

  CompactionRequest._() : super();
  factory CompactionRequest() => create();
  factory CompactionRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory CompactionRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  CompactionRequest clone() => CompactionRequest()..mergeFromMessage(this);
  CompactionRequest copyWith(void Function(CompactionRequest) updates) =>
      super.copyWith((message) => updates(message as CompactionRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CompactionRequest create() => CompactionRequest._();
  CompactionRequest createEmptyInstance() => create();
  static $pb.PbList<CompactionRequest> createRepeated() =>
      $pb.PbList<CompactionRequest>();
  @$core.pragma('dart2js:noInline')
  static CompactionRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<CompactionRequest>(create);
  static CompactionRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get revision => $_getI64(0);
  @$pb.TagNumber(1)
  set revision($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasRevision() => $_has(0);
  @$pb.TagNumber(1)
  void clearRevision() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get physical => $_getBF(1);
  @$pb.TagNumber(2)
  set physical($core.bool v) {
    $_setBool(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasPhysical() => $_has(1);
  @$pb.TagNumber(2)
  void clearPhysical() => clearField(2);
}

class CompactionResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('CompactionResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  CompactionResponse._() : super();
  factory CompactionResponse() => create();
  factory CompactionResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory CompactionResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  CompactionResponse clone() => CompactionResponse()..mergeFromMessage(this);
  CompactionResponse copyWith(void Function(CompactionResponse) updates) =>
      super.copyWith((message) => updates(message as CompactionResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CompactionResponse create() => CompactionResponse._();
  CompactionResponse createEmptyInstance() => create();
  static $pb.PbList<CompactionResponse> createRepeated() =>
      $pb.PbList<CompactionResponse>();
  @$core.pragma('dart2js:noInline')
  static CompactionResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<CompactionResponse>(create);
  static CompactionResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class HashRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('HashRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  HashRequest._() : super();
  factory HashRequest() => create();
  factory HashRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory HashRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  HashRequest clone() => HashRequest()..mergeFromMessage(this);
  HashRequest copyWith(void Function(HashRequest) updates) =>
      super.copyWith((message) => updates(message as HashRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HashRequest create() => HashRequest._();
  HashRequest createEmptyInstance() => create();
  static $pb.PbList<HashRequest> createRepeated() => $pb.PbList<HashRequest>();
  @$core.pragma('dart2js:noInline')
  static HashRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<HashRequest>(create);
  static HashRequest _defaultInstance;
}

class HashKVRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('HashKVRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'revision')
    ..hasRequiredFields = false;

  HashKVRequest._() : super();
  factory HashKVRequest() => create();
  factory HashKVRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory HashKVRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  HashKVRequest clone() => HashKVRequest()..mergeFromMessage(this);
  HashKVRequest copyWith(void Function(HashKVRequest) updates) =>
      super.copyWith((message) => updates(message as HashKVRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HashKVRequest create() => HashKVRequest._();
  HashKVRequest createEmptyInstance() => create();
  static $pb.PbList<HashKVRequest> createRepeated() =>
      $pb.PbList<HashKVRequest>();
  @$core.pragma('dart2js:noInline')
  static HashKVRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<HashKVRequest>(create);
  static HashKVRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get revision => $_getI64(0);
  @$pb.TagNumber(1)
  set revision($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasRevision() => $_has(0);
  @$pb.TagNumber(1)
  void clearRevision() => clearField(1);
}

class HashKVResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('HashKVResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..a<$core.int>(2, 'hash', $pb.PbFieldType.OU3)
    ..aInt64(3, 'compactRevision')
    ..hasRequiredFields = false;

  HashKVResponse._() : super();
  factory HashKVResponse() => create();
  factory HashKVResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory HashKVResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  HashKVResponse clone() => HashKVResponse()..mergeFromMessage(this);
  HashKVResponse copyWith(void Function(HashKVResponse) updates) =>
      super.copyWith((message) => updates(message as HashKVResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HashKVResponse create() => HashKVResponse._();
  HashKVResponse createEmptyInstance() => create();
  static $pb.PbList<HashKVResponse> createRepeated() =>
      $pb.PbList<HashKVResponse>();
  @$core.pragma('dart2js:noInline')
  static HashKVResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<HashKVResponse>(create);
  static HashKVResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get hash => $_getIZ(1);
  @$pb.TagNumber(2)
  set hash($core.int v) {
    $_setUnsignedInt32(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasHash() => $_has(1);
  @$pb.TagNumber(2)
  void clearHash() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get compactRevision => $_getI64(2);
  @$pb.TagNumber(3)
  set compactRevision($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasCompactRevision() => $_has(2);
  @$pb.TagNumber(3)
  void clearCompactRevision() => clearField(3);
}

class HashResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('HashResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..a<$core.int>(2, 'hash', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false;

  HashResponse._() : super();
  factory HashResponse() => create();
  factory HashResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory HashResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  HashResponse clone() => HashResponse()..mergeFromMessage(this);
  HashResponse copyWith(void Function(HashResponse) updates) =>
      super.copyWith((message) => updates(message as HashResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HashResponse create() => HashResponse._();
  HashResponse createEmptyInstance() => create();
  static $pb.PbList<HashResponse> createRepeated() =>
      $pb.PbList<HashResponse>();
  @$core.pragma('dart2js:noInline')
  static HashResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<HashResponse>(create);
  static HashResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get hash => $_getIZ(1);
  @$pb.TagNumber(2)
  set hash($core.int v) {
    $_setUnsignedInt32(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasHash() => $_has(1);
  @$pb.TagNumber(2)
  void clearHash() => clearField(2);
}

class SnapshotRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SnapshotRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  SnapshotRequest._() : super();
  factory SnapshotRequest() => create();
  factory SnapshotRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory SnapshotRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  SnapshotRequest clone() => SnapshotRequest()..mergeFromMessage(this);
  SnapshotRequest copyWith(void Function(SnapshotRequest) updates) =>
      super.copyWith((message) => updates(message as SnapshotRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SnapshotRequest create() => SnapshotRequest._();
  SnapshotRequest createEmptyInstance() => create();
  static $pb.PbList<SnapshotRequest> createRepeated() =>
      $pb.PbList<SnapshotRequest>();
  @$core.pragma('dart2js:noInline')
  static SnapshotRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<SnapshotRequest>(create);
  static SnapshotRequest _defaultInstance;
}

class SnapshotResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SnapshotResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..a<$fixnum.Int64>(2, 'remainingBytes', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$core.List<$core.int>>(3, 'blob', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  SnapshotResponse._() : super();
  factory SnapshotResponse() => create();
  factory SnapshotResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory SnapshotResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  SnapshotResponse clone() => SnapshotResponse()..mergeFromMessage(this);
  SnapshotResponse copyWith(void Function(SnapshotResponse) updates) =>
      super.copyWith((message) => updates(message as SnapshotResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SnapshotResponse create() => SnapshotResponse._();
  SnapshotResponse createEmptyInstance() => create();
  static $pb.PbList<SnapshotResponse> createRepeated() =>
      $pb.PbList<SnapshotResponse>();
  @$core.pragma('dart2js:noInline')
  static SnapshotResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<SnapshotResponse>(create);
  static SnapshotResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get remainingBytes => $_getI64(1);
  @$pb.TagNumber(2)
  set remainingBytes($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRemainingBytes() => $_has(1);
  @$pb.TagNumber(2)
  void clearRemainingBytes() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get blob => $_getN(2);
  @$pb.TagNumber(3)
  set blob($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasBlob() => $_has(2);
  @$pb.TagNumber(3)
  void clearBlob() => clearField(3);
}

enum WatchRequest_RequestUnion {
  createRequest,
  cancelRequest,
  progressRequest,
  notSet
}

class WatchRequest extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, WatchRequest_RequestUnion>
      _WatchRequest_RequestUnionByTag = {
    1: WatchRequest_RequestUnion.createRequest,
    2: WatchRequest_RequestUnion.cancelRequest,
    3: WatchRequest_RequestUnion.progressRequest,
    0: WatchRequest_RequestUnion.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('WatchRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<WatchCreateRequest>(1, 'createRequest',
        subBuilder: WatchCreateRequest.create)
    ..aOM<WatchCancelRequest>(2, 'cancelRequest',
        subBuilder: WatchCancelRequest.create)
    ..aOM<WatchProgressRequest>(3, 'progressRequest',
        subBuilder: WatchProgressRequest.create)
    ..hasRequiredFields = false;

  WatchRequest._() : super();
  factory WatchRequest() => create();
  factory WatchRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory WatchRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  WatchRequest clone() => WatchRequest()..mergeFromMessage(this);
  WatchRequest copyWith(void Function(WatchRequest) updates) =>
      super.copyWith((message) => updates(message as WatchRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WatchRequest create() => WatchRequest._();
  WatchRequest createEmptyInstance() => create();
  static $pb.PbList<WatchRequest> createRepeated() =>
      $pb.PbList<WatchRequest>();
  @$core.pragma('dart2js:noInline')
  static WatchRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<WatchRequest>(create);
  static WatchRequest _defaultInstance;

  WatchRequest_RequestUnion whichRequestUnion() =>
      _WatchRequest_RequestUnionByTag[$_whichOneof(0)];
  void clearRequestUnion() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  WatchCreateRequest get createRequest => $_getN(0);
  @$pb.TagNumber(1)
  set createRequest(WatchCreateRequest v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasCreateRequest() => $_has(0);
  @$pb.TagNumber(1)
  void clearCreateRequest() => clearField(1);
  @$pb.TagNumber(1)
  WatchCreateRequest ensureCreateRequest() => $_ensure(0);

  @$pb.TagNumber(2)
  WatchCancelRequest get cancelRequest => $_getN(1);
  @$pb.TagNumber(2)
  set cancelRequest(WatchCancelRequest v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasCancelRequest() => $_has(1);
  @$pb.TagNumber(2)
  void clearCancelRequest() => clearField(2);
  @$pb.TagNumber(2)
  WatchCancelRequest ensureCancelRequest() => $_ensure(1);

  @$pb.TagNumber(3)
  WatchProgressRequest get progressRequest => $_getN(2);
  @$pb.TagNumber(3)
  set progressRequest(WatchProgressRequest v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasProgressRequest() => $_has(2);
  @$pb.TagNumber(3)
  void clearProgressRequest() => clearField(3);
  @$pb.TagNumber(3)
  WatchProgressRequest ensureProgressRequest() => $_ensure(2);
}

class WatchCreateRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('WatchCreateRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, 'key', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, 'rangeEnd', $pb.PbFieldType.OY)
    ..aInt64(3, 'startRevision')
    ..aOB(4, 'progressNotify')
    ..pc<WatchCreateRequest_FilterType>(5, 'filters', $pb.PbFieldType.PE,
        valueOf: WatchCreateRequest_FilterType.valueOf,
        enumValues: WatchCreateRequest_FilterType.values)
    ..aOB(6, 'prevKv')
    ..aInt64(7, 'watchId')
    ..aOB(8, 'fragment')
    ..hasRequiredFields = false;

  WatchCreateRequest._() : super();
  factory WatchCreateRequest() => create();
  factory WatchCreateRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory WatchCreateRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  WatchCreateRequest clone() => WatchCreateRequest()..mergeFromMessage(this);
  WatchCreateRequest copyWith(void Function(WatchCreateRequest) updates) =>
      super.copyWith((message) => updates(message as WatchCreateRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WatchCreateRequest create() => WatchCreateRequest._();
  WatchCreateRequest createEmptyInstance() => create();
  static $pb.PbList<WatchCreateRequest> createRepeated() =>
      $pb.PbList<WatchCreateRequest>();
  @$core.pragma('dart2js:noInline')
  static WatchCreateRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<WatchCreateRequest>(create);
  static WatchCreateRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get key => $_getN(0);
  @$pb.TagNumber(1)
  set key($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get rangeEnd => $_getN(1);
  @$pb.TagNumber(2)
  set rangeEnd($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRangeEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearRangeEnd() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get startRevision => $_getI64(2);
  @$pb.TagNumber(3)
  set startRevision($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasStartRevision() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartRevision() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get progressNotify => $_getBF(3);
  @$pb.TagNumber(4)
  set progressNotify($core.bool v) {
    $_setBool(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasProgressNotify() => $_has(3);
  @$pb.TagNumber(4)
  void clearProgressNotify() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<WatchCreateRequest_FilterType> get filters => $_getList(4);

  @$pb.TagNumber(6)
  $core.bool get prevKv => $_getBF(5);
  @$pb.TagNumber(6)
  set prevKv($core.bool v) {
    $_setBool(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasPrevKv() => $_has(5);
  @$pb.TagNumber(6)
  void clearPrevKv() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get watchId => $_getI64(6);
  @$pb.TagNumber(7)
  set watchId($fixnum.Int64 v) {
    $_setInt64(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasWatchId() => $_has(6);
  @$pb.TagNumber(7)
  void clearWatchId() => clearField(7);

  @$pb.TagNumber(8)
  $core.bool get fragment => $_getBF(7);
  @$pb.TagNumber(8)
  set fragment($core.bool v) {
    $_setBool(7, v);
  }

  @$pb.TagNumber(8)
  $core.bool hasFragment() => $_has(7);
  @$pb.TagNumber(8)
  void clearFragment() => clearField(8);
}

class WatchCancelRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('WatchCancelRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'watchId')
    ..hasRequiredFields = false;

  WatchCancelRequest._() : super();
  factory WatchCancelRequest() => create();
  factory WatchCancelRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory WatchCancelRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  WatchCancelRequest clone() => WatchCancelRequest()..mergeFromMessage(this);
  WatchCancelRequest copyWith(void Function(WatchCancelRequest) updates) =>
      super.copyWith((message) => updates(message as WatchCancelRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WatchCancelRequest create() => WatchCancelRequest._();
  WatchCancelRequest createEmptyInstance() => create();
  static $pb.PbList<WatchCancelRequest> createRepeated() =>
      $pb.PbList<WatchCancelRequest>();
  @$core.pragma('dart2js:noInline')
  static WatchCancelRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<WatchCancelRequest>(create);
  static WatchCancelRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get watchId => $_getI64(0);
  @$pb.TagNumber(1)
  set watchId($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasWatchId() => $_has(0);
  @$pb.TagNumber(1)
  void clearWatchId() => clearField(1);
}

class WatchProgressRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('WatchProgressRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  WatchProgressRequest._() : super();
  factory WatchProgressRequest() => create();
  factory WatchProgressRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory WatchProgressRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  WatchProgressRequest clone() =>
      WatchProgressRequest()..mergeFromMessage(this);
  WatchProgressRequest copyWith(void Function(WatchProgressRequest) updates) =>
      super.copyWith((message) => updates(message as WatchProgressRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WatchProgressRequest create() => WatchProgressRequest._();
  WatchProgressRequest createEmptyInstance() => create();
  static $pb.PbList<WatchProgressRequest> createRepeated() =>
      $pb.PbList<WatchProgressRequest>();
  @$core.pragma('dart2js:noInline')
  static WatchProgressRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<WatchProgressRequest>(create);
  static WatchProgressRequest _defaultInstance;
}

class WatchResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('WatchResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aInt64(2, 'watchId')
    ..aOB(3, 'created')
    ..aOB(4, 'canceled')
    ..aInt64(5, 'compactRevision')
    ..aOS(6, 'cancelReason')
    ..aOB(7, 'fragment')
    ..pc<$2.Event>(11, 'events', $pb.PbFieldType.PM,
        subBuilder: $2.Event.create)
    ..hasRequiredFields = false;

  WatchResponse._() : super();
  factory WatchResponse() => create();
  factory WatchResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory WatchResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  WatchResponse clone() => WatchResponse()..mergeFromMessage(this);
  WatchResponse copyWith(void Function(WatchResponse) updates) =>
      super.copyWith((message) => updates(message as WatchResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WatchResponse create() => WatchResponse._();
  WatchResponse createEmptyInstance() => create();
  static $pb.PbList<WatchResponse> createRepeated() =>
      $pb.PbList<WatchResponse>();
  @$core.pragma('dart2js:noInline')
  static WatchResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<WatchResponse>(create);
  static WatchResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get watchId => $_getI64(1);
  @$pb.TagNumber(2)
  set watchId($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasWatchId() => $_has(1);
  @$pb.TagNumber(2)
  void clearWatchId() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get created => $_getBF(2);
  @$pb.TagNumber(3)
  set created($core.bool v) {
    $_setBool(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasCreated() => $_has(2);
  @$pb.TagNumber(3)
  void clearCreated() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get canceled => $_getBF(3);
  @$pb.TagNumber(4)
  set canceled($core.bool v) {
    $_setBool(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasCanceled() => $_has(3);
  @$pb.TagNumber(4)
  void clearCanceled() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get compactRevision => $_getI64(4);
  @$pb.TagNumber(5)
  set compactRevision($fixnum.Int64 v) {
    $_setInt64(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasCompactRevision() => $_has(4);
  @$pb.TagNumber(5)
  void clearCompactRevision() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get cancelReason => $_getSZ(5);
  @$pb.TagNumber(6)
  set cancelReason($core.String v) {
    $_setString(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasCancelReason() => $_has(5);
  @$pb.TagNumber(6)
  void clearCancelReason() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get fragment => $_getBF(6);
  @$pb.TagNumber(7)
  set fragment($core.bool v) {
    $_setBool(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasFragment() => $_has(6);
  @$pb.TagNumber(7)
  void clearFragment() => clearField(7);

  @$pb.TagNumber(11)
  $core.List<$2.Event> get events => $_getList(7);
}

class LeaseGrantRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseGrantRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'TTL', protoName: 'TTL')
    ..aInt64(2, 'ID', protoName: 'ID')
    ..hasRequiredFields = false;

  LeaseGrantRequest._() : super();
  factory LeaseGrantRequest() => create();
  factory LeaseGrantRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseGrantRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseGrantRequest clone() => LeaseGrantRequest()..mergeFromMessage(this);
  LeaseGrantRequest copyWith(void Function(LeaseGrantRequest) updates) =>
      super.copyWith((message) => updates(message as LeaseGrantRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseGrantRequest create() => LeaseGrantRequest._();
  LeaseGrantRequest createEmptyInstance() => create();
  static $pb.PbList<LeaseGrantRequest> createRepeated() =>
      $pb.PbList<LeaseGrantRequest>();
  @$core.pragma('dart2js:noInline')
  static LeaseGrantRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseGrantRequest>(create);
  static LeaseGrantRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get tTL => $_getI64(0);
  @$pb.TagNumber(1)
  set tTL($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasTTL() => $_has(0);
  @$pb.TagNumber(1)
  void clearTTL() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get iD => $_getI64(1);
  @$pb.TagNumber(2)
  set iD($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasID() => $_has(1);
  @$pb.TagNumber(2)
  void clearID() => clearField(2);
}

class LeaseGrantResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseGrantResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aInt64(2, 'ID', protoName: 'ID')
    ..aInt64(3, 'TTL', protoName: 'TTL')
    ..aOS(4, 'error')
    ..hasRequiredFields = false;

  LeaseGrantResponse._() : super();
  factory LeaseGrantResponse() => create();
  factory LeaseGrantResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseGrantResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseGrantResponse clone() => LeaseGrantResponse()..mergeFromMessage(this);
  LeaseGrantResponse copyWith(void Function(LeaseGrantResponse) updates) =>
      super.copyWith((message) => updates(message as LeaseGrantResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseGrantResponse create() => LeaseGrantResponse._();
  LeaseGrantResponse createEmptyInstance() => create();
  static $pb.PbList<LeaseGrantResponse> createRepeated() =>
      $pb.PbList<LeaseGrantResponse>();
  @$core.pragma('dart2js:noInline')
  static LeaseGrantResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseGrantResponse>(create);
  static LeaseGrantResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get iD => $_getI64(1);
  @$pb.TagNumber(2)
  set iD($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasID() => $_has(1);
  @$pb.TagNumber(2)
  void clearID() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get tTL => $_getI64(2);
  @$pb.TagNumber(3)
  set tTL($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasTTL() => $_has(2);
  @$pb.TagNumber(3)
  void clearTTL() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get error => $_getSZ(3);
  @$pb.TagNumber(4)
  set error($core.String v) {
    $_setString(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasError() => $_has(3);
  @$pb.TagNumber(4)
  void clearError() => clearField(4);
}

class LeaseRevokeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseRevokeRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'ID', protoName: 'ID')
    ..hasRequiredFields = false;

  LeaseRevokeRequest._() : super();
  factory LeaseRevokeRequest() => create();
  factory LeaseRevokeRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseRevokeRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseRevokeRequest clone() => LeaseRevokeRequest()..mergeFromMessage(this);
  LeaseRevokeRequest copyWith(void Function(LeaseRevokeRequest) updates) =>
      super.copyWith((message) => updates(message as LeaseRevokeRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseRevokeRequest create() => LeaseRevokeRequest._();
  LeaseRevokeRequest createEmptyInstance() => create();
  static $pb.PbList<LeaseRevokeRequest> createRepeated() =>
      $pb.PbList<LeaseRevokeRequest>();
  @$core.pragma('dart2js:noInline')
  static LeaseRevokeRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseRevokeRequest>(create);
  static LeaseRevokeRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class LeaseRevokeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseRevokeResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  LeaseRevokeResponse._() : super();
  factory LeaseRevokeResponse() => create();
  factory LeaseRevokeResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseRevokeResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseRevokeResponse clone() => LeaseRevokeResponse()..mergeFromMessage(this);
  LeaseRevokeResponse copyWith(void Function(LeaseRevokeResponse) updates) =>
      super.copyWith((message) => updates(message as LeaseRevokeResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseRevokeResponse create() => LeaseRevokeResponse._();
  LeaseRevokeResponse createEmptyInstance() => create();
  static $pb.PbList<LeaseRevokeResponse> createRepeated() =>
      $pb.PbList<LeaseRevokeResponse>();
  @$core.pragma('dart2js:noInline')
  static LeaseRevokeResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseRevokeResponse>(create);
  static LeaseRevokeResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class LeaseCheckpoint extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseCheckpoint',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'ID', protoName: 'ID')
    ..aInt64(2, 'remainingTTL', protoName: 'remaining_TTL')
    ..hasRequiredFields = false;

  LeaseCheckpoint._() : super();
  factory LeaseCheckpoint() => create();
  factory LeaseCheckpoint.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseCheckpoint.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseCheckpoint clone() => LeaseCheckpoint()..mergeFromMessage(this);
  LeaseCheckpoint copyWith(void Function(LeaseCheckpoint) updates) =>
      super.copyWith((message) => updates(message as LeaseCheckpoint));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseCheckpoint create() => LeaseCheckpoint._();
  LeaseCheckpoint createEmptyInstance() => create();
  static $pb.PbList<LeaseCheckpoint> createRepeated() =>
      $pb.PbList<LeaseCheckpoint>();
  @$core.pragma('dart2js:noInline')
  static LeaseCheckpoint getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseCheckpoint>(create);
  static LeaseCheckpoint _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get remainingTTL => $_getI64(1);
  @$pb.TagNumber(2)
  set remainingTTL($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRemainingTTL() => $_has(1);
  @$pb.TagNumber(2)
  void clearRemainingTTL() => clearField(2);
}

class LeaseCheckpointRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseCheckpointRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..pc<LeaseCheckpoint>(1, 'checkpoints', $pb.PbFieldType.PM,
        subBuilder: LeaseCheckpoint.create)
    ..hasRequiredFields = false;

  LeaseCheckpointRequest._() : super();
  factory LeaseCheckpointRequest() => create();
  factory LeaseCheckpointRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseCheckpointRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseCheckpointRequest clone() =>
      LeaseCheckpointRequest()..mergeFromMessage(this);
  LeaseCheckpointRequest copyWith(
          void Function(LeaseCheckpointRequest) updates) =>
      super.copyWith((message) => updates(message as LeaseCheckpointRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseCheckpointRequest create() => LeaseCheckpointRequest._();
  LeaseCheckpointRequest createEmptyInstance() => create();
  static $pb.PbList<LeaseCheckpointRequest> createRepeated() =>
      $pb.PbList<LeaseCheckpointRequest>();
  @$core.pragma('dart2js:noInline')
  static LeaseCheckpointRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseCheckpointRequest>(create);
  static LeaseCheckpointRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<LeaseCheckpoint> get checkpoints => $_getList(0);
}

class LeaseCheckpointResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseCheckpointResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  LeaseCheckpointResponse._() : super();
  factory LeaseCheckpointResponse() => create();
  factory LeaseCheckpointResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseCheckpointResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseCheckpointResponse clone() =>
      LeaseCheckpointResponse()..mergeFromMessage(this);
  LeaseCheckpointResponse copyWith(
          void Function(LeaseCheckpointResponse) updates) =>
      super.copyWith((message) => updates(message as LeaseCheckpointResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseCheckpointResponse create() => LeaseCheckpointResponse._();
  LeaseCheckpointResponse createEmptyInstance() => create();
  static $pb.PbList<LeaseCheckpointResponse> createRepeated() =>
      $pb.PbList<LeaseCheckpointResponse>();
  @$core.pragma('dart2js:noInline')
  static LeaseCheckpointResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseCheckpointResponse>(create);
  static LeaseCheckpointResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class LeaseKeepAliveRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseKeepAliveRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'ID', protoName: 'ID')
    ..hasRequiredFields = false;

  LeaseKeepAliveRequest._() : super();
  factory LeaseKeepAliveRequest() => create();
  factory LeaseKeepAliveRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseKeepAliveRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseKeepAliveRequest clone() =>
      LeaseKeepAliveRequest()..mergeFromMessage(this);
  LeaseKeepAliveRequest copyWith(
          void Function(LeaseKeepAliveRequest) updates) =>
      super.copyWith((message) => updates(message as LeaseKeepAliveRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseKeepAliveRequest create() => LeaseKeepAliveRequest._();
  LeaseKeepAliveRequest createEmptyInstance() => create();
  static $pb.PbList<LeaseKeepAliveRequest> createRepeated() =>
      $pb.PbList<LeaseKeepAliveRequest>();
  @$core.pragma('dart2js:noInline')
  static LeaseKeepAliveRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseKeepAliveRequest>(create);
  static LeaseKeepAliveRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class LeaseKeepAliveResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseKeepAliveResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aInt64(2, 'ID', protoName: 'ID')
    ..aInt64(3, 'TTL', protoName: 'TTL')
    ..hasRequiredFields = false;

  LeaseKeepAliveResponse._() : super();
  factory LeaseKeepAliveResponse() => create();
  factory LeaseKeepAliveResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseKeepAliveResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseKeepAliveResponse clone() =>
      LeaseKeepAliveResponse()..mergeFromMessage(this);
  LeaseKeepAliveResponse copyWith(
          void Function(LeaseKeepAliveResponse) updates) =>
      super.copyWith((message) => updates(message as LeaseKeepAliveResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseKeepAliveResponse create() => LeaseKeepAliveResponse._();
  LeaseKeepAliveResponse createEmptyInstance() => create();
  static $pb.PbList<LeaseKeepAliveResponse> createRepeated() =>
      $pb.PbList<LeaseKeepAliveResponse>();
  @$core.pragma('dart2js:noInline')
  static LeaseKeepAliveResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseKeepAliveResponse>(create);
  static LeaseKeepAliveResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get iD => $_getI64(1);
  @$pb.TagNumber(2)
  set iD($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasID() => $_has(1);
  @$pb.TagNumber(2)
  void clearID() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get tTL => $_getI64(2);
  @$pb.TagNumber(3)
  set tTL($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasTTL() => $_has(2);
  @$pb.TagNumber(3)
  void clearTTL() => clearField(3);
}

class LeaseTimeToLiveRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseTimeToLiveRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'ID', protoName: 'ID')
    ..aOB(2, 'keys')
    ..hasRequiredFields = false;

  LeaseTimeToLiveRequest._() : super();
  factory LeaseTimeToLiveRequest() => create();
  factory LeaseTimeToLiveRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseTimeToLiveRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseTimeToLiveRequest clone() =>
      LeaseTimeToLiveRequest()..mergeFromMessage(this);
  LeaseTimeToLiveRequest copyWith(
          void Function(LeaseTimeToLiveRequest) updates) =>
      super.copyWith((message) => updates(message as LeaseTimeToLiveRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseTimeToLiveRequest create() => LeaseTimeToLiveRequest._();
  LeaseTimeToLiveRequest createEmptyInstance() => create();
  static $pb.PbList<LeaseTimeToLiveRequest> createRepeated() =>
      $pb.PbList<LeaseTimeToLiveRequest>();
  @$core.pragma('dart2js:noInline')
  static LeaseTimeToLiveRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseTimeToLiveRequest>(create);
  static LeaseTimeToLiveRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get keys => $_getBF(1);
  @$pb.TagNumber(2)
  set keys($core.bool v) {
    $_setBool(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasKeys() => $_has(1);
  @$pb.TagNumber(2)
  void clearKeys() => clearField(2);
}

class LeaseTimeToLiveResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseTimeToLiveResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aInt64(2, 'ID', protoName: 'ID')
    ..aInt64(3, 'TTL', protoName: 'TTL')
    ..aInt64(4, 'grantedTTL', protoName: 'grantedTTL')
    ..p<$core.List<$core.int>>(5, 'keys', $pb.PbFieldType.PY)
    ..hasRequiredFields = false;

  LeaseTimeToLiveResponse._() : super();
  factory LeaseTimeToLiveResponse() => create();
  factory LeaseTimeToLiveResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseTimeToLiveResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseTimeToLiveResponse clone() =>
      LeaseTimeToLiveResponse()..mergeFromMessage(this);
  LeaseTimeToLiveResponse copyWith(
          void Function(LeaseTimeToLiveResponse) updates) =>
      super.copyWith((message) => updates(message as LeaseTimeToLiveResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseTimeToLiveResponse create() => LeaseTimeToLiveResponse._();
  LeaseTimeToLiveResponse createEmptyInstance() => create();
  static $pb.PbList<LeaseTimeToLiveResponse> createRepeated() =>
      $pb.PbList<LeaseTimeToLiveResponse>();
  @$core.pragma('dart2js:noInline')
  static LeaseTimeToLiveResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseTimeToLiveResponse>(create);
  static LeaseTimeToLiveResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get iD => $_getI64(1);
  @$pb.TagNumber(2)
  set iD($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasID() => $_has(1);
  @$pb.TagNumber(2)
  void clearID() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get tTL => $_getI64(2);
  @$pb.TagNumber(3)
  set tTL($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasTTL() => $_has(2);
  @$pb.TagNumber(3)
  void clearTTL() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get grantedTTL => $_getI64(3);
  @$pb.TagNumber(4)
  set grantedTTL($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasGrantedTTL() => $_has(3);
  @$pb.TagNumber(4)
  void clearGrantedTTL() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.List<$core.int>> get keys => $_getList(4);
}

class LeaseLeasesRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseLeasesRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  LeaseLeasesRequest._() : super();
  factory LeaseLeasesRequest() => create();
  factory LeaseLeasesRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseLeasesRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseLeasesRequest clone() => LeaseLeasesRequest()..mergeFromMessage(this);
  LeaseLeasesRequest copyWith(void Function(LeaseLeasesRequest) updates) =>
      super.copyWith((message) => updates(message as LeaseLeasesRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseLeasesRequest create() => LeaseLeasesRequest._();
  LeaseLeasesRequest createEmptyInstance() => create();
  static $pb.PbList<LeaseLeasesRequest> createRepeated() =>
      $pb.PbList<LeaseLeasesRequest>();
  @$core.pragma('dart2js:noInline')
  static LeaseLeasesRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseLeasesRequest>(create);
  static LeaseLeasesRequest _defaultInstance;
}

class LeaseStatus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseStatus',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aInt64(1, 'ID', protoName: 'ID')
    ..hasRequiredFields = false;

  LeaseStatus._() : super();
  factory LeaseStatus() => create();
  factory LeaseStatus.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseStatus.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseStatus clone() => LeaseStatus()..mergeFromMessage(this);
  LeaseStatus copyWith(void Function(LeaseStatus) updates) =>
      super.copyWith((message) => updates(message as LeaseStatus));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseStatus create() => LeaseStatus._();
  LeaseStatus createEmptyInstance() => create();
  static $pb.PbList<LeaseStatus> createRepeated() => $pb.PbList<LeaseStatus>();
  @$core.pragma('dart2js:noInline')
  static LeaseStatus getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseStatus>(create);
  static LeaseStatus _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class LeaseLeasesResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('LeaseLeasesResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<LeaseStatus>(2, 'leases', $pb.PbFieldType.PM,
        subBuilder: LeaseStatus.create)
    ..hasRequiredFields = false;

  LeaseLeasesResponse._() : super();
  factory LeaseLeasesResponse() => create();
  factory LeaseLeasesResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory LeaseLeasesResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  LeaseLeasesResponse clone() => LeaseLeasesResponse()..mergeFromMessage(this);
  LeaseLeasesResponse copyWith(void Function(LeaseLeasesResponse) updates) =>
      super.copyWith((message) => updates(message as LeaseLeasesResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LeaseLeasesResponse create() => LeaseLeasesResponse._();
  LeaseLeasesResponse createEmptyInstance() => create();
  static $pb.PbList<LeaseLeasesResponse> createRepeated() =>
      $pb.PbList<LeaseLeasesResponse>();
  @$core.pragma('dart2js:noInline')
  static LeaseLeasesResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<LeaseLeasesResponse>(create);
  static LeaseLeasesResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<LeaseStatus> get leases => $_getList(1);
}

class Member extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Member',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'ID', $pb.PbFieldType.OU6,
        protoName: 'ID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOS(2, 'name')
    ..pPS(3, 'peerURLs', protoName: 'peerURLs')
    ..pPS(4, 'clientURLs', protoName: 'clientURLs')
    ..aOB(5, 'isLearner', protoName: 'isLearner')
    ..hasRequiredFields = false;

  Member._() : super();
  factory Member() => create();
  factory Member.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory Member.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  Member clone() => Member()..mergeFromMessage(this);
  Member copyWith(void Function(Member) updates) =>
      super.copyWith((message) => updates(message as Member));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Member create() => Member._();
  Member createEmptyInstance() => create();
  static $pb.PbList<Member> createRepeated() => $pb.PbList<Member>();
  @$core.pragma('dart2js:noInline')
  static Member getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Member>(create);
  static Member _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.String> get peerURLs => $_getList(2);

  @$pb.TagNumber(4)
  $core.List<$core.String> get clientURLs => $_getList(3);

  @$pb.TagNumber(5)
  $core.bool get isLearner => $_getBF(4);
  @$pb.TagNumber(5)
  set isLearner($core.bool v) {
    $_setBool(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasIsLearner() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsLearner() => clearField(5);
}

class MemberAddRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberAddRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..pPS(1, 'peerURLs', protoName: 'peerURLs')
    ..aOB(2, 'isLearner', protoName: 'isLearner')
    ..hasRequiredFields = false;

  MemberAddRequest._() : super();
  factory MemberAddRequest() => create();
  factory MemberAddRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberAddRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberAddRequest clone() => MemberAddRequest()..mergeFromMessage(this);
  MemberAddRequest copyWith(void Function(MemberAddRequest) updates) =>
      super.copyWith((message) => updates(message as MemberAddRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberAddRequest create() => MemberAddRequest._();
  MemberAddRequest createEmptyInstance() => create();
  static $pb.PbList<MemberAddRequest> createRepeated() =>
      $pb.PbList<MemberAddRequest>();
  @$core.pragma('dart2js:noInline')
  static MemberAddRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberAddRequest>(create);
  static MemberAddRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get peerURLs => $_getList(0);

  @$pb.TagNumber(2)
  $core.bool get isLearner => $_getBF(1);
  @$pb.TagNumber(2)
  set isLearner($core.bool v) {
    $_setBool(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasIsLearner() => $_has(1);
  @$pb.TagNumber(2)
  void clearIsLearner() => clearField(2);
}

class MemberAddResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberAddResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aOM<Member>(2, 'member', subBuilder: Member.create)
    ..pc<Member>(3, 'members', $pb.PbFieldType.PM, subBuilder: Member.create)
    ..hasRequiredFields = false;

  MemberAddResponse._() : super();
  factory MemberAddResponse() => create();
  factory MemberAddResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberAddResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberAddResponse clone() => MemberAddResponse()..mergeFromMessage(this);
  MemberAddResponse copyWith(void Function(MemberAddResponse) updates) =>
      super.copyWith((message) => updates(message as MemberAddResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberAddResponse create() => MemberAddResponse._();
  MemberAddResponse createEmptyInstance() => create();
  static $pb.PbList<MemberAddResponse> createRepeated() =>
      $pb.PbList<MemberAddResponse>();
  @$core.pragma('dart2js:noInline')
  static MemberAddResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberAddResponse>(create);
  static MemberAddResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  Member get member => $_getN(1);
  @$pb.TagNumber(2)
  set member(Member v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasMember() => $_has(1);
  @$pb.TagNumber(2)
  void clearMember() => clearField(2);
  @$pb.TagNumber(2)
  Member ensureMember() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.List<Member> get members => $_getList(2);
}

class MemberRemoveRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberRemoveRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'ID', $pb.PbFieldType.OU6,
        protoName: 'ID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false;

  MemberRemoveRequest._() : super();
  factory MemberRemoveRequest() => create();
  factory MemberRemoveRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberRemoveRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberRemoveRequest clone() => MemberRemoveRequest()..mergeFromMessage(this);
  MemberRemoveRequest copyWith(void Function(MemberRemoveRequest) updates) =>
      super.copyWith((message) => updates(message as MemberRemoveRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberRemoveRequest create() => MemberRemoveRequest._();
  MemberRemoveRequest createEmptyInstance() => create();
  static $pb.PbList<MemberRemoveRequest> createRepeated() =>
      $pb.PbList<MemberRemoveRequest>();
  @$core.pragma('dart2js:noInline')
  static MemberRemoveRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberRemoveRequest>(create);
  static MemberRemoveRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class MemberRemoveResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberRemoveResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<Member>(2, 'members', $pb.PbFieldType.PM, subBuilder: Member.create)
    ..hasRequiredFields = false;

  MemberRemoveResponse._() : super();
  factory MemberRemoveResponse() => create();
  factory MemberRemoveResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberRemoveResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberRemoveResponse clone() =>
      MemberRemoveResponse()..mergeFromMessage(this);
  MemberRemoveResponse copyWith(void Function(MemberRemoveResponse) updates) =>
      super.copyWith((message) => updates(message as MemberRemoveResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberRemoveResponse create() => MemberRemoveResponse._();
  MemberRemoveResponse createEmptyInstance() => create();
  static $pb.PbList<MemberRemoveResponse> createRepeated() =>
      $pb.PbList<MemberRemoveResponse>();
  @$core.pragma('dart2js:noInline')
  static MemberRemoveResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberRemoveResponse>(create);
  static MemberRemoveResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Member> get members => $_getList(1);
}

class MemberUpdateRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberUpdateRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'ID', $pb.PbFieldType.OU6,
        protoName: 'ID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..pPS(2, 'peerURLs', protoName: 'peerURLs')
    ..hasRequiredFields = false;

  MemberUpdateRequest._() : super();
  factory MemberUpdateRequest() => create();
  factory MemberUpdateRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberUpdateRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberUpdateRequest clone() => MemberUpdateRequest()..mergeFromMessage(this);
  MemberUpdateRequest copyWith(void Function(MemberUpdateRequest) updates) =>
      super.copyWith((message) => updates(message as MemberUpdateRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberUpdateRequest create() => MemberUpdateRequest._();
  MemberUpdateRequest createEmptyInstance() => create();
  static $pb.PbList<MemberUpdateRequest> createRepeated() =>
      $pb.PbList<MemberUpdateRequest>();
  @$core.pragma('dart2js:noInline')
  static MemberUpdateRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberUpdateRequest>(create);
  static MemberUpdateRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get peerURLs => $_getList(1);
}

class MemberUpdateResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberUpdateResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<Member>(2, 'members', $pb.PbFieldType.PM, subBuilder: Member.create)
    ..hasRequiredFields = false;

  MemberUpdateResponse._() : super();
  factory MemberUpdateResponse() => create();
  factory MemberUpdateResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberUpdateResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberUpdateResponse clone() =>
      MemberUpdateResponse()..mergeFromMessage(this);
  MemberUpdateResponse copyWith(void Function(MemberUpdateResponse) updates) =>
      super.copyWith((message) => updates(message as MemberUpdateResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberUpdateResponse create() => MemberUpdateResponse._();
  MemberUpdateResponse createEmptyInstance() => create();
  static $pb.PbList<MemberUpdateResponse> createRepeated() =>
      $pb.PbList<MemberUpdateResponse>();
  @$core.pragma('dart2js:noInline')
  static MemberUpdateResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberUpdateResponse>(create);
  static MemberUpdateResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Member> get members => $_getList(1);
}

class MemberListRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberListRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  MemberListRequest._() : super();
  factory MemberListRequest() => create();
  factory MemberListRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberListRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberListRequest clone() => MemberListRequest()..mergeFromMessage(this);
  MemberListRequest copyWith(void Function(MemberListRequest) updates) =>
      super.copyWith((message) => updates(message as MemberListRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberListRequest create() => MemberListRequest._();
  MemberListRequest createEmptyInstance() => create();
  static $pb.PbList<MemberListRequest> createRepeated() =>
      $pb.PbList<MemberListRequest>();
  @$core.pragma('dart2js:noInline')
  static MemberListRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberListRequest>(create);
  static MemberListRequest _defaultInstance;
}

class MemberListResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberListResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<Member>(2, 'members', $pb.PbFieldType.PM, subBuilder: Member.create)
    ..hasRequiredFields = false;

  MemberListResponse._() : super();
  factory MemberListResponse() => create();
  factory MemberListResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberListResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberListResponse clone() => MemberListResponse()..mergeFromMessage(this);
  MemberListResponse copyWith(void Function(MemberListResponse) updates) =>
      super.copyWith((message) => updates(message as MemberListResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberListResponse create() => MemberListResponse._();
  MemberListResponse createEmptyInstance() => create();
  static $pb.PbList<MemberListResponse> createRepeated() =>
      $pb.PbList<MemberListResponse>();
  @$core.pragma('dart2js:noInline')
  static MemberListResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberListResponse>(create);
  static MemberListResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Member> get members => $_getList(1);
}

class MemberPromoteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberPromoteRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'ID', $pb.PbFieldType.OU6,
        protoName: 'ID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false;

  MemberPromoteRequest._() : super();
  factory MemberPromoteRequest() => create();
  factory MemberPromoteRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberPromoteRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberPromoteRequest clone() =>
      MemberPromoteRequest()..mergeFromMessage(this);
  MemberPromoteRequest copyWith(void Function(MemberPromoteRequest) updates) =>
      super.copyWith((message) => updates(message as MemberPromoteRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberPromoteRequest create() => MemberPromoteRequest._();
  MemberPromoteRequest createEmptyInstance() => create();
  static $pb.PbList<MemberPromoteRequest> createRepeated() =>
      $pb.PbList<MemberPromoteRequest>();
  @$core.pragma('dart2js:noInline')
  static MemberPromoteRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberPromoteRequest>(create);
  static MemberPromoteRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get iD => $_getI64(0);
  @$pb.TagNumber(1)
  set iD($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class MemberPromoteResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MemberPromoteResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<Member>(2, 'members', $pb.PbFieldType.PM, subBuilder: Member.create)
    ..hasRequiredFields = false;

  MemberPromoteResponse._() : super();
  factory MemberPromoteResponse() => create();
  factory MemberPromoteResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MemberPromoteResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MemberPromoteResponse clone() =>
      MemberPromoteResponse()..mergeFromMessage(this);
  MemberPromoteResponse copyWith(
          void Function(MemberPromoteResponse) updates) =>
      super.copyWith((message) => updates(message as MemberPromoteResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MemberPromoteResponse create() => MemberPromoteResponse._();
  MemberPromoteResponse createEmptyInstance() => create();
  static $pb.PbList<MemberPromoteResponse> createRepeated() =>
      $pb.PbList<MemberPromoteResponse>();
  @$core.pragma('dart2js:noInline')
  static MemberPromoteResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MemberPromoteResponse>(create);
  static MemberPromoteResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Member> get members => $_getList(1);
}

class DefragmentRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DefragmentRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  DefragmentRequest._() : super();
  factory DefragmentRequest() => create();
  factory DefragmentRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DefragmentRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  DefragmentRequest clone() => DefragmentRequest()..mergeFromMessage(this);
  DefragmentRequest copyWith(void Function(DefragmentRequest) updates) =>
      super.copyWith((message) => updates(message as DefragmentRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DefragmentRequest create() => DefragmentRequest._();
  DefragmentRequest createEmptyInstance() => create();
  static $pb.PbList<DefragmentRequest> createRepeated() =>
      $pb.PbList<DefragmentRequest>();
  @$core.pragma('dart2js:noInline')
  static DefragmentRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DefragmentRequest>(create);
  static DefragmentRequest _defaultInstance;
}

class DefragmentResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DefragmentResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  DefragmentResponse._() : super();
  factory DefragmentResponse() => create();
  factory DefragmentResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory DefragmentResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  DefragmentResponse clone() => DefragmentResponse()..mergeFromMessage(this);
  DefragmentResponse copyWith(void Function(DefragmentResponse) updates) =>
      super.copyWith((message) => updates(message as DefragmentResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DefragmentResponse create() => DefragmentResponse._();
  DefragmentResponse createEmptyInstance() => create();
  static $pb.PbList<DefragmentResponse> createRepeated() =>
      $pb.PbList<DefragmentResponse>();
  @$core.pragma('dart2js:noInline')
  static DefragmentResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<DefragmentResponse>(create);
  static DefragmentResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class MoveLeaderRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MoveLeaderRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'targetID', $pb.PbFieldType.OU6,
        protoName: 'targetID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false;

  MoveLeaderRequest._() : super();
  factory MoveLeaderRequest() => create();
  factory MoveLeaderRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MoveLeaderRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MoveLeaderRequest clone() => MoveLeaderRequest()..mergeFromMessage(this);
  MoveLeaderRequest copyWith(void Function(MoveLeaderRequest) updates) =>
      super.copyWith((message) => updates(message as MoveLeaderRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MoveLeaderRequest create() => MoveLeaderRequest._();
  MoveLeaderRequest createEmptyInstance() => create();
  static $pb.PbList<MoveLeaderRequest> createRepeated() =>
      $pb.PbList<MoveLeaderRequest>();
  @$core.pragma('dart2js:noInline')
  static MoveLeaderRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MoveLeaderRequest>(create);
  static MoveLeaderRequest _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get targetID => $_getI64(0);
  @$pb.TagNumber(1)
  set targetID($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasTargetID() => $_has(0);
  @$pb.TagNumber(1)
  void clearTargetID() => clearField(1);
}

class MoveLeaderResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MoveLeaderResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  MoveLeaderResponse._() : super();
  factory MoveLeaderResponse() => create();
  factory MoveLeaderResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MoveLeaderResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MoveLeaderResponse clone() => MoveLeaderResponse()..mergeFromMessage(this);
  MoveLeaderResponse copyWith(void Function(MoveLeaderResponse) updates) =>
      super.copyWith((message) => updates(message as MoveLeaderResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MoveLeaderResponse create() => MoveLeaderResponse._();
  MoveLeaderResponse createEmptyInstance() => create();
  static $pb.PbList<MoveLeaderResponse> createRepeated() =>
      $pb.PbList<MoveLeaderResponse>();
  @$core.pragma('dart2js:noInline')
  static MoveLeaderResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<MoveLeaderResponse>(create);
  static MoveLeaderResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AlarmRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AlarmRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..e<AlarmRequest_AlarmAction>(1, 'action', $pb.PbFieldType.OE,
        defaultOrMaker: AlarmRequest_AlarmAction.GET,
        valueOf: AlarmRequest_AlarmAction.valueOf,
        enumValues: AlarmRequest_AlarmAction.values)
    ..a<$fixnum.Int64>(2, 'memberID', $pb.PbFieldType.OU6,
        protoName: 'memberID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..e<AlarmType>(3, 'alarm', $pb.PbFieldType.OE,
        defaultOrMaker: AlarmType.NONE,
        valueOf: AlarmType.valueOf,
        enumValues: AlarmType.values)
    ..hasRequiredFields = false;

  AlarmRequest._() : super();
  factory AlarmRequest() => create();
  factory AlarmRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AlarmRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AlarmRequest clone() => AlarmRequest()..mergeFromMessage(this);
  AlarmRequest copyWith(void Function(AlarmRequest) updates) =>
      super.copyWith((message) => updates(message as AlarmRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AlarmRequest create() => AlarmRequest._();
  AlarmRequest createEmptyInstance() => create();
  static $pb.PbList<AlarmRequest> createRepeated() =>
      $pb.PbList<AlarmRequest>();
  @$core.pragma('dart2js:noInline')
  static AlarmRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AlarmRequest>(create);
  static AlarmRequest _defaultInstance;

  @$pb.TagNumber(1)
  AlarmRequest_AlarmAction get action => $_getN(0);
  @$pb.TagNumber(1)
  set action(AlarmRequest_AlarmAction v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasAction() => $_has(0);
  @$pb.TagNumber(1)
  void clearAction() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get memberID => $_getI64(1);
  @$pb.TagNumber(2)
  set memberID($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasMemberID() => $_has(1);
  @$pb.TagNumber(2)
  void clearMemberID() => clearField(2);

  @$pb.TagNumber(3)
  AlarmType get alarm => $_getN(2);
  @$pb.TagNumber(3)
  set alarm(AlarmType v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasAlarm() => $_has(2);
  @$pb.TagNumber(3)
  void clearAlarm() => clearField(3);
}

class AlarmMember extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AlarmMember',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'memberID', $pb.PbFieldType.OU6,
        protoName: 'memberID', defaultOrMaker: $fixnum.Int64.ZERO)
    ..e<AlarmType>(2, 'alarm', $pb.PbFieldType.OE,
        defaultOrMaker: AlarmType.NONE,
        valueOf: AlarmType.valueOf,
        enumValues: AlarmType.values)
    ..hasRequiredFields = false;

  AlarmMember._() : super();
  factory AlarmMember() => create();
  factory AlarmMember.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AlarmMember.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AlarmMember clone() => AlarmMember()..mergeFromMessage(this);
  AlarmMember copyWith(void Function(AlarmMember) updates) =>
      super.copyWith((message) => updates(message as AlarmMember));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AlarmMember create() => AlarmMember._();
  AlarmMember createEmptyInstance() => create();
  static $pb.PbList<AlarmMember> createRepeated() => $pb.PbList<AlarmMember>();
  @$core.pragma('dart2js:noInline')
  static AlarmMember getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AlarmMember>(create);
  static AlarmMember _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get memberID => $_getI64(0);
  @$pb.TagNumber(1)
  set memberID($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasMemberID() => $_has(0);
  @$pb.TagNumber(1)
  void clearMemberID() => clearField(1);

  @$pb.TagNumber(2)
  AlarmType get alarm => $_getN(1);
  @$pb.TagNumber(2)
  set alarm(AlarmType v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasAlarm() => $_has(1);
  @$pb.TagNumber(2)
  void clearAlarm() => clearField(2);
}

class AlarmResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AlarmResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<AlarmMember>(2, 'alarms', $pb.PbFieldType.PM,
        subBuilder: AlarmMember.create)
    ..hasRequiredFields = false;

  AlarmResponse._() : super();
  factory AlarmResponse() => create();
  factory AlarmResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AlarmResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AlarmResponse clone() => AlarmResponse()..mergeFromMessage(this);
  AlarmResponse copyWith(void Function(AlarmResponse) updates) =>
      super.copyWith((message) => updates(message as AlarmResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AlarmResponse create() => AlarmResponse._();
  AlarmResponse createEmptyInstance() => create();
  static $pb.PbList<AlarmResponse> createRepeated() =>
      $pb.PbList<AlarmResponse>();
  @$core.pragma('dart2js:noInline')
  static AlarmResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AlarmResponse>(create);
  static AlarmResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<AlarmMember> get alarms => $_getList(1);
}

class StatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('StatusRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  StatusRequest._() : super();
  factory StatusRequest() => create();
  factory StatusRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory StatusRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  StatusRequest clone() => StatusRequest()..mergeFromMessage(this);
  StatusRequest copyWith(void Function(StatusRequest) updates) =>
      super.copyWith((message) => updates(message as StatusRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusRequest create() => StatusRequest._();
  StatusRequest createEmptyInstance() => create();
  static $pb.PbList<StatusRequest> createRepeated() =>
      $pb.PbList<StatusRequest>();
  @$core.pragma('dart2js:noInline')
  static StatusRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<StatusRequest>(create);
  static StatusRequest _defaultInstance;
}

class StatusResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('StatusResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aOS(2, 'version')
    ..aInt64(3, 'dbSize', protoName: 'dbSize')
    ..a<$fixnum.Int64>(4, 'leader', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(5, 'raftIndex', $pb.PbFieldType.OU6,
        protoName: 'raftIndex', defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(6, 'raftTerm', $pb.PbFieldType.OU6,
        protoName: 'raftTerm', defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(7, 'raftAppliedIndex', $pb.PbFieldType.OU6,
        protoName: 'raftAppliedIndex', defaultOrMaker: $fixnum.Int64.ZERO)
    ..pPS(8, 'errors')
    ..aInt64(9, 'dbSizeInUse', protoName: 'dbSizeInUse')
    ..aOB(10, 'isLearner', protoName: 'isLearner')
    ..hasRequiredFields = false;

  StatusResponse._() : super();
  factory StatusResponse() => create();
  factory StatusResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory StatusResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  StatusResponse clone() => StatusResponse()..mergeFromMessage(this);
  StatusResponse copyWith(void Function(StatusResponse) updates) =>
      super.copyWith((message) => updates(message as StatusResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusResponse create() => StatusResponse._();
  StatusResponse createEmptyInstance() => create();
  static $pb.PbList<StatusResponse> createRepeated() =>
      $pb.PbList<StatusResponse>();
  @$core.pragma('dart2js:noInline')
  static StatusResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<StatusResponse>(create);
  static StatusResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get version => $_getSZ(1);
  @$pb.TagNumber(2)
  set version($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasVersion() => $_has(1);
  @$pb.TagNumber(2)
  void clearVersion() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get dbSize => $_getI64(2);
  @$pb.TagNumber(3)
  set dbSize($fixnum.Int64 v) {
    $_setInt64(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasDbSize() => $_has(2);
  @$pb.TagNumber(3)
  void clearDbSize() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get leader => $_getI64(3);
  @$pb.TagNumber(4)
  set leader($fixnum.Int64 v) {
    $_setInt64(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasLeader() => $_has(3);
  @$pb.TagNumber(4)
  void clearLeader() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get raftIndex => $_getI64(4);
  @$pb.TagNumber(5)
  set raftIndex($fixnum.Int64 v) {
    $_setInt64(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasRaftIndex() => $_has(4);
  @$pb.TagNumber(5)
  void clearRaftIndex() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get raftTerm => $_getI64(5);
  @$pb.TagNumber(6)
  set raftTerm($fixnum.Int64 v) {
    $_setInt64(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasRaftTerm() => $_has(5);
  @$pb.TagNumber(6)
  void clearRaftTerm() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get raftAppliedIndex => $_getI64(6);
  @$pb.TagNumber(7)
  set raftAppliedIndex($fixnum.Int64 v) {
    $_setInt64(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasRaftAppliedIndex() => $_has(6);
  @$pb.TagNumber(7)
  void clearRaftAppliedIndex() => clearField(7);

  @$pb.TagNumber(8)
  $core.List<$core.String> get errors => $_getList(7);

  @$pb.TagNumber(9)
  $fixnum.Int64 get dbSizeInUse => $_getI64(8);
  @$pb.TagNumber(9)
  set dbSizeInUse($fixnum.Int64 v) {
    $_setInt64(8, v);
  }

  @$pb.TagNumber(9)
  $core.bool hasDbSizeInUse() => $_has(8);
  @$pb.TagNumber(9)
  void clearDbSizeInUse() => clearField(9);

  @$pb.TagNumber(10)
  $core.bool get isLearner => $_getBF(9);
  @$pb.TagNumber(10)
  set isLearner($core.bool v) {
    $_setBool(9, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasIsLearner() => $_has(9);
  @$pb.TagNumber(10)
  void clearIsLearner() => clearField(10);
}

class AuthEnableRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthEnableRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  AuthEnableRequest._() : super();
  factory AuthEnableRequest() => create();
  factory AuthEnableRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthEnableRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthEnableRequest clone() => AuthEnableRequest()..mergeFromMessage(this);
  AuthEnableRequest copyWith(void Function(AuthEnableRequest) updates) =>
      super.copyWith((message) => updates(message as AuthEnableRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthEnableRequest create() => AuthEnableRequest._();
  AuthEnableRequest createEmptyInstance() => create();
  static $pb.PbList<AuthEnableRequest> createRepeated() =>
      $pb.PbList<AuthEnableRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthEnableRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthEnableRequest>(create);
  static AuthEnableRequest _defaultInstance;
}

class AuthDisableRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthDisableRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  AuthDisableRequest._() : super();
  factory AuthDisableRequest() => create();
  factory AuthDisableRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthDisableRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthDisableRequest clone() => AuthDisableRequest()..mergeFromMessage(this);
  AuthDisableRequest copyWith(void Function(AuthDisableRequest) updates) =>
      super.copyWith((message) => updates(message as AuthDisableRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthDisableRequest create() => AuthDisableRequest._();
  AuthDisableRequest createEmptyInstance() => create();
  static $pb.PbList<AuthDisableRequest> createRepeated() =>
      $pb.PbList<AuthDisableRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthDisableRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthDisableRequest>(create);
  static AuthDisableRequest _defaultInstance;
}

class AuthenticateRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthenticateRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOS(2, 'password')
    ..hasRequiredFields = false;

  AuthenticateRequest._() : super();
  factory AuthenticateRequest() => create();
  factory AuthenticateRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthenticateRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthenticateRequest clone() => AuthenticateRequest()..mergeFromMessage(this);
  AuthenticateRequest copyWith(void Function(AuthenticateRequest) updates) =>
      super.copyWith((message) => updates(message as AuthenticateRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthenticateRequest create() => AuthenticateRequest._();
  AuthenticateRequest createEmptyInstance() => create();
  static $pb.PbList<AuthenticateRequest> createRepeated() =>
      $pb.PbList<AuthenticateRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthenticateRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthenticateRequest>(create);
  static AuthenticateRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class AuthUserAddRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserAddRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOS(2, 'password')
    ..aOM<$3.UserAddOptions>(3, 'options', subBuilder: $3.UserAddOptions.create)
    ..hasRequiredFields = false;

  AuthUserAddRequest._() : super();
  factory AuthUserAddRequest() => create();
  factory AuthUserAddRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserAddRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserAddRequest clone() => AuthUserAddRequest()..mergeFromMessage(this);
  AuthUserAddRequest copyWith(void Function(AuthUserAddRequest) updates) =>
      super.copyWith((message) => updates(message as AuthUserAddRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserAddRequest create() => AuthUserAddRequest._();
  AuthUserAddRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserAddRequest> createRepeated() =>
      $pb.PbList<AuthUserAddRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserAddRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserAddRequest>(create);
  static AuthUserAddRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);

  @$pb.TagNumber(3)
  $3.UserAddOptions get options => $_getN(2);
  @$pb.TagNumber(3)
  set options($3.UserAddOptions v) {
    setField(3, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasOptions() => $_has(2);
  @$pb.TagNumber(3)
  void clearOptions() => clearField(3);
  @$pb.TagNumber(3)
  $3.UserAddOptions ensureOptions() => $_ensure(2);
}

class AuthUserGetRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserGetRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..hasRequiredFields = false;

  AuthUserGetRequest._() : super();
  factory AuthUserGetRequest() => create();
  factory AuthUserGetRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserGetRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserGetRequest clone() => AuthUserGetRequest()..mergeFromMessage(this);
  AuthUserGetRequest copyWith(void Function(AuthUserGetRequest) updates) =>
      super.copyWith((message) => updates(message as AuthUserGetRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserGetRequest create() => AuthUserGetRequest._();
  AuthUserGetRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserGetRequest> createRepeated() =>
      $pb.PbList<AuthUserGetRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserGetRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserGetRequest>(create);
  static AuthUserGetRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);
}

class AuthUserDeleteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserDeleteRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..hasRequiredFields = false;

  AuthUserDeleteRequest._() : super();
  factory AuthUserDeleteRequest() => create();
  factory AuthUserDeleteRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserDeleteRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserDeleteRequest clone() =>
      AuthUserDeleteRequest()..mergeFromMessage(this);
  AuthUserDeleteRequest copyWith(
          void Function(AuthUserDeleteRequest) updates) =>
      super.copyWith((message) => updates(message as AuthUserDeleteRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserDeleteRequest create() => AuthUserDeleteRequest._();
  AuthUserDeleteRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserDeleteRequest> createRepeated() =>
      $pb.PbList<AuthUserDeleteRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserDeleteRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserDeleteRequest>(create);
  static AuthUserDeleteRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);
}

class AuthUserChangePasswordRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthUserChangePasswordRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOS(2, 'password')
    ..hasRequiredFields = false;

  AuthUserChangePasswordRequest._() : super();
  factory AuthUserChangePasswordRequest() => create();
  factory AuthUserChangePasswordRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserChangePasswordRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserChangePasswordRequest clone() =>
      AuthUserChangePasswordRequest()..mergeFromMessage(this);
  AuthUserChangePasswordRequest copyWith(
          void Function(AuthUserChangePasswordRequest) updates) =>
      super.copyWith(
          (message) => updates(message as AuthUserChangePasswordRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserChangePasswordRequest create() =>
      AuthUserChangePasswordRequest._();
  AuthUserChangePasswordRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserChangePasswordRequest> createRepeated() =>
      $pb.PbList<AuthUserChangePasswordRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserChangePasswordRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserChangePasswordRequest>(create);
  static AuthUserChangePasswordRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class AuthUserGrantRoleRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserGrantRoleRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'user')
    ..aOS(2, 'role')
    ..hasRequiredFields = false;

  AuthUserGrantRoleRequest._() : super();
  factory AuthUserGrantRoleRequest() => create();
  factory AuthUserGrantRoleRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserGrantRoleRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserGrantRoleRequest clone() =>
      AuthUserGrantRoleRequest()..mergeFromMessage(this);
  AuthUserGrantRoleRequest copyWith(
          void Function(AuthUserGrantRoleRequest) updates) =>
      super.copyWith((message) => updates(message as AuthUserGrantRoleRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserGrantRoleRequest create() => AuthUserGrantRoleRequest._();
  AuthUserGrantRoleRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserGrantRoleRequest> createRepeated() =>
      $pb.PbList<AuthUserGrantRoleRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserGrantRoleRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserGrantRoleRequest>(create);
  static AuthUserGrantRoleRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get user => $_getSZ(0);
  @$pb.TagNumber(1)
  set user($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get role => $_getSZ(1);
  @$pb.TagNumber(2)
  set role($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRole() => $_has(1);
  @$pb.TagNumber(2)
  void clearRole() => clearField(2);
}

class AuthUserRevokeRoleRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserRevokeRoleRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOS(2, 'role')
    ..hasRequiredFields = false;

  AuthUserRevokeRoleRequest._() : super();
  factory AuthUserRevokeRoleRequest() => create();
  factory AuthUserRevokeRoleRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserRevokeRoleRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserRevokeRoleRequest clone() =>
      AuthUserRevokeRoleRequest()..mergeFromMessage(this);
  AuthUserRevokeRoleRequest copyWith(
          void Function(AuthUserRevokeRoleRequest) updates) =>
      super
          .copyWith((message) => updates(message as AuthUserRevokeRoleRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserRevokeRoleRequest create() => AuthUserRevokeRoleRequest._();
  AuthUserRevokeRoleRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserRevokeRoleRequest> createRepeated() =>
      $pb.PbList<AuthUserRevokeRoleRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserRevokeRoleRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserRevokeRoleRequest>(create);
  static AuthUserRevokeRoleRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get role => $_getSZ(1);
  @$pb.TagNumber(2)
  set role($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasRole() => $_has(1);
  @$pb.TagNumber(2)
  void clearRole() => clearField(2);
}

class AuthRoleAddRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleAddRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..hasRequiredFields = false;

  AuthRoleAddRequest._() : super();
  factory AuthRoleAddRequest() => create();
  factory AuthRoleAddRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleAddRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleAddRequest clone() => AuthRoleAddRequest()..mergeFromMessage(this);
  AuthRoleAddRequest copyWith(void Function(AuthRoleAddRequest) updates) =>
      super.copyWith((message) => updates(message as AuthRoleAddRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleAddRequest create() => AuthRoleAddRequest._();
  AuthRoleAddRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRoleAddRequest> createRepeated() =>
      $pb.PbList<AuthRoleAddRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleAddRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleAddRequest>(create);
  static AuthRoleAddRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);
}

class AuthRoleGetRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleGetRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'role')
    ..hasRequiredFields = false;

  AuthRoleGetRequest._() : super();
  factory AuthRoleGetRequest() => create();
  factory AuthRoleGetRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleGetRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleGetRequest clone() => AuthRoleGetRequest()..mergeFromMessage(this);
  AuthRoleGetRequest copyWith(void Function(AuthRoleGetRequest) updates) =>
      super.copyWith((message) => updates(message as AuthRoleGetRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleGetRequest create() => AuthRoleGetRequest._();
  AuthRoleGetRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRoleGetRequest> createRepeated() =>
      $pb.PbList<AuthRoleGetRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleGetRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleGetRequest>(create);
  static AuthRoleGetRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get role => $_getSZ(0);
  @$pb.TagNumber(1)
  set role($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasRole() => $_has(0);
  @$pb.TagNumber(1)
  void clearRole() => clearField(1);
}

class AuthUserListRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserListRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  AuthUserListRequest._() : super();
  factory AuthUserListRequest() => create();
  factory AuthUserListRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserListRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserListRequest clone() => AuthUserListRequest()..mergeFromMessage(this);
  AuthUserListRequest copyWith(void Function(AuthUserListRequest) updates) =>
      super.copyWith((message) => updates(message as AuthUserListRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserListRequest create() => AuthUserListRequest._();
  AuthUserListRequest createEmptyInstance() => create();
  static $pb.PbList<AuthUserListRequest> createRepeated() =>
      $pb.PbList<AuthUserListRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthUserListRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserListRequest>(create);
  static AuthUserListRequest _defaultInstance;
}

class AuthRoleListRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleListRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..hasRequiredFields = false;

  AuthRoleListRequest._() : super();
  factory AuthRoleListRequest() => create();
  factory AuthRoleListRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleListRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleListRequest clone() => AuthRoleListRequest()..mergeFromMessage(this);
  AuthRoleListRequest copyWith(void Function(AuthRoleListRequest) updates) =>
      super.copyWith((message) => updates(message as AuthRoleListRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleListRequest create() => AuthRoleListRequest._();
  AuthRoleListRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRoleListRequest> createRepeated() =>
      $pb.PbList<AuthRoleListRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleListRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleListRequest>(create);
  static AuthRoleListRequest _defaultInstance;
}

class AuthRoleDeleteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleDeleteRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'role')
    ..hasRequiredFields = false;

  AuthRoleDeleteRequest._() : super();
  factory AuthRoleDeleteRequest() => create();
  factory AuthRoleDeleteRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleDeleteRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleDeleteRequest clone() =>
      AuthRoleDeleteRequest()..mergeFromMessage(this);
  AuthRoleDeleteRequest copyWith(
          void Function(AuthRoleDeleteRequest) updates) =>
      super.copyWith((message) => updates(message as AuthRoleDeleteRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleDeleteRequest create() => AuthRoleDeleteRequest._();
  AuthRoleDeleteRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRoleDeleteRequest> createRepeated() =>
      $pb.PbList<AuthRoleDeleteRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleDeleteRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleDeleteRequest>(create);
  static AuthRoleDeleteRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get role => $_getSZ(0);
  @$pb.TagNumber(1)
  set role($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasRole() => $_has(0);
  @$pb.TagNumber(1)
  void clearRole() => clearField(1);
}

class AuthRoleGrantPermissionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthRoleGrantPermissionRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'name')
    ..aOM<$3.Permission>(2, 'perm', subBuilder: $3.Permission.create)
    ..hasRequiredFields = false;

  AuthRoleGrantPermissionRequest._() : super();
  factory AuthRoleGrantPermissionRequest() => create();
  factory AuthRoleGrantPermissionRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleGrantPermissionRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleGrantPermissionRequest clone() =>
      AuthRoleGrantPermissionRequest()..mergeFromMessage(this);
  AuthRoleGrantPermissionRequest copyWith(
          void Function(AuthRoleGrantPermissionRequest) updates) =>
      super.copyWith(
          (message) => updates(message as AuthRoleGrantPermissionRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleGrantPermissionRequest create() =>
      AuthRoleGrantPermissionRequest._();
  AuthRoleGrantPermissionRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRoleGrantPermissionRequest> createRepeated() =>
      $pb.PbList<AuthRoleGrantPermissionRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleGrantPermissionRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleGrantPermissionRequest>(create);
  static AuthRoleGrantPermissionRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $3.Permission get perm => $_getN(1);
  @$pb.TagNumber(2)
  set perm($3.Permission v) {
    setField(2, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasPerm() => $_has(1);
  @$pb.TagNumber(2)
  void clearPerm() => clearField(2);
  @$pb.TagNumber(2)
  $3.Permission ensurePerm() => $_ensure(1);
}

class AuthRoleRevokePermissionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthRoleRevokePermissionRequest',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOS(1, 'role')
    ..a<$core.List<$core.int>>(2, 'key', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(3, 'rangeEnd', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  AuthRoleRevokePermissionRequest._() : super();
  factory AuthRoleRevokePermissionRequest() => create();
  factory AuthRoleRevokePermissionRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleRevokePermissionRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleRevokePermissionRequest clone() =>
      AuthRoleRevokePermissionRequest()..mergeFromMessage(this);
  AuthRoleRevokePermissionRequest copyWith(
          void Function(AuthRoleRevokePermissionRequest) updates) =>
      super.copyWith(
          (message) => updates(message as AuthRoleRevokePermissionRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleRevokePermissionRequest create() =>
      AuthRoleRevokePermissionRequest._();
  AuthRoleRevokePermissionRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRoleRevokePermissionRequest> createRepeated() =>
      $pb.PbList<AuthRoleRevokePermissionRequest>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleRevokePermissionRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleRevokePermissionRequest>(
          create);
  static AuthRoleRevokePermissionRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get role => $_getSZ(0);
  @$pb.TagNumber(1)
  set role($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasRole() => $_has(0);
  @$pb.TagNumber(1)
  void clearRole() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get key => $_getN(1);
  @$pb.TagNumber(2)
  set key($core.List<$core.int> v) {
    $_setBytes(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasKey() => $_has(1);
  @$pb.TagNumber(2)
  void clearKey() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get rangeEnd => $_getN(2);
  @$pb.TagNumber(3)
  set rangeEnd($core.List<$core.int> v) {
    $_setBytes(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasRangeEnd() => $_has(2);
  @$pb.TagNumber(3)
  void clearRangeEnd() => clearField(3);
}

class AuthEnableResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthEnableResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthEnableResponse._() : super();
  factory AuthEnableResponse() => create();
  factory AuthEnableResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthEnableResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthEnableResponse clone() => AuthEnableResponse()..mergeFromMessage(this);
  AuthEnableResponse copyWith(void Function(AuthEnableResponse) updates) =>
      super.copyWith((message) => updates(message as AuthEnableResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthEnableResponse create() => AuthEnableResponse._();
  AuthEnableResponse createEmptyInstance() => create();
  static $pb.PbList<AuthEnableResponse> createRepeated() =>
      $pb.PbList<AuthEnableResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthEnableResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthEnableResponse>(create);
  static AuthEnableResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthDisableResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthDisableResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthDisableResponse._() : super();
  factory AuthDisableResponse() => create();
  factory AuthDisableResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthDisableResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthDisableResponse clone() => AuthDisableResponse()..mergeFromMessage(this);
  AuthDisableResponse copyWith(void Function(AuthDisableResponse) updates) =>
      super.copyWith((message) => updates(message as AuthDisableResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthDisableResponse create() => AuthDisableResponse._();
  AuthDisableResponse createEmptyInstance() => create();
  static $pb.PbList<AuthDisableResponse> createRepeated() =>
      $pb.PbList<AuthDisableResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthDisableResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthDisableResponse>(create);
  static AuthDisableResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthenticateResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthenticateResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..aOS(2, 'token')
    ..hasRequiredFields = false;

  AuthenticateResponse._() : super();
  factory AuthenticateResponse() => create();
  factory AuthenticateResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthenticateResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthenticateResponse clone() =>
      AuthenticateResponse()..mergeFromMessage(this);
  AuthenticateResponse copyWith(void Function(AuthenticateResponse) updates) =>
      super.copyWith((message) => updates(message as AuthenticateResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthenticateResponse create() => AuthenticateResponse._();
  AuthenticateResponse createEmptyInstance() => create();
  static $pb.PbList<AuthenticateResponse> createRepeated() =>
      $pb.PbList<AuthenticateResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthenticateResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthenticateResponse>(create);
  static AuthenticateResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get token => $_getSZ(1);
  @$pb.TagNumber(2)
  set token($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearToken() => clearField(2);
}

class AuthUserAddResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserAddResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthUserAddResponse._() : super();
  factory AuthUserAddResponse() => create();
  factory AuthUserAddResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserAddResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserAddResponse clone() => AuthUserAddResponse()..mergeFromMessage(this);
  AuthUserAddResponse copyWith(void Function(AuthUserAddResponse) updates) =>
      super.copyWith((message) => updates(message as AuthUserAddResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserAddResponse create() => AuthUserAddResponse._();
  AuthUserAddResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserAddResponse> createRepeated() =>
      $pb.PbList<AuthUserAddResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserAddResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserAddResponse>(create);
  static AuthUserAddResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthUserGetResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserGetResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pPS(2, 'roles')
    ..hasRequiredFields = false;

  AuthUserGetResponse._() : super();
  factory AuthUserGetResponse() => create();
  factory AuthUserGetResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserGetResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserGetResponse clone() => AuthUserGetResponse()..mergeFromMessage(this);
  AuthUserGetResponse copyWith(void Function(AuthUserGetResponse) updates) =>
      super.copyWith((message) => updates(message as AuthUserGetResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserGetResponse create() => AuthUserGetResponse._();
  AuthUserGetResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserGetResponse> createRepeated() =>
      $pb.PbList<AuthUserGetResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserGetResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserGetResponse>(create);
  static AuthUserGetResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.String> get roles => $_getList(1);
}

class AuthUserDeleteResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserDeleteResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthUserDeleteResponse._() : super();
  factory AuthUserDeleteResponse() => create();
  factory AuthUserDeleteResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserDeleteResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserDeleteResponse clone() =>
      AuthUserDeleteResponse()..mergeFromMessage(this);
  AuthUserDeleteResponse copyWith(
          void Function(AuthUserDeleteResponse) updates) =>
      super.copyWith((message) => updates(message as AuthUserDeleteResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserDeleteResponse create() => AuthUserDeleteResponse._();
  AuthUserDeleteResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserDeleteResponse> createRepeated() =>
      $pb.PbList<AuthUserDeleteResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserDeleteResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserDeleteResponse>(create);
  static AuthUserDeleteResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthUserChangePasswordResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthUserChangePasswordResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthUserChangePasswordResponse._() : super();
  factory AuthUserChangePasswordResponse() => create();
  factory AuthUserChangePasswordResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserChangePasswordResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserChangePasswordResponse clone() =>
      AuthUserChangePasswordResponse()..mergeFromMessage(this);
  AuthUserChangePasswordResponse copyWith(
          void Function(AuthUserChangePasswordResponse) updates) =>
      super.copyWith(
          (message) => updates(message as AuthUserChangePasswordResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserChangePasswordResponse create() =>
      AuthUserChangePasswordResponse._();
  AuthUserChangePasswordResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserChangePasswordResponse> createRepeated() =>
      $pb.PbList<AuthUserChangePasswordResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserChangePasswordResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserChangePasswordResponse>(create);
  static AuthUserChangePasswordResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthUserGrantRoleResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserGrantRoleResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthUserGrantRoleResponse._() : super();
  factory AuthUserGrantRoleResponse() => create();
  factory AuthUserGrantRoleResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserGrantRoleResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserGrantRoleResponse clone() =>
      AuthUserGrantRoleResponse()..mergeFromMessage(this);
  AuthUserGrantRoleResponse copyWith(
          void Function(AuthUserGrantRoleResponse) updates) =>
      super
          .copyWith((message) => updates(message as AuthUserGrantRoleResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserGrantRoleResponse create() => AuthUserGrantRoleResponse._();
  AuthUserGrantRoleResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserGrantRoleResponse> createRepeated() =>
      $pb.PbList<AuthUserGrantRoleResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserGrantRoleResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserGrantRoleResponse>(create);
  static AuthUserGrantRoleResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthUserRevokeRoleResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthUserRevokeRoleResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthUserRevokeRoleResponse._() : super();
  factory AuthUserRevokeRoleResponse() => create();
  factory AuthUserRevokeRoleResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserRevokeRoleResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserRevokeRoleResponse clone() =>
      AuthUserRevokeRoleResponse()..mergeFromMessage(this);
  AuthUserRevokeRoleResponse copyWith(
          void Function(AuthUserRevokeRoleResponse) updates) =>
      super.copyWith(
          (message) => updates(message as AuthUserRevokeRoleResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserRevokeRoleResponse create() => AuthUserRevokeRoleResponse._();
  AuthUserRevokeRoleResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserRevokeRoleResponse> createRepeated() =>
      $pb.PbList<AuthUserRevokeRoleResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserRevokeRoleResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserRevokeRoleResponse>(create);
  static AuthUserRevokeRoleResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthRoleAddResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleAddResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthRoleAddResponse._() : super();
  factory AuthRoleAddResponse() => create();
  factory AuthRoleAddResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleAddResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleAddResponse clone() => AuthRoleAddResponse()..mergeFromMessage(this);
  AuthRoleAddResponse copyWith(void Function(AuthRoleAddResponse) updates) =>
      super.copyWith((message) => updates(message as AuthRoleAddResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleAddResponse create() => AuthRoleAddResponse._();
  AuthRoleAddResponse createEmptyInstance() => create();
  static $pb.PbList<AuthRoleAddResponse> createRepeated() =>
      $pb.PbList<AuthRoleAddResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleAddResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleAddResponse>(create);
  static AuthRoleAddResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthRoleGetResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleGetResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pc<$3.Permission>(2, 'perm', $pb.PbFieldType.PM,
        subBuilder: $3.Permission.create)
    ..hasRequiredFields = false;

  AuthRoleGetResponse._() : super();
  factory AuthRoleGetResponse() => create();
  factory AuthRoleGetResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleGetResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleGetResponse clone() => AuthRoleGetResponse()..mergeFromMessage(this);
  AuthRoleGetResponse copyWith(void Function(AuthRoleGetResponse) updates) =>
      super.copyWith((message) => updates(message as AuthRoleGetResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleGetResponse create() => AuthRoleGetResponse._();
  AuthRoleGetResponse createEmptyInstance() => create();
  static $pb.PbList<AuthRoleGetResponse> createRepeated() =>
      $pb.PbList<AuthRoleGetResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleGetResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleGetResponse>(create);
  static AuthRoleGetResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$3.Permission> get perm => $_getList(1);
}

class AuthRoleListResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleListResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pPS(2, 'roles')
    ..hasRequiredFields = false;

  AuthRoleListResponse._() : super();
  factory AuthRoleListResponse() => create();
  factory AuthRoleListResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleListResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleListResponse clone() =>
      AuthRoleListResponse()..mergeFromMessage(this);
  AuthRoleListResponse copyWith(void Function(AuthRoleListResponse) updates) =>
      super.copyWith((message) => updates(message as AuthRoleListResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleListResponse create() => AuthRoleListResponse._();
  AuthRoleListResponse createEmptyInstance() => create();
  static $pb.PbList<AuthRoleListResponse> createRepeated() =>
      $pb.PbList<AuthRoleListResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleListResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleListResponse>(create);
  static AuthRoleListResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.String> get roles => $_getList(1);
}

class AuthUserListResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthUserListResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..pPS(2, 'users')
    ..hasRequiredFields = false;

  AuthUserListResponse._() : super();
  factory AuthUserListResponse() => create();
  factory AuthUserListResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthUserListResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthUserListResponse clone() =>
      AuthUserListResponse()..mergeFromMessage(this);
  AuthUserListResponse copyWith(void Function(AuthUserListResponse) updates) =>
      super.copyWith((message) => updates(message as AuthUserListResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthUserListResponse create() => AuthUserListResponse._();
  AuthUserListResponse createEmptyInstance() => create();
  static $pb.PbList<AuthUserListResponse> createRepeated() =>
      $pb.PbList<AuthUserListResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthUserListResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthUserListResponse>(create);
  static AuthUserListResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$core.String> get users => $_getList(1);
}

class AuthRoleDeleteResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRoleDeleteResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthRoleDeleteResponse._() : super();
  factory AuthRoleDeleteResponse() => create();
  factory AuthRoleDeleteResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleDeleteResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleDeleteResponse clone() =>
      AuthRoleDeleteResponse()..mergeFromMessage(this);
  AuthRoleDeleteResponse copyWith(
          void Function(AuthRoleDeleteResponse) updates) =>
      super.copyWith((message) => updates(message as AuthRoleDeleteResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleDeleteResponse create() => AuthRoleDeleteResponse._();
  AuthRoleDeleteResponse createEmptyInstance() => create();
  static $pb.PbList<AuthRoleDeleteResponse> createRepeated() =>
      $pb.PbList<AuthRoleDeleteResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleDeleteResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleDeleteResponse>(create);
  static AuthRoleDeleteResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthRoleGrantPermissionResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthRoleGrantPermissionResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthRoleGrantPermissionResponse._() : super();
  factory AuthRoleGrantPermissionResponse() => create();
  factory AuthRoleGrantPermissionResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleGrantPermissionResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleGrantPermissionResponse clone() =>
      AuthRoleGrantPermissionResponse()..mergeFromMessage(this);
  AuthRoleGrantPermissionResponse copyWith(
          void Function(AuthRoleGrantPermissionResponse) updates) =>
      super.copyWith(
          (message) => updates(message as AuthRoleGrantPermissionResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleGrantPermissionResponse create() =>
      AuthRoleGrantPermissionResponse._();
  AuthRoleGrantPermissionResponse createEmptyInstance() => create();
  static $pb.PbList<AuthRoleGrantPermissionResponse> createRepeated() =>
      $pb.PbList<AuthRoleGrantPermissionResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleGrantPermissionResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleGrantPermissionResponse>(
          create);
  static AuthRoleGrantPermissionResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

class AuthRoleRevokePermissionResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(
      'AuthRoleRevokePermissionResponse',
      package: const $pb.PackageName('etcdserverpb'),
      createEmptyInstance: create)
    ..aOM<ResponseHeader>(1, 'header', subBuilder: ResponseHeader.create)
    ..hasRequiredFields = false;

  AuthRoleRevokePermissionResponse._() : super();
  factory AuthRoleRevokePermissionResponse() => create();
  factory AuthRoleRevokePermissionResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory AuthRoleRevokePermissionResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  AuthRoleRevokePermissionResponse clone() =>
      AuthRoleRevokePermissionResponse()..mergeFromMessage(this);
  AuthRoleRevokePermissionResponse copyWith(
          void Function(AuthRoleRevokePermissionResponse) updates) =>
      super.copyWith(
          (message) => updates(message as AuthRoleRevokePermissionResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRoleRevokePermissionResponse create() =>
      AuthRoleRevokePermissionResponse._();
  AuthRoleRevokePermissionResponse createEmptyInstance() => create();
  static $pb.PbList<AuthRoleRevokePermissionResponse> createRepeated() =>
      $pb.PbList<AuthRoleRevokePermissionResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthRoleRevokePermissionResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<AuthRoleRevokePermissionResponse>(
          create);
  static AuthRoleRevokePermissionResponse _defaultInstance;

  @$pb.TagNumber(1)
  ResponseHeader get header => $_getN(0);
  @$pb.TagNumber(1)
  set header(ResponseHeader v) {
    setField(1, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasHeader() => $_has(0);
  @$pb.TagNumber(1)
  void clearHeader() => clearField(1);
  @$pb.TagNumber(1)
  ResponseHeader ensureHeader() => $_ensure(0);
}

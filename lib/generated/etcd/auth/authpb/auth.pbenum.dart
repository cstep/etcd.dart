// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: etcd/auth/authpb/auth.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Permission_Type extends $pb.ProtobufEnum {
  static const Permission_Type READ = Permission_Type._(0, 'READ');
  static const Permission_Type WRITE = Permission_Type._(1, 'WRITE');
  static const Permission_Type READWRITE = Permission_Type._(2, 'READWRITE');

  static const $core.List<Permission_Type> values = <Permission_Type>[
    READ,
    WRITE,
    READWRITE,
  ];

  static final $core.Map<$core.int, Permission_Type> _byValue =
      $pb.ProtobufEnum.initByValue(values);
  static Permission_Type valueOf($core.int value) => _byValue[value];

  const Permission_Type._($core.int v, $core.String n) : super(v, n);
}

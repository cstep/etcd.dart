// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: etcd/auth/authpb/auth.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const UserAddOptions$json = const {
  '1': 'UserAddOptions',
  '2': const [
    const {'1': 'no_password', '3': 1, '4': 1, '5': 8, '10': 'noPassword'},
  ],
};

const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 12, '10': 'name'},
    const {'1': 'password', '3': 2, '4': 1, '5': 12, '10': 'password'},
    const {'1': 'roles', '3': 3, '4': 3, '5': 9, '10': 'roles'},
    const {
      '1': 'options',
      '3': 4,
      '4': 1,
      '5': 11,
      '6': '.authpb.UserAddOptions',
      '10': 'options'
    },
  ],
};

const Permission$json = const {
  '1': 'Permission',
  '2': const [
    const {
      '1': 'permType',
      '3': 1,
      '4': 1,
      '5': 14,
      '6': '.authpb.Permission.Type',
      '10': 'permType'
    },
    const {'1': 'key', '3': 2, '4': 1, '5': 12, '10': 'key'},
    const {'1': 'range_end', '3': 3, '4': 1, '5': 12, '10': 'rangeEnd'},
  ],
  '4': const [Permission_Type$json],
};

const Permission_Type$json = const {
  '1': 'Type',
  '2': const [
    const {'1': 'READ', '2': 0},
    const {'1': 'WRITE', '2': 1},
    const {'1': 'READWRITE', '2': 2},
  ],
};

const Role$json = const {
  '1': 'Role',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 12, '10': 'name'},
    const {
      '1': 'keyPermission',
      '3': 2,
      '4': 3,
      '5': 11,
      '6': '.authpb.Permission',
      '10': 'keyPermission'
    },
  ],
};

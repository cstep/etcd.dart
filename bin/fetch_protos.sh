#!/bin/bash

set -e # abort all if any command fails
set -u # error if referencing non-existent variables

DEST_DIR="../protos"
BAK_DIR="${DEST_DIR}.bak.dir"
TEMP_DIR="${DEST_DIR}.temp.dir"

VERSION_ETCD="v3.4.0"
VERSION_GOGO="v1.3.1"
VERSION_GAPI="v0.1.0"

# set our working directory
cd "$(cygpath "$(dirname "${BASH_SOURCE[0]}")")"

rm -rf "$BAK_DIR"
rm -rf "$TEMP_DIR"
mkdir -p "$DEST_DIR"



SUB_PATH="etcdserver/etcdserverpb"
TARGET="${DEST_DIR}.temp.dir/etcd/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/etcd-io/etcd/${VERSION_ETCD}/${SUB_PATH}/rpc.proto" > "${TARGET}/rpc.proto"
set +x # stop showing commands as they are run

SUB_PATH="auth/authpb"
TARGET="${DEST_DIR}.temp.dir/etcd/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/etcd-io/etcd/${VERSION_ETCD}/${SUB_PATH}/auth.proto" > "${TARGET}/auth.proto"
set +x # stop showing commands as they are run

SUB_PATH="mvcc/mvccpb"
TARGET="${DEST_DIR}.temp.dir/etcd/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/etcd-io/etcd/${VERSION_ETCD}/${SUB_PATH}/kv.proto" > "${TARGET}/kv.proto"
set +x # stop showing commands as they are run

SUB_PATH=""
TARGET="${DEST_DIR}.temp.dir/gogoproto/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/gogo/protobuf/${VERSION_GOGO}/gogoproto/${SUB_PATH}/gogo.proto" > "${TARGET}/gogo.proto"
set +x # stop showing commands as they are run

SUB_PATH="api"
TARGET="${DEST_DIR}.temp.dir/google/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/googleapis/api-common-protos/${VERSION_GAPI}/google/${SUB_PATH}/annotations.proto" > "${TARGET}/annotations.proto"
set +x # stop showing commands as they are run

SUB_PATH="api"
TARGET="${DEST_DIR}.temp.dir/google/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/googleapis/api-common-protos/${VERSION_GAPI}/google/${SUB_PATH}/http.proto" > "${TARGET}/http.proto"
set +x # stop showing commands as they are run



mv "$DEST_DIR" "$BAK_DIR" && rm -rf "$DEST_DIR" && mv "$TEMP_DIR" "$DEST_DIR" && rm -rf "$BAK_DIR"

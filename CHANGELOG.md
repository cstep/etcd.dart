## 0.0.1

- Initial version, created by Stagehand

## 0.0.2

- pub health adjustments

## 0.0.3

- Modify generated grpc files to turn off some linter errors
- Support for `compact` requests
- Support for `transaction` requests
- Support for key prefixes

## 0.0.4

- Minor Internal Refactorings
- Add generic type to kv.fetch() method
- Add `isShutdown` and `isShuttingDown` flags

## 0.0.5

- Hold back grpc dependency version to avoid breaking change in grpc 2.8.0 until we can properly update

## 0.0.6

- update all dependencies to most recent versions (including grpc which was held back in the last release)

import 'dart:convert';
import 'dart:mirrors';

import 'package:etcd/etcd.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:grpc/grpc.dart';
import 'package:grpc/service_api.dart';
import 'package:test/test.dart';

void main() {
  /// tests to ensure all services declared in the proto files are exposed by the EtcdClient
  group("Exposes ETCD APIs", () {
    Map<Symbol, ClassMirror> declaredServices = Map.fromEntries(currentMirrorSystem().libraries.entries
      .firstWhere((libEntry) => libEntry.key.toString().contains('etcdserverpb/rpc.pbgrpc.dart')).value.declarations.entries
      .where((declarationEntry) {
        DeclarationMirror concreteClassMirror = declarationEntry.value;

        return (concreteClassMirror is ClassMirror && concreteClassMirror.isSubclassOf(reflectClass(Client)) && !concreteClassMirror.isAbstract);
      })
      .map((entry) => new MapEntry(entry.key, entry.value as ClassMirror)));

    Map<Symbol, ClassMirror> implementedServices = Map.fromEntries(reflectClass(EtcdClient).declarations.entries
      .where((declarationEntry) {
        if (declarationEntry.value is! VariableMirror) return false;

        DeclarationMirror concreteClassMirror = (declarationEntry.value as VariableMirror).type;
        return (concreteClassMirror is ClassMirror && concreteClassMirror.isSubtypeOf(reflectClass(Client)) && !concreteClassMirror.isAbstract);
      })
      .map((entry) => new MapEntry(entry.key, (entry.value as VariableMirror).type as ClassMirror)));

    var ignoredServices = ['AuthClient', 'ClusterClient', 'MaintenanceClient'];

    declaredServices
      ..removeWhere((Symbol service, _) => ignoredServices.contains(MirrorSystem.getName(service)))
      ..forEach((key, mirror) {
      var declaredName = MirrorSystem.getName(key);

      test('Exposes `$declaredName`', () {
        expect(implementedServices.values, anyElement((ClassMirror implementationMirror) {
          var implementedName = MirrorSystem.getName(implementationMirror.simpleName);

          return (implementedName == 'Etcd${declaredName}');
        }));
      });
    });
  });

  test("constructor", () {
    var expectedHost     = faker.internet.ipv4Address();
    var expectedPort     = faker.randomGenerator.int(only32bit: true);
    var expectedEncoding = ascii;
    var expectedOptions  = new ChannelOptions();

    var client = new EtcdClient(expectedHost, port: expectedPort, encoding: expectedEncoding, options: expectedOptions);

    // -------- public properties
    expect(client.kv,    isA<EtcdKVClient>(),    reason: 'exposed kv client other than expected'   );
    expect(client.lease, isA<EtcdLeaseClient>(), reason: 'exposed lease client other than expected');
    expect(client.watch, isA<EtcdWatchClient>(), reason: 'exposed watch client other than expected');

    // -------- private properties
    var clientMirror   = reflect(client);
    var channelSymbol  = MirrorSystem.getSymbol('_channel', clientMirror.type.owner);
    var encodingSymbol = MirrorSystem.getSymbol('_encoding', clientMirror.type.owner);

    var actualChannel  = clientMirror.getField(channelSymbol).reflectee;
    var actualEncoding = clientMirror.getField(encodingSymbol).reflectee;

    expect(actualChannel.host,    equals(expectedHost),     reason: 'Configured host other than expected'    );
    expect(actualChannel.port,    equals(expectedPort),     reason: 'Configured port other than expected'    );
    expect(actualChannel.options, equals(expectedOptions),  reason: 'Configured options other than expected' );
    expect(actualEncoding,        equals(expectedEncoding), reason: 'Configured encoding other than expected');
  });

  test('shutdown()', () {
    // should call:
    //   _channel.shutdown()
    //   lease.shutdown()
    //   watch.shutdown()
  }, skip: 'not implemented');

  test('getConnection()', () {
    // should call:
    //   _channel.getConnection()
  }, skip: 'not implemented');
}

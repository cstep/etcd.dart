import 'dart:convert';
import 'dart:mirrors';

import 'package:etcd/etcd.dart';
import 'package:etcd/generated/etcd/etcdserver/etcdserverpb/rpc.pbgrpc.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import '../helpers/TestUtils.dart';
import '../mocks/GrpcMocks.dart';

void main() {
  GrpcChannelMock  channelMock;
  EtcdKVClient     client;
  GrpcKVClientMock coreKvMock;
  Encoding         encoding;

  setUp(() {
    encoding    = ascii;
    channelMock = new GrpcChannelMock();
    client      = new EtcdKVClient(channelMock, encoding);
    coreKvMock  = new GrpcKVClientMock(channelMock);

    // -------- inject our mocked delegate into our real kvClient
    var clientMirror   = reflect(client);
    var delegateSymbol = MirrorSystem.getSymbol('_delegate', clientMirror.type.superclass.owner);
    clientMirror.setField(delegateSymbol, coreKvMock);
  });

  test('put()', () {
    var expectedValues = {
      'key'        : faker.lorem.word(),
      'value'      : faker.lorem.sentence(),
      'lease'      : faker.randomGenerator.int(),
      'prevKv'     : faker.randomGenerator.boolean(),
      'ignoreValue': faker.randomGenerator.boolean(),
      'ignoreLease': faker.randomGenerator.boolean(),
    };

    var result = client.put(
      expectedValues['key'],
      expectedValues['value'],
      lease      : expectedValues['lease'],
      prevKv     : expectedValues['prevKv'],
      ignoreValue: expectedValues['ignoreValue'],
      ignoreLease: expectedValues['ignoreLease']
    );
    expect(result, isA<Future<String>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.put(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('fetch()', () {
    var expectedValues = {
      'key'         : faker.lorem.word(),
      'limit'       : 1,
      'revision'    : faker.randomGenerator.int(),
      'serializable': faker.randomGenerator.boolean(),
    };

    var result = client.fetch(expectedValues['key'],
      revision: expectedValues['revision'],
      serializable: expectedValues['serializable']
    );
    expect(result, isA<Future<dynamic>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.range(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('fetchPrefix()', () {
    var expectedValues = {
      'key'              : faker.lorem.word(),
      'limit'            : faker.randomGenerator.int(only32bit: true),
      'revision'         : faker.randomGenerator.int(),
      'serializable'     : faker.randomGenerator.boolean(),
      'keysOnly'         : faker.randomGenerator.boolean(),
      'countOnly'        : faker.randomGenerator.boolean(),
      'minModRevision'   : faker.randomGenerator.int(),
      'maxModRevision'   : faker.randomGenerator.int(),
      'minCreateRevision': faker.randomGenerator.int(),
      'maxCreateRevision': faker.randomGenerator.int(),
      'sortOrder'        : faker.randomGenerator.element(RangeRequest_SortOrder.values),
      'sortTarget'       : faker.randomGenerator.element(RangeRequest_SortTarget.values),
    };

    var result = client.fetchPrefix(
      expectedValues['key'],
      limit            : expectedValues['limit'],
      revision         : expectedValues['revision'],
      serializable     : expectedValues['serializable'],
      keysOnly         : expectedValues['keysOnly'],
      countOnly        : expectedValues['countOnly'],
      minModRevision   : expectedValues['minModRevision'],
      maxModRevision   : expectedValues['maxModRevision'],
      minCreateRevision: expectedValues['minCreateRevision'],
      maxCreateRevision: expectedValues['maxCreateRevision'],
      sortOrder        : expectedValues['sortOrder'],
      sortTarget       : expectedValues['sortTarget'],
    );
    expect(result, isA<Future<EtcdFetchRangeResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.range(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");

    expectedValues['rangeEnd'] = encoding.decode(encoding.encode(expectedValues['key'])..last += 1); // -- increment the last byte of key to indicate it's a prefix (see KVClient._fetchRangeRequest())
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('fetchRange()', () {
    var expectedValues = {
      'key'              : faker.lorem.word(),
      'rangeEnd'         : faker.lorem.word(),
      'limit'            : faker.randomGenerator.int(only32bit: true),
      'revision'         : faker.randomGenerator.int(),
      'serializable'     : faker.randomGenerator.boolean(),
      'keysOnly'         : faker.randomGenerator.boolean(),
      'countOnly'        : faker.randomGenerator.boolean(),
      'minModRevision'   : faker.randomGenerator.int(),
      'maxModRevision'   : faker.randomGenerator.int(),
      'minCreateRevision': faker.randomGenerator.int(),
      'maxCreateRevision': faker.randomGenerator.int(),
      'sortOrder'        : faker.randomGenerator.element(RangeRequest_SortOrder.values),
      'sortTarget'       : faker.randomGenerator.element(RangeRequest_SortTarget.values),
    };

    var result = client.fetchRange(
      expectedValues['key'],
      expectedValues['rangeEnd'],
      limit            : expectedValues['limit'],
      revision         : expectedValues['revision'],
      serializable     : expectedValues['serializable'],
      keysOnly         : expectedValues['keysOnly'],
      countOnly        : expectedValues['countOnly'],
      minModRevision   : expectedValues['minModRevision'],
      maxModRevision   : expectedValues['maxModRevision'],
      minCreateRevision: expectedValues['minCreateRevision'],
      maxCreateRevision: expectedValues['maxCreateRevision'],
      sortOrder        : expectedValues['sortOrder'],
      sortTarget       : expectedValues['sortTarget'],
    );
    expect(result, isA<Future<EtcdFetchRangeResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.range(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");

    expectedValues['rangeEnd'] = encoding.decode(encoding.encode(expectedValues['rangeEnd'])..last += 1); // -- increment the last byte of rangeEnd to make it inclusive (see KVClient._fetchRangeRequest())
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('delete()', () {
    var expectedValues = {
      'key'   : faker.lorem.word(),
      'prevKv': faker.randomGenerator.boolean(),
    };

    var result = client.delete(
      expectedValues['key'],
      prevKv: expectedValues['prevKv']
    );
    expect(result, isA<Future<EtcdKeyValue>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.deleteRange(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('deletePrefix()', () {
    var expectedValues = {
      'key'     : faker.lorem.word(),
      'prevKv'  : faker.randomGenerator.boolean(),
    };

    var result = client.deletePrefix(
      expectedValues['key'],
      prevKv: expectedValues['prevKv']
    );
    expect(result, isA<Future<EtcdDeleteRangeResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.deleteRange(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");

    expectedValues['rangeEnd'] = encoding.decode(encoding.encode(expectedValues['key'])..last += 1); // -- increment the last byte of key to indicate it's a prefix (see KVClient._deleteRangeRequest())
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('deleteRange()', () {
    var expectedValues = {
      'key'     : faker.lorem.word(),
      'rangeEnd': faker.lorem.word(),
      'prevKv'  : faker.randomGenerator.boolean(),
    };

    var result = client.deleteRange(
      expectedValues['key'],
      expectedValues['rangeEnd'],
      prevKv: expectedValues['prevKv']
    );
    expect(result, isA<Future<EtcdDeleteRangeResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.deleteRange(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");

    expectedValues['rangeEnd'] = encoding.decode(encoding.encode(expectedValues['rangeEnd'])..last += 1); // -- increment the last byte of rangeEnd to make it inclusive (see KVClient._deleteRangeRequest())
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('compact()', () {
    var expectedValues = {
      'revision': faker.random.int(),
    };

    var result = client.compact(
      expectedValues['revision'],
    );
    expect(result, isA<Future<void>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreKvMock.compact(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });
}

import 'dart:convert';
import 'package:protobuf/protobuf.dart';
import 'package:test/test.dart';
import 'GrpcUtils.dart';

abstract class TestUtils {
  static void verifySingleRequestKeys(GeneratedMessage request, Map<String, dynamic> expectedValues, [Encoding encoding]) {
    var parsedRawRequest = GrpcUtils.parseRawRequest(request, encoding);

    expectedValues.forEach((key, value) {
      if (value is Duration) {
        value = value.inSeconds;
      }
      expect(parsedRawRequest, containsPair(key, value), reason: 'Serialized value for `$key` other than expected');
    });

    expect(parsedRawRequest.keys, hasLength(expectedValues.length), reason: 'Serialized request contains additional, unexpected keys');
  }

  static void verifyRepeatedRequestKeys(Stream<GeneratedMessage> requestStream, List<Map<String, dynamic>> expectedValues, [Encoding encoding]) {
    requestStream.forEach((GeneratedMessage request) {
      verifySingleRequestKeys(request, expectedValues.removeAt(0), encoding);
    });
  }
}

import 'package:test/test.dart';

Matcher containedWithin(List haystack) => predicate((needle) => (haystack??[]).contains(needle));

Matcher containsKey(dynamic key) => containsPair(key, anything);

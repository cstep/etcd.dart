import 'dart:convert';

import 'package:fixnum/fixnum.dart';
import 'package:protobuf/protobuf.dart';

abstract class GrpcUtils {
  static Map<String, dynamic> messageToMap(GeneratedMessage message, [bool onlyPresent = true]) {
    if (message == null) {
      return null;
    }

    Map<String, dynamic> namedValues = {};
    Iterable<FieldInfo>  fields      = message.info_.fieldInfo.values;

    for (var field in fields) {
      if (onlyPresent == true && !message.hasField(field.tagNumber)) {
        continue;
      }
      // else

      var value = message.getFieldOrNull(field.tagNumber);
      if (value is Iterable) {
        value = (value as Iterable).toList();
      }

      namedValues[field.name] = value;
    }

    return namedValues;
  }

  static Map<String, dynamic> parseRawRequest(GeneratedMessage request, [Encoding encoding]) {
    return GrpcUtils.messageToMap(request)
      .map((key, value) {
        dynamic newValue = value;

             if (value is Int64           ) { newValue = value.toInt(); }
        else if (value is Int32           ) { newValue = value.toInt(); }
        else if (value is List<int>       ) { newValue = encoding?.decode(value)??value; }
        else if (value is GeneratedMessage) { newValue = parseRawRequest(value, encoding); }

        return new MapEntry(key, newValue);
      });
  }
}

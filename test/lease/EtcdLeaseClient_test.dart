import 'dart:convert';
import 'dart:mirrors';

import 'package:etcd/etcd.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import '../helpers/TestUtils.dart';
import '../mocks/GrpcMocks.dart';

void main() {
  GrpcChannelMock     channelMock;
  EtcdLeaseClient     client;
  GrpcLeaseClientMock coreLeaseMock;
  Encoding            encoding;

  setUp(() {
    encoding      = ascii;
    channelMock   = new GrpcChannelMock();
    client        = new EtcdLeaseClient(channelMock, encoding);
    coreLeaseMock = new GrpcLeaseClientMock(channelMock);

    // -------- inject our mocked delegate into our real leaseClient
    var clientMirror   = reflect(client);
    var delegateSymbol = MirrorSystem.getSymbol('_delegate', clientMirror.type.superclass.owner);
    clientMirror.setField(delegateSymbol, coreLeaseMock);
  });

  test('grant()', () {
    var expectedValues = {
      'TTL': new Duration(seconds: faker.randomGenerator.int(only32bit: true)),
      'ID' : faker.randomGenerator.int(),
    };

    var result = client.grant(
      expectedValues['TTL'],
      requestedId: expectedValues['ID'],
    );
    expect(result, isA<Future<EtcdLeaseGrantResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreLeaseMock.leaseGrant(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('revoke()', () {
    var expectedValues = {
      'ID': faker.randomGenerator.int(),
    };

    var result = client.revoke(
      expectedValues['ID'],
    );
    expect(result, isA<Future<EtcdLeaseRevokeResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreLeaseMock.leaseRevoke(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('keepAlive()', () {
    var expectedValues = {
      'ID': faker.randomGenerator.int(),
    };

    var result = client.keepAlive(
      expectedValues['ID'],
    );
    expect(result, isA<Stream<EtcdLeaseKeepAliveResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreLeaseMock.leaseKeepAlive(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifyRepeatedRequestKeys(verification.captured.first, [expectedValues], encoding);
  });

  test('timeToLive()', () {
    var expectedValues = {
      'ID'  : faker.randomGenerator.int(),
      'keys': faker.randomGenerator.boolean(),
    };

    var result = client.timeToLive(
      expectedValues['ID'],
      listKeys: expectedValues['keys']
    );
    expect(result, isA<Future<EtcdLeaseTimeToLiveResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreLeaseMock.leaseTimeToLive(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });

  test('leases()', () {
    var expectedValues = <String, dynamic>{};

    var result = client.list();
    expect(result, isA<Future<EtcdLeaseListResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreLeaseMock.leaseLeases(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifySingleRequestKeys(verification.captured.first, expectedValues, encoding);
  });
}

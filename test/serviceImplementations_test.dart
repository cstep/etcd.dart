import 'dart:mirrors';
import 'package:test/test.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:etcd/etcd.dart';
import 'package:grpc/grpc.dart';
import 'package:etcd/generated/etcd/etcdserver/etcdserverpb/rpc.pbgrpc.dart'; // this is needed in order for our reflection to find the library // ignore: unused_import

void main() {
  Chain.capture(() { // -- enable terse stack traces

    /// tests to ensure all classes implementing a GRPC service have implemented all abstract methods from their parents
    group("GRPC Service Implementations", () {
      var grpcLibrary = currentMirrorSystem().libraries.keys.where((lib) => lib.toString() == 'package:etcd/generated/etcd/etcdserver/etcdserverpb/rpc.pbgrpc.dart').map((key) => currentMirrorSystem().libraries[key]).first;

      Map<Type, List<String>> ignoredMethods = {
        EtcdKVClient   : ['txn'],
        EtcdLeaseClient: [],
        EtcdWatchClient: []
      };

      Map<Type, Map<String, String>> mappedMethods = {
        EtcdKVClient   : {
          'range': 'fetchRange'
        },
        EtcdLeaseClient: {
          'leaseGrant'     : 'grant',
          'leaseRevoke'    : 'revoke',
          'leaseKeepAlive' : 'keepAlive',
          'leaseTimeToLive': 'timeToLive',
          'leaseLeases'    : 'list',
        },
        EtcdWatchClient: {
          'watch': 'range'
        },
      };

      currentMirrorSystem().libraries.keys
        .where((lib) => lib.toString().contains('package:etcd/etcd.dart')).map((key) => currentMirrorSystem().libraries[key])
        .forEach((LibraryMirror library) {
          library.declarations.forEach((Symbol key, DeclarationMirror concreteClassMirror) {
            if (concreteClassMirror is ClassMirror && concreteClassMirror.isSubtypeOf(reflectClass(Client)) && !concreteClassMirror.isAbstract) {
              var etcdClientName   = MirrorSystem.getName(concreteClassMirror.simpleName).replaceFirst('Etcd', '');
              var etcdClientType   = concreteClassMirror.reflectedType;
              var targetClientName = MirrorSystem.getName(concreteClassMirror.simpleName).replaceFirst('Etcd', '');
              var targetSymbol     = MirrorSystem.getSymbol(targetClientName, grpcLibrary);

              // ======== Find Our Declared Endpoints (abstract methods)
              Map<Symbol, MethodMirror> abstractMethods = {};

              var superclass = grpcLibrary.declarations.values.where((x) => x.simpleName == targetSymbol).map((x) => x as ClassMirror).first;
              do {
                superclass.declarations.values
                  .where((declaration) => (declaration is MethodMirror ? (declaration.isRegularMethod && !MirrorSystem.getName(declaration.simpleName).startsWith(r'$')) : false)).toList()
                  .forEach((method) => abstractMethods[method.simpleName] = method);
              } while (((superclass = superclass.superclass) != null) && superclass != reflectClass(Object)); // -- walk up the class hierarchy

              // ======== Check That All Endpoints are Implemented
              group("$etcdClientName implements all defined endpoints", () {
                abstractMethods.forEach((Symbol name, MethodMirror abstractMethod) {
                  String targetName = MirrorSystem.getName(abstractMethod.simpleName);
                  String mappedName = mappedMethods[etcdClientType][targetName] ?? targetName;
                  String proxyName  = '_${MirrorSystem.getName(abstractMethod.simpleName)}';
                  bool   mayProxy   = false; // concreteClassMirror.isSubtypeOf(reflectClass(ClassThatImplementsMethodProxying));

                  Function testDirect = (DeclarationMirror implementation) {
                    if (implementation is MethodMirror) {
                      String realName    = MirrorSystem.getName(implementation.simpleName);
                      bool   isSameName  = (realName == mappedName);
                      bool   isSameClass = (implementation.location.toString() != abstractMethod.location.toString()); // -- not just inheritance of the unimplemented abstract method

                      // if (isSameName && isSameClass) {
                      //   print('${concreteClassMirror.simpleName} at ${concreteClassMirror.location.sourceUri.path}\nimplements\n\t${realName} at ${implementation.location.sourceUri.path}\n\t which satisfies\n\t\t${targetName} from ${abstractMethod.location.sourceUri.path}');
                      // }
                      return (isSameName && isSameClass);
                    } else {
                      return false;
                    }
                  };

                  Function testProxy = (DeclarationMirror implementation) {
                    if (implementation is MethodMirror) {
                      String realName    = MirrorSystem.getName(implementation.simpleName);
                      bool   isSameName  = (mayProxy && realName == proxyName);
                      bool   isSameClass = (implementation.location?.toString() != abstractMethod.location.toString()); // -- not just inheritance of the unimplemented abstract method

                      // if (isSameName && isSameClass) {
                      //   print('${concreteClassMirror.simpleName} at ${concreteClassMirror.location.sourceUri.path}\nimplements\n\t${proxyName} at ${implementation.location.sourceUri.path}\n\t which satisfies\n\t\t${targetName} from ${abstractMethod.location.sourceUri.path}\n\t\tvia middleware proxy');
                      // }
                      return (isSameName && isSameClass);
                    } else {
                      return false;
                    }
                  };

                  // -------- assert this particular endpoint has been implemented
                  var isIgnored = ignoredMethods[etcdClientType].contains(targetName); //forEach((String ignoredName) => abstractMethods.removeWhere((Symbol methodName, _) => MirrorSystem.getName(methodName) == ignoredName));
                  test(targetName, () {
                    bool directlyImplements = concreteClassMirror.declarations.values.any(testDirect);
                    bool implementsProxy    = concreteClassMirror.declarations.values.any(testProxy );

                    expect(directlyImplements || implementsProxy, isTrue, reason: 'missing required implementation of method `$targetName()`' + (mayProxy ? ' (or proxy method `$proxyName()`)' : ''));
                    expect(directlyImplements  ^ implementsProxy, isTrue, reason: 'may not define BOTH direct implementation `$targetName()` and proxy method `$proxyName()`');
                  }, skip: (isIgnored ? '$etcdClientType\.$mappedName not implemented' : false));
                });
              });
            }
          });
        });
    });
  });
}

import 'dart:async';
import 'dart:mirrors';

import 'package:etcd/generated/etcd/etcdserver/etcdserverpb/rpc.pbgrpc.dart' as rpc ;
import 'package:grpc/grpc.dart' as grpc_core;
import 'package:grpc/src/client/connection.dart' as grpc_hidden ;
import 'package:mockito/mockito.dart';

class GrpcChannelMock extends Mock implements grpc_core.ClientChannel {
  grpc_hidden.ClientConnection _connection;

  @override
  GrpcConnectionMock createConnection() => new GrpcConnectionMock();

  @override
  Future<GrpcConnectionMock> getConnection() async {
    return _connection ??= createConnection();
  }

  @override
  GrpcClientCallMock<Q, R> createCall<Q, R>(grpc_core.ClientMethod<Q, R> method, Stream<Q>requests, grpc_core.CallOptions options) {
    return new GrpcClientCallMock<Q, R>(); // -- we just need the generic types to be passed!
  }
}

class GrpcConnectionMock extends Mock implements grpc_hidden.ClientConnection {
}

class GrpcClientCallMock<Q, R> extends Mock implements grpc_core.ClientCall<Q, R> {
  final StreamController<R> _responses = new StreamController();

  @override Stream<R> get response => _responses.stream;

  @override bool isCancelled = false;

  @override Future<void> cancel() async {
    isCancelled = true;
    return _responses.close();
  }
}

abstract class GrpcClientMock extends Mock implements grpc_core.Client {
  final GrpcChannelMock       _channel;
  final grpc_core.CallOptions _options;

  GrpcClientMock(this._channel, {grpc_core.CallOptions options}): _options = (options??grpc_core.CallOptions());

  @override
  GrpcClientCallMock<Q, R> $createCall<Q, R>(grpc_core.ClientMethod<Q, R> method, Stream<Q> requests, {grpc_core.CallOptions options}) {
    return _channel.createCall(method, requests, _options.mergedWith(options));
  }
}

class GrpcKVClientMock extends GrpcClientMock implements rpc.KVClient {
  static final _mirror = reflectClass(rpc.KVClient);
  static final grpc_core.ClientMethod<rpc.PutRequest,         rpc.PutResponse        > _putMethod         = _mirror.getField(MirrorSystem.getSymbol(r'_$put',         _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.RangeRequest,       rpc.RangeResponse      > _rangeMethod       = _mirror.getField(MirrorSystem.getSymbol(r'_$range',       _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.DeleteRangeRequest, rpc.DeleteRangeResponse> _deleteRangeMethod = _mirror.getField(MirrorSystem.getSymbol(r'_$deleteRange', _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.CompactionRequest,  rpc.CompactionResponse > _compactMethod     = _mirror.getField(MirrorSystem.getSymbol(r'_$compact',     _mirror.owner)).reflectee;

  GrpcKVClientMock(GrpcChannelMock channel, {grpc_core.CallOptions Options}): super(channel, options: Options) {
    when(        put(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_putMethod,         Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when(      range(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_rangeMethod,       Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when(deleteRange(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_deleteRangeMethod, Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when(    compact(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_compactMethod,     Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
  }
}

class GrpcLeaseClientMock extends GrpcClientMock implements rpc.LeaseClient {
  static final _mirror = reflectClass(rpc.LeaseClient);
  static final grpc_core.ClientMethod<rpc.LeaseGrantRequest,      rpc.LeaseGrantResponse     > _grantMethod      = _mirror.getField(MirrorSystem.getSymbol(r'_$leaseGrant',      _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.LeaseRevokeRequest,     rpc.LeaseRevokeResponse    > _revokeMethod     = _mirror.getField(MirrorSystem.getSymbol(r'_$leaseRevoke',     _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.LeaseKeepAliveRequest,  rpc.LeaseKeepAliveResponse > _keepAliveMethod  = _mirror.getField(MirrorSystem.getSymbol(r'_$leaseKeepAlive',  _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.LeaseTimeToLiveRequest, rpc.LeaseTimeToLiveResponse> _timeToLiveMethod = _mirror.getField(MirrorSystem.getSymbol(r'_$leaseTimeToLive', _mirror.owner)).reflectee;
  static final grpc_core.ClientMethod<rpc.LeaseLeasesRequest,     rpc.LeaseLeasesResponse    > _leasesMethod     = _mirror.getField(MirrorSystem.getSymbol(r'_$leaseLeases',     _mirror.owner)).reflectee;

  GrpcLeaseClientMock(GrpcChannelMock channel, {grpc_core.CallOptions Options}): super(channel, options: Options) {
    when(     leaseGrant(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_grantMethod,      Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when(    leaseRevoke(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_revokeMethod,     Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when( leaseKeepAlive(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseStream($createCall(_keepAliveMethod,  Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when(leaseTimeToLive(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_timeToLiveMethod, Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
    when(    leaseLeases(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseFuture($createCall(_leasesMethod,     Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
  }
}

class GrpcWatchClientMock extends GrpcClientMock implements rpc.WatchClient {
  static final _mirror = reflectClass(rpc.WatchClient);
  static final grpc_core.ClientMethod<rpc.WatchRequest, rpc.WatchResponse> _watchMethod = _mirror.getField(MirrorSystem.getSymbol(r'_$watch', _mirror.owner)).reflectee;

  GrpcWatchClientMock(GrpcChannelMock channel, {grpc_core.CallOptions Options}): super(channel, options: Options) {
    when(watch(any, options: anyNamed('options'))).thenAnswer((Invocation i) => new grpc_core.ResponseStream($createCall(_watchMethod, Stream.fromIterable([i.positionalArguments[0]]), options: i.namedArguments['options'])));
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:mirrors';

import 'package:etcd/etcd.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import '../helpers/GrpcUtils.dart';
import '../helpers/Matchers.dart';
import '../helpers/TestUtils.dart';
import '../mocks/GrpcMocks.dart';

void main() {
  GrpcChannelMock     channelMock;
  EtcdWatchClient     client;
  GrpcWatchClientMock coreWatchMock;
  Encoding            encoding;

  setUp(() {
    encoding      = ascii;
    channelMock   = new GrpcChannelMock();
    client        = new EtcdWatchClient(channelMock, encoding);
    coreWatchMock = new GrpcWatchClientMock(channelMock);

    // -------- inject our mocked delegate into our real leaseClient
    var clientMirror   = reflect(client);
    var delegateSymbol = MirrorSystem.getSymbol('_delegate', clientMirror.type.superclass.owner);
    clientMirror.setField(delegateSymbol, coreWatchMock);
  });

  test('single()', () {
    var expectedValues = {
      'key'           : faker.lorem.word(),
      'startRevision' : faker.randomGenerator.int(),
      'progressNotify': faker.randomGenerator.boolean(),
      'filters'       : faker.randomGenerator.elements(EtcdWatchFilter.values, unique: true, count: faker.randomGenerator.int(min: 1, max: EtcdWatchFilter.values.length)),
      'prevKv'        : faker.randomGenerator.boolean(),
      'watchId'       : faker.randomGenerator.int(),
      'fragment'      : faker.randomGenerator.boolean(),
    };

    var result = client.single(
      expectedValues['key'],
      startRevision : expectedValues['startRevision'],
      progressNotify: expectedValues['progressNotify'],
      filters       : expectedValues['filters'],
      prevKv        : expectedValues['prevKv'],
      watchId       : expectedValues['watchId'],
      fragment      : expectedValues['fragment'],
    );
    expect(result, isA<Future<EtcdWatcher>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreWatchMock.watch(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifyRepeatedRequestKeys(verification.captured.first, [{'createRequest': expectedValues}], encoding);
  });

  test('prefix()', () {
    var expectedValues = {
      'key'           : faker.lorem.word(),
      'startRevision' : faker.randomGenerator.int(),
      'progressNotify': faker.randomGenerator.boolean(),
      'filters'       : faker.randomGenerator.elements(EtcdWatchFilter.values, unique: true, count: faker.randomGenerator.int(min: 1, max: EtcdWatchFilter.values.length)),
      'prevKv'        : faker.randomGenerator.boolean(),
      'watchId'       : faker.randomGenerator.int(),
      'fragment'      : faker.randomGenerator.boolean(),
    };

    var result = client.prefix(
      expectedValues['key'],
      startRevision : expectedValues['startRevision'],
      progressNotify: expectedValues['progressNotify'],
      filters       : expectedValues['filters'],
      prevKv        : expectedValues['prevKv'],
      watchId       : expectedValues['watchId'],
      fragment      : expectedValues['fragment'],
    );
    expect(result, isA<Future<EtcdWatcher>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreWatchMock.watch(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");

    expectedValues['rangeEnd'] = encoding.decode(encoding.encode(expectedValues['key'])..last += 1); // -- increment the last byte of key to indicate it's a prefix (see WatchClient._watchRangeRequest())
    TestUtils.verifyRepeatedRequestKeys(verification.captured.first, [{'createRequest': expectedValues}], encoding);
  });

  test('range()', () {
    var expectedValues = {
      'key'           : faker.lorem.word(),
      'rangeEnd'      : faker.lorem.word(),
      'startRevision' : faker.randomGenerator.int(),
      'progressNotify': faker.randomGenerator.boolean(),
      'filters'       : faker.randomGenerator.elements(EtcdWatchFilter.values, unique: true, count: faker.randomGenerator.int(min: 1, max: EtcdWatchFilter.values.length)),
      'prevKv'        : faker.randomGenerator.boolean(),
      'watchId'       : faker.randomGenerator.int(),
      'fragment'      : faker.randomGenerator.boolean(),
    };

    var result = client.range(
      expectedValues['key'],
      expectedValues['rangeEnd'],
      startRevision : expectedValues['startRevision'],
      progressNotify: expectedValues['progressNotify'],
      filters       : expectedValues['filters'],
      prevKv        : expectedValues['prevKv'],
      watchId       : expectedValues['watchId'],
      fragment      : expectedValues['fragment'],
    );
    expect(result, isA<Future<EtcdWatcher>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreWatchMock.watch(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");

    expectedValues['rangeEnd'] = encoding.decode(encoding.encode(expectedValues['rangeEnd'])..last += 1); // -- increment the last byte of rangeEnd to make it inclusive (see WatchClient._watchRangeRequest())
    TestUtils.verifyRepeatedRequestKeys(verification.captured.first, [{'createRequest': expectedValues}], encoding);
  });

  test('cancel()', () {
    var expectedValues = {
      'watchId': faker.randomGenerator.int(),
    };

    var result = client.cancel(
      expectedValues['watchId'],
    );
    expect(result, isA<Future<void>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreWatchMock.watch(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifyRepeatedRequestKeys(verification.captured.first, [{'cancelRequest': expectedValues}], encoding);
  });

  test('progress()', () {
    var expectedValues = {
    };

    var result = client.progress(
      faker.randomGenerator.int(), // -- this is used internally to cache our stream instances, but isn't actually passed in the request, so we've omitted it from `expectedValues` and just passed it directly
    );
    expect(result, isA<Future<EtcdWatchResponse>>(), reason: 'Returned result of type other than expected');

    var verification = verify(coreWatchMock.watch(captureAny, options: captureAnyNamed('options')));
    expect(verification.callCount, equals(1), reason: "Unexpected number of calls");
    TestUtils.verifyRepeatedRequestKeys(verification.captured.first, [{'progressRequest': expectedValues}], encoding);
  });

  test('streamFor()', () {
    var firstWatchId = faker.randomGenerator.int();
    var otherWatchId = faker.randomGenerator.int();

    var shouldBeNull = client.streamFor(firstWatchId, createIfNotExists: false);
    expect(shouldBeNull, isNull, reason: 'Call for non-existent watchId should return null when `createIfNotExists` is set to `false`, but did not');

    var first        = client.streamFor(firstWatchId, createIfNotExists: true);
    var sameInstance = client.streamFor(firstWatchId, createIfNotExists: true);
    var other        = client.streamFor(otherWatchId, createIfNotExists: true);

    expect(first, isA<Stream>().having((x) => x.isBroadcast, 'isBroadcast', isTrue), reason: 'Returned stream unexpectedly not a broadcastStream');

    expect(first, same(sameInstance), reason: 'Multiple calls for the same watchId should return the same stream, but did not'    );
    expect(first, isNot(same(other)), reason: 'Multiple calls for different watchIds should return different streams, but did not');
  });

  // @depends('streamFor()')
  test('shutdown()', () async {
    var watchIds = faker.randomGenerator.ints(faker.randomGenerator.integer(10, min: 3));
    var watches  = List.generate(watchIds.length, (i) => client.streamFor(watchIds[i], createIfNotExists: true));

    var closedCounter = 0;

    var streams = reflect(client).getField(MirrorSystem.getSymbol('_watchStreams', reflect(client).type.owner)).reflectee; // -- this is a private property, so we have to reflect it out

    client.shutdown(); // ignore: unawaited_futures

    List<Future> streamFutures = (streams.values as Iterable).map<Future<dynamic>>((streamCache) {
      var streamAsFuture = (streamCache.requestStream.stream as Stream).toList();

      // -------- verify each outgoing request
      streamAsFuture.then((stream) => stream.forEach((rawRequest) {
        var parsedRawRequest = GrpcUtils.parseRawRequest(rawRequest);
        expect(parsedRawRequest,                  containsKey('cancelRequest'),                       reason: 'generated request was unexpectedly not a cancelRequest'    );
        expect(parsedRawRequest['cancelRequest'], containsPair('watchId', containedWithin(watchIds)), reason: 'Cancel request should contain a valid watchId, but did not');
        closedCounter++;
      }));

      return new Completer().future; //streamAsFuture;
    }).toList();

    // -------- wait for all streams to finish
    return expectLater(
      Future.wait(streamFutures)
        .timeout(Duration(seconds: 1), onTimeout: () => null).whenComplete(() => null)
        .then((_) => closedCounter),
      completion(equals(watches.length))
    );
  });
}

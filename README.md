![Bitbucket Pipelines branch](https://img.shields.io/bitbucket/pipelines/cstep/etcd.dart/master?label=build)
![Codecov](https://img.shields.io/codecov/c/bitbucket/cstep/etcd.dart/master?label=coverage)
![Dart Version](https://img.shields.io/badge/dart-%5E2.7-blue)
[![extra_pedantic on pub.dev](https://img.shields.io/badge/style-extra__pedantic-blue)](https://pub.dev/packages/extra_pedantic)
![License](https://img.shields.io/badge/license-Apache%202-blue)

A client library for connecting to and interacting with [ETCD](https://etcd.io/), a distributed, reliable key-value store for the most critical data of a distributed system

## Usage

Create a client:
```dart
var client = new EtcdClient('etcd.example.com', port: 2379, options: new ChannelOptions(credentials: ChannelCredentials.insecure()));
```

Fetch a value:
```dart
var currentValue = await client.kv.fetch('myKey');
```

Set/Update a value:
```dart
await client.kv.put('myKey', 'myValue');
```

Attach a lease to a value:
```dart
var lease = await client.lease.grant(Duration(seconds: 5));
await client.kv.put('myKey', 'myValue', lease: lease.id);
```

Check remaining time on a lease:
```dart
var remainingTTL = await client.lease.timeToLive(lease.id, listKeys: true);
```

Renew a lease:
```dart
await lease.keepAlive();
```

Revoke a lease (and the values its attached to):
```dart
await client.lease.revoke(lease.id);
```

Watch a value for changes:
```
var watcher = await client.watch.single(KEY, prevKv: true);
watcher.listen((EtcdWatchEvent event) {
  switch (event.type) {
    case EtcdWatchEventType.PUT:
      print('Key `${event.kv.key}` modified: replaced value `${event.prevKv.value}` with `${event.kv.value}`');
      break;

    case EtcdWatchEventType.DELETE:
      print('Key `${event.kv.key}` was deleted: deleted value was `${event.prevKv.value}`');
      break;
  }
});
```

## Features and bugs

For now, please file feature requests and bugs by emailing the author
